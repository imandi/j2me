package com.iMandi;

/***
 *
 * Copyright (C) 2008 Alessandro La Rosa
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: alessandro.larosa@gmail.com
 *
 * Author: Alessandro La Rosa
 */

import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;

public class TabMenu
{
	int background = 0xffffff;

	int bgColor = 0xcccccc;
	int bgFocusedColor = 0x0000ff;
	int foreColor = 0x000000;
	int foreFocusedColor = 0xffffff;
	int cornerRadius = 6;
	int padding = 2;
	int margin = 0;
	Font font = Font.getDefaultFont();

	int scrollStep = 20;

	public int selectedTab = 0;	//selected tab index
	int[] tabsWidth = null;	//width of single tabs
	int[] tabsLeft = null;	//left X coordinate of single tabs
	public int tabHeight = 0;		//height of tabs (equal for all tabs)
	String[] tabs = null;	//tab labels
	int menuWidth = 0;		//total menu width

	int viewportWidth = 0;	//visible viewport width
	int viewportX = 0;		//current viewport X coordinate

        public void setbgColor(int color){
            bgColor = color;
        }
	public TabMenu(String[] tabs, int width)
	{
		this.tabs = tabs;

		this.viewportWidth = width;

		initialize();
	}
	void initialize()
	{
		tabHeight = font.getHeight() + cornerRadius + 2 * padding;

		menuWidth = 0;

		tabsWidth = new int[tabs.length];
		tabsLeft  = new int[tabs.length];

		for(int i = 0; i < tabsWidth.length; i++)
		{
			tabsWidth[i] = font.stringWidth(tabs[i]) + 2 * padding + 2 * cornerRadius;

			tabsLeft[i] = menuWidth;

			menuWidth += tabsWidth[i];

			if(i > 0)
			{
				menuWidth += margin;
			}
		}
	}
	public void goRight()
	{
		go(+1);
	}
	public void goLeft()
	{
		go(-1);
	}
	private void go(int delta)
	{
		int newTab = Math.max(0, Math.min(tabs.length - 1, selectedTab + delta));

		boolean scroll = true;

		if(newTab != selectedTab && isTabVisible(newTab))
		{
			selectedTab = newTab;

			if(	(delta > 0 && tabsLeft[selectedTab] + tabsWidth[selectedTab] > viewportX + viewportWidth) ||
				(delta < 0 && tabsLeft[selectedTab] < viewportX))
			{
				scroll = true;
			}
			else
			{
				scroll = false;
			}
		}
		if(scroll)
		{
			viewportX = Math.max(0, Math.min(menuWidth - viewportWidth, viewportX + delta * scrollStep));
		}
	}
	private boolean isTabVisible(int tabIndex)
	{
		return tabsLeft[tabIndex] < viewportX + viewportWidth &&
			tabsLeft[tabIndex] + tabsWidth[tabIndex] >= viewportX;
	}

	public void paint(Graphics g)
	{
		int currentX = - viewportX;

//		g.setClip(0, 0, viewportWidth, tabHeight);

		g.setColor(background);
		g.fillRect(0, 0, viewportWidth, tabHeight);

		for(int i = 0; i < tabs.length; i++)
		{
			g.setColor(i == selectedTab ? bgFocusedColor : bgColor);

//			g.fillRoundRect(currentX, 0, tabsWidth[i], tabHeight + cornerRadius, 2 * cornerRadius, 2 * cornerRadius);
                        g.fillRect(currentX, 0, tabsWidth[i], tabHeight + cornerRadius);

			g.setColor(i == selectedTab ? foreFocusedColor : foreColor);

			g.drawString(tabs[i], currentX + cornerRadius + padding, cornerRadius + padding, Graphics.LEFT | Graphics.TOP);

			currentX += tabsWidth[i] + margin;
		}
	}
}