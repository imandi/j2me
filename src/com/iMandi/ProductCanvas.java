package com.iMandi;

import com.iMandi.bean.CartItems;
import com.iMandi.bean.CartProduct;
import com.iMandi.bean.Category;
import com.iMandi.bean.Product;
import com.iMandi.bean.Profile;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.MyConnection;
import com.iMandi.connection.MyConnectionInf;
import com.iMandi.connection.RMS;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import org.json.me.StringWriter;
import org.json.me.util.ImageList;

public class ProductCanvas extends ImandiCanvas implements MyConnectionInf {

    Product productDetail;
    ItemDetailView detailView = new ItemDetailView();
//    int detailY = 0;

    public ProductCanvas() {
        title = "Product";
        LEFT_MENU = "Back";
        CENTER_MENU = "Select";

         if (CartItems.getInstance().cartItemTable.size() > 0 /*&& 1==2*/) {
                RIGHT_MENU ="Cart[" + CartItems.getInstance().cartItemTable.size() + "]";
            }
            else
            {
                RIGHT_MENU = null;
            }

        isLoading= true;
        MyConnection.getInstance().makeConnection(Constant.URL_PRODUCT_LIST + ((Category) Constant.currentCategory.elementAt(0)).getCategoryid(), this, null, "GET", (byte) 1);
    }
    List friendList;

    protected void paint(Graphics g) {
        super.paint(g);

        if (friendList != null && productDetail == null && !isLoading) {
            getPrevFont();
            friendList.paint(g);
            setPrevFont();
        }
       
        if (isLoading) {
            Constant.loading(g, this, Constant.LOADING);
        }
        showMenu();

        if (productDetail != null) {
            detailView.diaplayDetail(g, this, productDetail, screenMoveY);
             drawTitle(title);
             if (isLoading) {
                Constant.loading(g, this, Constant.LOADING);
            }
            return;
        }

        showAlert();
        drawBottomMenu();
        drawTitle(title);
    }

    protected void keyPressed(final int keyCode) {
        if (isLoading) {
            return;
        }

        if (!Constant.isKnownKey(this, keyCode)) {
            if (keyCode == Constant.KEY_SOFT_LEFT) {


                if (productDetail != null) {
                    ImageList.images.removeAllElements();
                    productDetail = null;
                    refresh();
                    System.gc();
                    title = "Product";
                    return;
                }

            } else {
                 if(Profile.getInstance().getUserId()!= null){
                    CartCanvas canvas = new CartCanvas();
                canvas.prevDisplay = canvas ;
                iMandiMIDlet.display.setCurrent(canvas);
                    }else{
//                        CartCanvas canvas = new CartCanvas();
//                        LoginScreen loginScreen = new LoginScreen(canvas,null);
//                        loginScreen.display();
//                        HelloMIDlet.display.setCurrent();
                    }
            }
        }
        super.keyPressed(keyCode);
        if (!showMenu && !showAlert) {
            if (friendList != null) {
                new Thread(new Runnable() {

                    public void run() {
                        friendList.keyPressed(keyCode);
                    }
                }).start();
            }
        }
        refresh();
    }

    protected void keyRepeated(int keyCode) {
        super.keyRepeated(keyCode);
        if (friendList != null) {
            friendList.keyPressed(keyCode);
        }
        refresh();
    }

    class List extends IMandiList //extends Canvas
    {

        Vector products;
        ProductCanvas canvas;
        int rowMenuIndex;
        boolean rowMenu;
        int maxCommandInRow = 0;
        private ProductCanvas productCanvas;

        public List(String title, Vector products, ProductCanvas canvas) {
            this.products = products;
            this.canvas = canvas;
            itemsTop = new int[products.size()];
            this.itemHeight = (canvas.g.getFont().getHeight()*3)+5;
        }

        public int getItemWidth() {
            return canvas.getWidth() - 2 * borderWidth - 2 * padding - 2 * margin;
        }

        public void keyPressed(int key) {

            int keyCode = canvas.getGameAction(key);

            // is there 1 item at least?
            if (products.size() > 0) {
                // going up
                if (keyCode == Canvas.UP) {

                     if(productDetail!= null){
                    screenMoveY = screenMoveY +100;
                    if(screenMoveY>0)
                        screenMoveY =0;
                    return;
                }
                     
                    // current item is clipped on top, so can scroll up
                    if (itemsTop[selectedItem] - Constant.TITLE_HEIGHT * 2 < scrollTop) {
                        scrollTop -= SCROLL_STEP;
                        if (scrollTop < 0) {
                            scrollTop = 0;
                        }
                        canvas.repaint();
                    } // is there a previous item?
                    else if (selectedItem > 0) {
                        selectedItem--;
                        canvas.repaint();
                    }
                    if (selectedItem == 0) {
                        
                    }
                    rowMenuIndex = 0;
                    rowMenu = false;
                } //going down
                else if (keyCode == Canvas.DOWN) {
                    if(productDetail!=null){
                    if(screenDown)
                    screenMoveY = screenMoveY -100;

                    return;
                }
                    // current item is clipped on bottom, so can scroll down
                    if (itemsTop[selectedItem] + itemHeight >= scrollTop + (canvas.getHeight() - itemHeight)) {
                        scrollTop += SCROLL_STEP;
                        //selectedItem++;
                        canvas.repaint();
                    } // is there a following item?
                    else if (selectedItem < products.size() - 1) {
                        selectedItem++;

                        canvas.repaint();
                    }
                    if (selectedItem == products.size() - 1) {
                        
                    }
                    rowMenu = false;
                    rowMenuIndex = 0;
                } else if (keyCode == Canvas.RIGHT) {
                } else if (keyCode == Canvas.LEFT) {
                } else if (keyCode == Canvas.FIRE) {

                    if (productDetail == null) {
                        new Thread(new Runnable() {

                            public void run() {
                                try {
                                   showLoading();
                                   MyConnection.getInstance().fetchData(Constant.URL_PRODUCT_DETAIL + ((Product) products.elementAt(selectedItem)).getProductId(), ProductCanvas.this, (byte) 2);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }).start();
//                    Utility.openProductList(CategoryScreen.this, (Category) products.elementAt(selectedItem));
                    }else {
                    if (productDetail != null) {
                        if (Profile.getInstance().getUserId() != null) {
                            ((Product) products.elementAt(selectedItem)).setProductCartQuentity(1);

//                            if (CartItems.getInstance().getItem((Product) products.elementAt(selectedItem)) != null) {
////                        ((Product) products.elementAt(selectedItem)).setProductCartQuentity(0);//removing item
//                            }
                            if (((Product) products.elementAt(selectedItem)).getProductCartQuentity() > 0) {
//                                CartItems.getInstance().addItem((Product) products.elementAt(selectedItem), canvas);
                                CartItems.getInstance().addItem((Product) productDetail, canvas);
                            } else {
//                                CartItems.getInstance().removeItem((Product) products.elementAt(selectedItem), canvas);
                                CartItems.getInstance().removeItem((Product) productDetail, canvas);
                            }
                        } else {
//                            LoginScreen loginScreen = new LoginScreen(canvas, (Product) products.elementAt(selectedItem));
//                            loginScreen.display();

//                        HelloMIDlet.display.setCurrent();
                        }
                        canvas.repaint();
                        return;
                    }
                }
                } 
            }
            //Constant.printLog("scrollTop:", ""+scrollTop+" :itemTop:"+itemsTop[selectedItem]);
            refresh();
        }

        protected void paint(Graphics g) {

            g.setFont(Constant.font_medium);
            // paint List background
            g.setColor(Constant.COLOR_bgColor);
            g.fillRect(0, 20, canvas.getWidth(), canvas.getHeight());

            if (showDetail) {
//            canvas.detailView.diaplayDetail(g, canvas);
                return;
            }

            // translate accordingly to current List vertical scroll
            g.translate(0, -scrollTop);

            int top = 20;

            g.setFont(font);

            // loop List items
            for (int i = 0; i < products.size(); i++) {
                int itemRows = 1;//itemLines[i].length;

                Image imagePart = null;//product;//imgetImage(i);

                int itemHeight = this.itemHeight;//itemRows * font.getHeight() + linePadding * (itemRows - 1);

                itemsTop[i] = top;
//            itemsHeight[i] = itemHeight;

                // is image part higher than the text part?
                if (imagePart != null && imagePart.getHeight() > itemHeight) {
                    itemHeight = imagePart.getHeight();
                }
//                itemHeight += 2 * padding + 2 * borderWidth;

                itemHeight = (g.getFont().getHeight()*2)+10;
                g.translate(0, top);

                if (borderWidth > 0) {
                    // paint item border
                    g.setColor(i == selectedItem ? Constant.COLOR_borderColor : Constant.COLOR_borderColor);
                    g.fillRect(margin, margin + topSpace, canvas.getWidth() - 2 * margin, itemHeight);
                }

                // paint item background
                g.setColor(i == selectedItem ? Constant.COLOR_backSelectedColor : Constant.COLOR_backColor);
                g.drawRect(margin + borderWidth, margin + borderWidth + topSpace, canvas.getWidth() - 2 * margin - 2 * borderWidth, itemHeight - 2 * borderWidth);

                if(i == selectedItem)
                {
                    g.setColor(Constant.COLOR_backSelectedColor);
                    g.fillRect(margin + borderWidth, margin + borderWidth + topSpace,3, itemHeight - 2 * borderWidth);
                }
                // has this item an image part?
                if (imagePart != null) {
                    g.drawImage(imagePart, margin + borderWidth + padding, margin + borderWidth + padding + topSpace, Graphics.TOP | Graphics.LEFT);
                }

                // paint item text rows
                g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

                int textLeft = margin + borderWidth + padding + (imagePart != null ? imagePart.getWidth() + padding : 0);

                getPrevFont();
                g.setFont(Constant.font_medium_bold);
                g.drawString(((Product) products.elementAt(i)).getProductName(), textLeft+6, topSpace + margin + borderWidth, Graphics.TOP | Graphics.LEFT);
                g.setFont(Constant.font_medium);
                String suk = ((Product) products.elementAt(i)).getProductSku() ;
                g.drawString(suk, textLeft+6, topSpace + margin + borderWidth + padding  + font.getHeight(), Graphics.TOP | Graphics.LEFT);
                g.setFont(Constant.font_medium_bold);
                g.drawString(" | "+((Product) products.elementAt(i)).getProductPrice(), textLeft+6+(g.getFont().stringWidth(suk)), topSpace + margin + borderWidth + padding  + font.getHeight(), Graphics.TOP | Graphics.LEFT);
//                g.drawString(((Product) products.elementAt(i)).getProductPrice(), textLeft+6, topSpace + margin + borderWidth + padding + (2 * font.getHeight()), Graphics.TOP | Graphics.LEFT);

                setPrevFont();
                g.translate(0, -top);

                top += itemHeight + 2 * margin;
            }
            // finally, translate back
            g.translate(0, scrollTop);
        }

        public void recycleImage() {
        }
        boolean adding = false;
        int index = 10;

    }

    public void menuItemSelected(String menuItemLabel) {
        super.menuItemSelected(menuItemLabel);
        alertText = menuItemLabel;
        showAlert = true;
        isKeyEventConsume = true;
    }

    public void sendData(String data, byte requestId) {
        super.sendData(data, requestId);
        if (requestId == 1) {
            System.out.println("sendData:" + data);
            Vector products = JsonStringToOjbect.getProductList(data);
            System.out.println("productList -->" + products.toString());
            friendList = new List("Test canvas", products, this);
//        startImageLoading();
        } else if (requestId == 2) {
            try{
            System.out.println("sendData:" + data);
            productDetail = JsonStringToOjbect.getProductDetail(data);
            title = "Product Detail";
            System.out.println("productDetail -->" + productDetail.toString());
            startImageLoading();
            } catch (Exception ex) {

                        ex.printStackTrace();
                    }
        }
        hideLoading();
        refresh();
    }
    Thread imageLoading;

    private void startImageLoading() {
        try{
           
        if(productDetail !=null && productDetail.productImage != null && productDetail.productImage.size()>0)
        Constant.printLog("startImageLoading", productDetail.productImage.toString());
        imageLoading = new Thread(new Runnable() {

            public void run() {

                ImageList.images.removeAllElements();
                if(productDetail !=null && productDetail.productImage != null && productDetail.productImage.size()>0)
                for (int i = 0; i < productDetail.productImage.size(); i++) {
                    try {

//                        Thread.sleep(5000);
                        if(productDetail == null){
                            hideLoading();
                            refresh();
                            return;
                        }
//                        showLoading();
                            showLoading();
                        if(productDetail !=null && productDetail.productImage != null && productDetail.productImage.size()>0){
                            MyConnection.getInstance().downloadAndSaveImage((String) productDetail.productImage.elementAt(i), i, ProductCanvas.this,(byte)1);
                        }
                        else
                        {
                            break;
                        }
                    } catch (Exception ex) {
                        hideLoading();
                        refresh();
                        ex.printStackTrace();
                    }
                }
                refresh();

            }
        });
        imageLoading.start();
        } catch (Exception ex) {
                hideLoading();
                refresh();
                        ex.printStackTrace();
                    }
        
    }

     public void imageLoaded(String Status,byte requestID) {
         super.imageLoaded(Status, requestID);
        repaint();
        System.out.println("IMAGE LOADED!!!!!");
    }
  
}

