package com.iMandi;

import com.ui.components.TextBox;
import com.iMandi.bean.CitrusErrors;
import com.iMandi.bean.Profile;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.MyConnection;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.TextField;
import org.json.me.JSONException;
import org.json.me.JSONObject;

/**
 *
 * @author nagendrakumar
 */
public class ProfileCanvas
        extends ImandiCanvas {

    byte totalItem =7;
    byte currentSelected = 1;
    byte buttonMargin = 2;
    TextBox name, dob, address, pin, mobile, aadhar, preferencess, crops;
    FormHelper formHelper = new FormHelper();
    int otpWatingTime = 0;
    Image image;

    public ProfileCanvas(String title) {
        this.title = title;
        LEFT_MENU = "SAVE";
        RIGHT_MENU = "SKIP" ;
        Profile.getInstance().restoreMe();
        if(Profile.getInstance().getPasswordEncrepted() == null)
            Profile.getInstance().setPasswordEncrepted(Utility.encriptPass(Profile.getInstance().getPassword())) ;
        
        new Thread(new Runnable() {
            public void run() {
                try{
                    Thread.sleep(1000);
                MyConnection.getInstance().makeConnection(Constant.URL_PROFILE + Profile.getInstance().getUsername(), ProfileCanvas.this, null, "GET", (byte) 1); 
                }catch(Exception e){
                    
                }
            }
        }).start();
    }

    void init() {
        if (image == null) {
            try {
                image = Image.createImage("/default-profile.png");
            } catch (Exception e) {
            }

        }
        if (formHelper == null) {
            formHelper = new FormHelper();
            
        }
    }

    protected void paint(Graphics g) {
        super.paint(g);
        init();
        drawProfileScreen(g);
        drawTitle(title);
        drawBottomMenu();


    }

    protected void keyPressed(int keyCode) {

//        super.keyPressed(keyCode);

        if (!Constant.isKnownKey(this, keyCode)) {
            if (keyCode == Constant.KEY_SOFT_LEFT) {
//                Utility.openLoginAsGuest(null);
//                {"imageFileId":"imageFileId123","aboutMe":"aboutMe123","currentStatus":"currstatus","dob":"05-JUN-1980","gender":"male","name":"Pushpesh Kr","email":"pkr@gmail.com","location":"Delhi","privacyStatusMap":{"DNM":1,"DND":30,"DNC":1},"backupFileId":"pkrbkp", "address":"address123"}
                
                
                 final JSONObject inner = new JSONObject();
                    try {
                        if(Constant.isEmpty(mobile.getText())){
                            Constant.showError("Please enter mobile number!");
                            return;
                        }
                        if(Constant.isEmpty(name.getText())){
                            Constant.showError("Please enter name!");
                            return;
                        }
                        if(Constant.isEmpty(pin.getText())){
                            Constant.showError("Please enter pin code!");
                            return;
                        }
//                        if(!Constant.isEmpty(.getText())){
//                            Constant.showError("Please enter mobile number!");
//                        }
//                        
                        Profile.getInstance().setMobileNumber(mobile.getText());

                        //{"imageFileId":"imageFileId123","aboutMe":"aboutMe123","currentStatus":"currstatus",
                        //"dob":"05-JUN-1980","gender":"male","name":"Pushpesh Kr",
                        //"email":"pkr@gmail.com","location":"Delhi",
                        //"privacyStatusMap":{"DNM":1,"DND":30,"DNC":1},"backupFileId":"pkrbkp",
                        //"address":"address123"}
                        
                        inner.put("mobileNumber", mobile.getText());//Profile.getInstance().getMobileNumber());
//                        inner.put("imageFileId", "imageFileId123");
                        inner.put("pinCode", pin.getText());
                        inner.put("dob", dob.getText());
                        inner.put("gender", "male");
                        inner.put("name", name.getText());
                        inner.put("address", address.getText());
                        inner.put("aadharCardNumber", aadhar.getText());


                    } catch (JSONException ex) {
                        ex.printStackTrace();
//                        Constant.showError(ex.getMessage(),HelloMIDlet.display);
                    }
                  new Thread(new Runnable() {

                public void run() {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                     MyConnection.getInstance().makeConnection(Constant.URL_PROFILE_UPDATE+Profile.getInstance().getUserId(), ProfileCanvas.this, inner.toString(),"POST",(byte)2);
                }
            }).start();
                
            } else //if (keyCode == Constant.KEY_SOFT_LEF)
            {
               if(RIGHT_MENU ==null)
                   return; 
//                Utility.openLoginAsGuest(null);
                if(!Utility.displayPrevScreen())
                     Utility.openLoginAsGuest(null);
            }
        }
        keyCode = getGameAction(keyCode);
        if (name != null) {
            name.keyPressed(keyCode, keyCode);
        }
        if (dob != null) {
            dob.keyPressed(keyCode, keyCode);

        }
        if (address != null) {
            address.keyPressed(keyCode, keyCode);

        }
        if (mobile != null) {
            mobile.keyPressed(keyCode, keyCode);

        }
        if (aadhar != null) {
            aadhar.keyPressed(keyCode, keyCode);

        }
//        if (preferencess != null) {
//            preferencess.keyPressed(keyCode, keyCode);
//
//        }
//        if (crops != null) {
//            crops.keyPressed(keyCode, keyCode);
//
//        }
        if (pin != null) {
            pin.keyPressed(keyCode, keyCode);
//          if(name != null)
//          name.keyPressed(keyCode, keyCode);


            
        }
        if (keyCode == UP) {
            currentSelected--;
            if (currentSelected <= 0) {
//                currentSelected = totalItem;
                currentSelected++;
            }
            formHelper.screenMoveY = formHelper.screenMoveY + 40;
            if (formHelper.screenMoveY > 0) {
                formHelper.screenMoveY = 0;
            }

        } //going down
        else if (keyCode == DOWN) {
            currentSelected++;
            if (currentSelected > totalItem) {
//                currentSelected = 1;
//                formHelper.screenMoveY = 0;
//                formHelper.screenDown = false;
                currentSelected--;
            }
            if (formHelper.screenDown) {
                formHelper.screenMoveY = formHelper.screenMoveY - 40;
            }
        } else if (keyCode == RIGHT) {

        } else if (keyCode == LEFT) {

        } else if (keyCode == FIRE) {
            if (showAlert) {
                showAlert = false;
            }
            if (currentSelected == 2 && type == 1) {

            } else if (currentSelected == 2 && otpWatingTime == 0 && type == 2) {

            } else if (currentSelected == 3 && otpWatingTime == 0 && type == 2) {

            }
        }
        refresh();
    }

    public void recycleImage() {
        super.recycleImage();
        System.gc();
        CENTER_MENU = null;
        formHelper = null;
    }

    private void drawProfileScreen(Graphics g) {

        int bX = 10;
        int bY = Constant.TITLE_HEIGHT + buttonMargin + formHelper.screenMoveY;
        bY = formHelper.drawImage(g, bX, bY, getWidth(), getHeight(), image);
        int bW = getWidth() - 20;
        int bH = 20;
        bY = formHelper.drawLabel(g, bX, bY, "Enter your full name", getWidth(), getHeight(), buttonMargin, 0x00000, (byte) -1);

        if (name == null) {
            name = new TextBox("", Profile.getInstance().getName(), bX, bY, bW);
            name.setDisplay(iMandiMIDlet.display);
            name.setContainer(this);
        }
        name.setY(bY);
        bY = formHelper.drawEditField(g, bX, bY, bW, bH, currentSelected, "", getWidth(), getHeight(), buttonMargin, 0x000FF, (byte) 1, name);

        bY = formHelper.drawLabel(g, bX, bY, "Gender", getWidth(), getHeight(), buttonMargin, 0x00000, (byte) -1);
//        public int drawOther(Graphics g,int bX,int bY,int bW,int bH,byte currentSelected,String buttonTitle,int screenWidht,int screenHeight,byte buttonMargin,int buttonColor,byte myIndex){
        bY = formHelper.drawOther(g,bX,bY,bW,bH,currentSelected,Profile.getInstance().getGender(),getWidth(),getHeight(),buttonMargin, 0x00000,(byte)2,1);

        bY = formHelper.drawLabel(g, bX, bY, "Dob", getWidth(), getHeight(), buttonMargin, 0x00000, (byte) -1);

        if (dob == null) {
            dob = new TextBox(Profile.getInstance().getDob(), Profile.getInstance().getDob(), bX, bY, bW);
            dob.setDisplay(iMandiMIDlet.display);
            dob.setContainer(this);
        }
        dob.setY(bY);
        bY = formHelper.drawEditField(g, bX, bY, bW, bH, currentSelected, "", getWidth(), getHeight(), buttonMargin, 0x000FF, (byte) 3, dob);

        bY = formHelper.drawLabel(g, bX, bY, "Address", getWidth(), getHeight(), buttonMargin, 0x00000, (byte) -1);

        if (address == null) {
            address = new TextBox(Profile.getInstance().getAddress(), Profile.getInstance().getAddress(), bX, bY, bW);
            address.setDisplay(iMandiMIDlet.display);
            address.setContainer(this);
        }
        address.setY(bY);
        bY = formHelper.drawEditField(g, bX, bY, bW, bH, currentSelected, "", getWidth(), getHeight(), buttonMargin, 0x000FF, (byte) 4, address);

        bY = formHelper.drawLabel(g, bX, bY, "Pin", getWidth(), getHeight(), buttonMargin, 0x00000, (byte) -1);

        if (pin == null) {
            pin = new TextBox(Profile.getInstance().getPinCode(), Profile.getInstance().getPinCode(), bX, bY, bW,TextField.NUMERIC,6);
            pin.setDisplay(iMandiMIDlet.display);
            pin.setContainer(this);
        }
        pin.setY(bY);
        bY = formHelper.drawEditField(g, bX, bY, bW, bH, currentSelected, "", getWidth(), getHeight(), buttonMargin, 0x000FF, (byte) 5, pin);

        bY = formHelper.drawLabel(g, bX, bY, "Mobile No.", getWidth(), getHeight(), buttonMargin, 0x00000, (byte) -1);

        if (mobile == null) {
            if(Profile.getInstance().getMobileNumber().indexOf("91-") != -1)
            {
                Profile.getInstance().setMobileNumber(Constant.replace(Profile.getInstance().getMobileNumber(), "91-",""));
            }
            mobile = new TextBox(Profile.getInstance().getMobileNumber(), Profile.getInstance().getMobileNumber(), bX, bY, bW,TextField.NUMERIC,10);
            mobile.setDisplay(iMandiMIDlet.display);
            mobile.setContainer(this);
        }
        mobile.setY(bY);
        bY = formHelper.drawEditField(g, bX, bY, bW, bH, currentSelected, "", getWidth(), getHeight(), buttonMargin, 0x000FF, (byte) 6, mobile);

        bY = formHelper.drawLabel(g, bX, bY, "Aadhar No.", getWidth(), getHeight(), buttonMargin, 0x00000, (byte) -1);

        if (aadhar == null) {
            aadhar = new TextBox(Profile.getInstance().getAadharCardNumber(), Profile.getInstance().getAadharCardNumber(), bX, bY, bW,TextField.NUMERIC,12);
            aadhar.setDisplay(iMandiMIDlet.display);
            aadhar.setContainer(this);
        }
        aadhar.setY(bY);
        bY = formHelper.drawEditField(g, bX, bY, bW, bH, currentSelected, "", getWidth(), getHeight(), buttonMargin, 0x000FF, (byte) 7, aadhar);

//        bY = formHelper.drawLabel(g, bX, bY, "Preferencess", getWidth(), getHeight(), buttonMargin, 0x00000, (byte) -1);
//
//        if (preferencess == null) {
//            preferencess = new TextBox("", "", bX, bY, bW);
//            preferencess.setDisplay(iMandiMIDlet.display);
//            preferencess.setContainer(this);
//        }
//        preferencess.setY(bY);
//        bY = formHelper.drawEditField(g, bX, bY, bW, bH, currentSelected, "", getWidth(), getHeight(), buttonMargin, 0x000FF, (byte) 7, preferencess);
//
//        bY = formHelper.drawLabel(g, bX, bY, "Corps", getWidth(), getHeight(), buttonMargin, 0x00000, (byte) -1);
//
//        if (crops == null) {
//            crops = new TextBox("", "", bX, bY, bW);
//            crops.setDisplay(iMandiMIDlet.display);
//            crops.setContainer(this);
//        }
//        crops.setY(bY);
//        bY = formHelper.drawEditField(g, bX, bY, bW, bH, currentSelected, "", getWidth(), getHeight(), buttonMargin, 0x000FF, (byte) 8, crops);
    }

    public void sendData(String data, byte requestId) {

//        {"userId":167,"userName":"91983tx74e4d621","mobileNumber":"91-9818662101","countryCode":"91","userState":"active","type":"user","imei":"865800226639641020","activationDate":"Nov 3, 2017 7:07:55 PM","clientVersion":"iMandi_J2me_1_0_0","createdDate":"Oct 30, 2017 4:49:09 PM","status":"success","message":"User profile data"}

        super.sendData(data, requestId);
        if (requestId == 1) {
//            {
//    "userId": 167,
//    "userName": "91983tx74e4d621",
//    "mobileNumber": "91-9818662101",
//    "countryCode": "91",
//    "userState": "active",
//    "type": "user",
//    "name": "91983tx74e4d621",
//    "imageFileId": "imageFileId123",
//    "dob": "05-Jun-1980",
//    "gender": "male",
//    "aadharCardNumber": "666666685255",
//    "address": "",
//    "pinCode": "5895233",
//    "imei": "865800226639641020",
//    "activationDate": "Nov 5, 2017 3:09:00 PM",
//    "clientVersion": "iMandi_J2me_1_0_0",
//    "createdDate": "Oct 30, 2017 4:49:09 PM",
//    "status": "success",
//    "message": "User profile data"
//}
            Profile profile = JsonStringToOjbect.getProfile(data);
            profile.saveMe();
            name= dob= address= pin= mobile= aadhar= preferencess= crops=aadhar= null;

            if(Constant.isEmpty(profile.getMobileNumber()) || Constant.isEmpty(profile.getName()) || Constant.isEmpty(profile.getPinCode())){
                           RIGHT_MENU = null ;
            }else{
                RIGHT_MENU = "SKIP" ;
            }
                        

        }else if(requestId == 2){
            MyConnection.getInstance().makeConnection(Constant.URL_PROFILE + Profile.getInstance().getUsername(), ProfileCanvas.this, null, "GET", (byte) 1);
            CitrusErrors citrusErrors = JsonStringToOjbect.getMessage(data);
            if(citrusErrors.messageSub!=null && citrusErrors.code !=null){
                Constant.showError(citrusErrors.messageSub);
            }
            else{
                Constant.showError(citrusErrors.message);
                new Thread(new Runnable() {

                public void run() {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                     if(!Utility.displayPrevScreen())
                     Utility.openLoginAsGuest(null);
                }
            }).start();
        }
            }
        refresh();
    }
}
