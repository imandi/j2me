package com.iMandi;

import com.iMandi.bean.CartItems;
import com.iMandi.bean.Category;
import com.iMandi.bean.Grid;
import com.iMandi.bean.Product;
import com.iMandi.bean.Profile;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.MyConnection;
import java.io.IOException;
import java.util.Vector;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class GridCanvas extends ImandiCanvas {

    Vector gridVec;
    public int column = 2;
    public int padding = 50;
    int gridWidht = 0;
    int startX = 0;
    int startY = 25;
    int selected = 0;
//    int startY ;
    public int type = 1;

    public GridCanvas()  {
        Constant.printMemory();
        init();

        if(CartItems.getInstance().cartItemTable == null || CartItems.getInstance().cartItemTable.size()<=0)
        new Thread(new Runnable() {

            public void run() {
                JSONObject inner = new JSONObject();
                    try {
                        inner.put("customer_id", Profile.getInstance().getUserId());
                        MyConnection.getInstance().fetchDataPost(Constant.URL_BASE + Constant.URL_GET_CART_DATA, GridCanvas.this, inner.toString(),(byte)1);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
            }
        }).start();
         

    }

    public GridCanvas(Vector gridVec, int gridWidht, int column) {
        init();
        this.gridVec = gridVec;
        this.gridWidht = gridWidht;
        this.column = column;
    }
    public GridCanvas(Vector gridVec, int gridWidht, int column,int type) {
        this.type = type;
        init();
        this.gridVec = gridVec;
        this.gridWidht = gridWidht;
        this.column = column;
    }

    public void init() {
        try {
            gridVec = new Vector();
            if (type == 1) {
//            for (int i = 0; i < 2; i++) {
//                Grid g = new Grid("GI", "", "");
//                gridVec.addElement(g);
//            }
//                padding = 70;
//                column = 3;

//                type = 2;
        column = 3;
        padding = 15;
        
                Grid g = new Grid("Feed", "1", "Feed", Image.createImage("/voice_record.png"));
//                gridVec.addElement(g);
                g = new Grid("Market", "2", "Market", Image.createImage("/marketplace.png"));
                gridVec.addElement(g);
//                g = new Grid("Media", "2", "friends", Image.createImage("/voice_record.png"));
//                gridVec.addElement(g);
                g = new Grid("Formus", "2", "Formus", Image.createImage("/voice_record.png"));
//                gridVec.addElement(g);
                g = new Grid("Friends", "2", "Friends", Image.createImage("/voice_record.png"));
//                gridVec.addElement(g);
//                g = new Grid("Radio", "2", "friends", Image.createImage("/voice_record.png"));
//                gridVec.addElement(g);
                g = new Grid("Profile", "2", "Profile", Image.createImage("/friends.png"));
                gridVec.addElement(g);
                g = new Grid("Settings", "2", "Settings", Image.createImage("/setting.png"));
                gridVec.addElement(g);

                g = new Grid("Forums", "2", "fourm", Image.createImage("/ic_forum.png"));
                gridVec.addElement(g);

            } else if (type == 2) {
//             padding = 50;
//             column = 2;

                 column = 3;
        padding = 30;
        
                Grid g = new Grid("Fertilizers", "3", "ecom-cat", Image.createImage("/fertiliser.png"));
                gridVec.addElement(g);
                g = new Grid("Pesticides", "4", "ecom-cat", Image.createImage("/pesticides.png"));
                gridVec.addElement(g);
                g = new Grid("Seeds", "11", "ecom-cat", Image.createImage("/seeds.png"));
                gridVec.addElement(g);
                            
            }
            else if (type == 3) {
                column = 1;
                padding = 15;

                Grid g = new Grid("Soil management", "1", "fourm-chat", Image.createImage("/soil_1.png"));
                gridVec.addElement(g);
//                g = new Grid("Fourm-1", "2", "fourm-chat", Image.createImage("/pesticides.png"));
//                gridVec.addElement(g);
//                g = new Grid("Fourm-1", "3", "fourm-chat", Image.createImage("/seeds.png"));
//                gridVec.addElement(g);
            }

            gridWidht = 50 + padding;//space/column ;
            int spaceOcopied = gridWidht * column;//(((gridWidht + padding) * (column-1)))+gridWidht;
            if (column == 1) {
                spaceOcopied = gridWidht;
            }
//            Constant.printLog("GridCanvas>", "spaceOcopied:" + spaceOcopied);
//            Constant.printLog("GridCanvas>", "gridWidht:" + gridWidht);
//            Constant.printLog("GridCanvas>", "screen widht:" + getWidth());
            startX = (getWidth() - spaceOcopied) / 2;
//            Constant.printLog("GridCanvas>", "startX:" + startX);
        } catch (Exception e) {
            Constant.printLog("GridCanvas>init>ERROR::", e.getMessage());
        }
        refresh();
    }

    protected void paint(Graphics g) {
        super.paint(g);
        int y = Constant.TITLE_HEIGHT + startY + screenMoveY;
//        int lastY = y;
        int index = 0;
        for (int i = 0; i < gridVec.size(); i++) {
            g.setColor(0xf9b520);
//            Constant.printLog("------->", "" + (((startX + (gridWidht * index)) + (padding / 2)) + 3));
            //g.fillRect(((startX + (gridWidht * index)) + (padding / 2))+3, (y - (padding / 2))+3, gridWidht-3, gridWidht-3);
//            g.fillRoundRect(((startX + (gridWidht * index)) + (0)) + 3, (y - (0)) + 3, gridWidht - 3, gridWidht - 3, gridWidht - 3, gridWidht - 3);

            g.drawImage(((Grid) gridVec.elementAt(i)).image, ((startX + (gridWidht * index)) + (0)) + 3+(padding/2), (y - (0)) + 3, Graphics.TOP | Graphics.LEFT);
            if (selected == i) {
                g.setColor(Constant.COLOR_BLACK);
                g.drawRoundRect((startX + (gridWidht * index)) + (0)+(padding/2), y - (0), (gridWidht-padding)+6, (gridWidht-padding)+6, (gridWidht-padding)+6, (gridWidht-padding)+6);
//                g.drawRoundRect((startX + 1+(gridWidht * index)) + (0), y - (1), (gridWidht-padding)+5, (gridWidht-padding)+5, (gridWidht-padding)+5, (gridWidht-padding)+5);
//                g.drawRect((startX + (gridWidht * index)) + (padding / 2), y - (padding / 2), gridWidht, gridWidht);

                if ((y+gridWidht+gridWidht) > getHeight() - Constant.HEIGHT_BOTTOM) {
                    screenDown = true;
                } else {
                    screenDown = false;
                }
            }
//            Constant.printLog("screenDown:", ""+screenDown);
            g.setColor(Constant.COLOR_BLACK);
//            getPrevFont();
//            g.setFont(Constant.font_small);
            g.drawString(((Grid) gridVec.elementAt(i)).title, (startX + (gridWidht * index)) + (0) + ((gridWidht - g.getFont().stringWidth(((Grid) gridVec.elementAt(i)).title)) / 2), ((y - (0)) + (gridWidht-padding)) - g.getFont().getHeight() + Constant.TITLE_HEIGHT, 20);

            index++;
            if (index == column) {
                y = y + Constant.TITLE_HEIGHT + Constant.TITLE_HEIGHT + padding+10;
//                lastY = y;
                index = 0;
            }
//            setPrevFont();
        }
        drawTitle(title);
        drawBottomMenu();
    }

    protected void keyPressed(int key) {
        super.keyPressed(key);

        int keyCode = getGameAction(key);
        if (keyCode == Canvas.UP || keyCode == Canvas.LEFT) {
            selected--;
            if (selected < 0) {
                selected = gridVec.size() - 1;
            }
            screenMoveY = screenMoveY + 100;
            if (screenMoveY > 0) {
                screenMoveY = 0;
            }
        } else if (keyCode == Canvas.DOWN || keyCode == Canvas.RIGHT) {
            selected++;
            if (selected > gridVec.size() - 1) {
                selected = 0;
            }
            if (screenDown) {
                screenMoveY = screenMoveY - 100;
            }

        } else if (keyCode == Canvas.FIRE) {

            if (((Grid) gridVec.elementAt(selected)).action.equalsIgnoreCase("market")) {
                Utility.openMarketPlaceScreen(this);
            } else if (((Grid) gridVec.elementAt(selected)).action.equalsIgnoreCase("Profile")) {
                Utility.openProfileScreen(this);
            } else if (((Grid) gridVec.elementAt(selected)).action.equalsIgnoreCase("ecom-cat")) {
                Category category = new Category();
                category.setCategoryid(((Grid) gridVec.elementAt(selected)).id);
                Utility.openProductList(this, category);
//                Utility.openProfileScreen(this);
            }
            else if (((Grid) gridVec.elementAt(selected)).action.equalsIgnoreCase("fourm")) {
                Category category = new Category();
                category.setCategoryid(((Grid) gridVec.elementAt(selected)).id);
                Utility.openFourmCategory(this, category);
            }
             else if (((Grid) gridVec.elementAt(selected)).action.equalsIgnoreCase("fourm-chat")) {
                Category category = new Category();
                category.setCategoryid(((Grid) gridVec.elementAt(selected)).id);
                Utility.openFourmChat(this, category);
            }
            else {
//                Utility.openFriendScreen(this);
                Constant.showError(Constant.TO_DO);
//                iMandiMIDlet.display.setCurrent(new ProductCanvas());

            }
        }
        refresh();
        repaint();
        recycleImage();
        init();
    }

    public void sendData(String data,byte requestId) {
isLoading= false;
        if(requestId == 1){
            System.out.println("getCartProductList>sendData:" + data);
            Vector productList = JsonStringToOjbect.getCartProductList(data);
            CartItems.getInstance().cartItemTable = productList;
            System.out.println("getCartProductList -->" + productList.toString());
            CartItems.getInstance().cartItemTable = productList;
        }
        refresh();
    }
}
