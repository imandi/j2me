package com.iMandi;


import com.iMandi.bean.CartItems;
import com.iMandi.bean.Product;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;

/**
 *
 * @author nagendrakumar
 */
public class ChoiceGroupView implements CommandListener{
    private Command add, cancel;
    Canvas prevDisplay;
    Product product ;
    ChoiceGroup quentity;
public void showDropDown(Canvas prevDisplay,String[] choice,String title,Product product)
    {
    this.product=product;
    this.prevDisplay = prevDisplay ;
        Form form = new Form("Choice Form");

        cancel = new Command("Back", Command.CANCEL, 2);
        add = new Command("Select", Command.OK, 2);


        //ChoiceGroup(label,type,elements,image)
        quentity = new ChoiceGroup(title, Choice.POPUP,
                               choice, null);
        quentity.setSelectedIndex(product.getProductCartQuentity(), true) ;
        form.append(quentity);
        form.addCommand(cancel);
        form.addCommand(add);
        form.setCommandListener(this);

        iMandiMIDlet.display.setCurrent(form);
//        product.setProductQuentity(product.getProductQuentity()+1);
    }

    public void commandAction(Command c, Displayable d) {
       
        product.setProductCartQuentity(Integer.parseInt(quentity.getString(quentity.getSelectedIndex())));

        if(product.getProductCartQuentity()>0){
            CartItems.getInstance().addItem(product,null);
        }
        else{
//             CartItems.getInstance().removeItem(product);
        }
        iMandiMIDlet.display.setCurrent(prevDisplay);
    }
}
