package com.iMandi;

import com.iMandi.bean.CartItems;
import com.iMandi.bean.Product;
import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class ItemDetailView {

    public void diaplayDetail(Graphics g, Canvas canvas, Product product, int startY) {
        try {
            Constant.printLog("diaplayDetail>", product.toString());
            int width = canvas.getWidth();
            int height = canvas.getHeight();
//            product.setProductDesc("Works alongside The Director of Technology Programs to develop new course content for Spring 2018 Designs and develops the Web Development course that is 100% up to date, meeting the latest industry standards Skills up new students who have not had prior Web Development experience before using the latest technologies and design tools Manages and delivers the course materials to students in an engaging thought-provoking manner in our online learning management system: Canvas Optimize and be flexible to change the course to suit the needs of your students and new technologies semester after semester Engage in meaningful online conversations and dialogue with your students online in the learning management system, supporting their learning objectives and career goals Diligently respond to students needs and questions during the course schedule Acts as a technical mentor guiding students on a technical journey of discovery");

            if(product.getProductDesc() == null || product.getProductDesc().trim().length()<=0 ){
                product.setProductDesc("description not available");
            }
            String s[] = CanvasProductList.getTextRows(product.getProductDesc(), Constant.font_small, canvas.getWidth()-20);

            int image_width_area = 100;
            int image_height_area = 75;
            Image image = ImageList.loading;
            if (ImageList.images.size() > 0) {
                image = (Image) ImageList.images.elementAt(0);
//                if(CanvasProductList.detailMaxY>0)
//                CanvasProductList.detailMaxY = 0;
            }

            g.setClip(3, (Constant.TITLE_HEIGHT + 10)+(startY), image_width_area, image_height_area);
            g.drawImage(image, 3, (Constant.TITLE_HEIGHT + 10)+(startY), 20);
            g.setColor(0);
            g.setClip(0, 0, canvas.getWidth(), canvas.getHeight());


            int y = (Constant.TITLE_HEIGHT + 10 + image.getHeight())+(startY);
            int lastY = y;

            ((ImandiCanvas)canvas).getPrevFont();
            g.setFont(Constant.font_medium_bold);
            g.drawString(product.getProductName(), 5+image_width_area,  startY+(Constant.TITLE_HEIGHT + 10), Graphics.TOP | Graphics.LEFT);
            y = lastY + 20;
            g.drawString(product.getProductPrice(), 5+image_width_area,startY+ (Constant.TITLE_HEIGHT + 10)+(image_height_area-(g.getFont().getHeight()*2)), Graphics.TOP | Graphics.LEFT);
            y = lastY + 20;
            g.drawString(product.getProductSku(), 5+image_width_area, lastY=startY+(Constant.TITLE_HEIGHT + 10)+(image_height_area-g.getFont().getHeight()), Graphics.TOP | Graphics.LEFT);
            y = lastY + 30;

            g.drawString("Product Detail", 10, lastY = y, Graphics.TOP | Graphics.LEFT);
            ((ImandiCanvas)canvas).setPrevFont();
            y = lastY + 23;
//          g.drawString("Name: "+product.getProductName(), 5,lastY+y, Graphics.TOP | Graphics.LEFT);
//          y = lastY+20;
//          g.drawString("Category: "+product.getProductCategory(), 5,lastY+y, Graphics.TOP | Graphics.LEFT);
//          y = lastY+20;
//          g.drawString("Price: "+product.getProductPrice(), 5,lastY+y, Graphics.TOP | Graphics.LEFT);
//          y = lastY+20;
//          g.drawString("StockStatus: "+product.getProductStockStatus(), 5,lastY+y, Graphics.TOP | Graphics.LEFT);
//          y = lastY+20;

//          g.drawString(product.getProduct, 5,lastY+y, Graphics.TOP | Graphics.LEFT);

            g.setColor(Constant.COLOR_BORDER);
            g.drawRoundRect(8, lastY=y-2, canvas.getWidth()-16, g.getFont().getHeight()*s.length,5,5);

            g.setColor(Constant.COLOR_BLACK);
            for (int j = 0; j < s.length; j++) {
                if(s[j] != null && !s[j].trim().equalsIgnoreCase("null"))
                g.drawString(s[j], 12, lastY = y + (j * Constant.font_small.getHeight()), Graphics.TOP | Graphics.LEFT);
                else
                g.drawString("NA", 12, lastY = y + (j * Constant.font_small.getHeight()), Graphics.TOP | Graphics.LEFT);
            }
            y = lastY + 20;
            if(y>canvas.getHeight()-Constant.HEIGHT_BOTTOM) {
               ( (ImandiCanvas)canvas).screenDown = true;
            }
            else
                ( (ImandiCanvas)canvas).screenDown = false;

            g.setColor(Constant.COLOR_BOTTOM);
//        if(bottomMenu)
//            g.setColor(Constant.COLOR_TAB_SELECTED);


            g.fillRect(0, canvas.getHeight() - (Constant.HEIGHT_BOTTOM), canvas.getWidth(), Constant.HEIGHT_BOTTOM);


            g.setColor(Constant.COLOR_BLACK);
//            g.drawString("Add to cart", 5,lastY=y+(0*CanvasProductList.font.getHeight()), Graphics.TOP | Graphics.LEFT);

//          for(int j = 0; j < s.length; j++)
//			{
//
//                        	g.drawString(s[j], 5,lastY=y+(j*CanvasProductList.font.getHeight()), Graphics.TOP | Graphics.LEFT);
//			}

            g.drawString("Back", 2, canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
//           if(CartItems.getInstance().getItem(product) != null){
//                        g.drawString("Remove from cart", (canvas.getWidth()/2)-(g.getFont().stringWidth("Remove from cart")/2), canvas.getHeight() - 2,Graphics.BOTTOM|Graphics.LEFT);
//           }else
            g.drawString("Add to cart", (canvas.getWidth() / 2) - (g.getFont().stringWidth("Add to cart") / 2), canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);

            if (CartItems.getInstance().cartItemTable.size() > 0 /*&& 1==2*/) {
                g.drawString("Cart[" + CartItems.getInstance().cartItemTable.size() + "]", canvas.getWidth() - g.getFont().stringWidth("Cart[" + CartItems.getInstance().cartItemTable.size() + "]"), canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
            }
            else
            {
                g.drawString("Cart", canvas.getWidth() - g.getFont().stringWidth("Cack"), canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
            }

        } catch (Exception ex) {
            g.setColor(0xffffff);
            g.drawString("Failed to load image!", 0, 0, 17);
            return;
        }

    }
    public void diaplayDetail2(Graphics g, Canvas canvas, Product product, int startY) {
        try {
            Constant.printLog("diaplayDetail>", product.toString());
            int width = canvas.getWidth();
            int height = canvas.getHeight();
            String s[] = CanvasProductList.getTextRows(product.getProductDesc(), Constant.font_small, canvas.getWidth());

            Image image = ImageList.loading;
            if (ImageList.images.size() > 0) {
                image = (Image) ImageList.images.elementAt(0);
//                if(CanvasProductList.detailMaxY>0)
//                CanvasProductList.detailMaxY = 0;
            }
            g.drawImage(image, width / 2, (Constant.TITLE_HEIGHT + 10)+(startY), 17);
            g.setColor(0);

            int y = (Constant.TITLE_HEIGHT + 10 + image.getHeight())+(startY);
            int lastY = y;

            g.drawString("Suk: " + product.getProductSku(), 5, lastY, Graphics.TOP | Graphics.LEFT);
            y = lastY + 20;
            g.drawString("Price: " + product.getProductPrice(), 5, lastY=y, Graphics.TOP | Graphics.LEFT);
            y = lastY + 20;
            g.drawString("Description:", 5, lastY = y, Graphics.TOP | Graphics.LEFT);
            y = lastY + 20;
//          g.drawString("Name: "+product.getProductName(), 5,lastY+y, Graphics.TOP | Graphics.LEFT);
//          y = lastY+20;
//          g.drawString("Category: "+product.getProductCategory(), 5,lastY+y, Graphics.TOP | Graphics.LEFT);
//          y = lastY+20;
//          g.drawString("Price: "+product.getProductPrice(), 5,lastY+y, Graphics.TOP | Graphics.LEFT);
//          y = lastY+20;
//          g.drawString("StockStatus: "+product.getProductStockStatus(), 5,lastY+y, Graphics.TOP | Graphics.LEFT);
//          y = lastY+20;

//          g.drawString(product.getProduct, 5,lastY+y, Graphics.TOP | Graphics.LEFT);


            for (int j = 0; j < s.length; j++) {
                if(s[j] != null && !s[j].trim().equalsIgnoreCase("null"))
                g.drawString(s[j], 5, lastY = y + (j * Constant.font_small.getHeight()), Graphics.TOP | Graphics.LEFT);
                else
                g.drawString("NA", 5, lastY = y + (j * Constant.font_small.getHeight()), Graphics.TOP | Graphics.LEFT);
            }
            y = lastY + 20;
            if(y>canvas.getHeight()-Constant.HEIGHT_BOTTOM) {
               ( (ImandiCanvas)canvas).screenDown = true;
            }
            else
                ( (ImandiCanvas)canvas).screenDown = false;

            g.setColor(Constant.COLOR_BOTTOM);
//        if(bottomMenu)
//            g.setColor(Constant.COLOR_TAB_SELECTED);


            g.fillRect(0, canvas.getHeight() - (Constant.HEIGHT_BOTTOM), canvas.getWidth(), Constant.HEIGHT_BOTTOM);


            g.setColor(Constant.COLOR_MENU_TEXT);
//            g.drawString("Add to cart", 5,lastY=y+(0*CanvasProductList.font.getHeight()), Graphics.TOP | Graphics.LEFT);

//          for(int j = 0; j < s.length; j++)
//			{
//
//                        	g.drawString(s[j], 5,lastY=y+(j*CanvasProductList.font.getHeight()), Graphics.TOP | Graphics.LEFT);
//			}

            g.drawString("Back", 2, canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
//           if(CartItems.getInstance().getItem(product) != null){
//                        g.drawString("Remove from cart", (canvas.getWidth()/2)-(g.getFont().stringWidth("Remove from cart")/2), canvas.getHeight() - 2,Graphics.BOTTOM|Graphics.LEFT);
//           }else
            g.drawString("Add to cart", (canvas.getWidth() / 2) - (g.getFont().stringWidth("Add to cart") / 2), canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);

            if (CartItems.getInstance().cartItemTable.size() > 0 /*&& 1==2*/) {
                g.drawString("Cart[" + CartItems.getInstance().cartItemTable.size() + "]", canvas.getWidth() - g.getFont().stringWidth("Cart[" + CartItems.getInstance().cartItemTable.size() + "]"), canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
            }
            else
            {
                g.drawString("Cart", canvas.getWidth() - g.getFont().stringWidth("Cack"), canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
            }

        } catch (Exception ex) {
            g.setColor(0xffffff);
            g.drawString("Failed to load image!", 0, 0, 17);
            return;
        }

    }
}
