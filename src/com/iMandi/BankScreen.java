package com.iMandi;

import com.iMandi.bean.Bank;
import com.iMandi.bean.CartItems;
import com.iMandi.bean.Category;
import java.util.Vector;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class BankScreen extends ImandiCanvas {

    BankScreen canvas;
    BankList bankList;
    AddressList addressList;
    byte type = 1;//address 2 is bank

    public BankScreen() {
        LEFT_MENU = "Back";
        CENTER_MENU = "Select";
        canvas = this;
        setFullScreenMode(true);

        Bank bank = new Bank();
        bank.setBankid("1");
        bank.setBankname(CartItems.getInstance().pick_location) ;

        Vector addressVect = new Vector();
        addressVect.addElement(bank);
//         if("This product is not available in your location");
//        bank = new Bank();
//        bank.setBankid("1");
//        bank.setBankname("Nagendra Kumar, Priyanka Apartment, 204, wing B, opposite Akurti Star building, Andheri East, MIDC, Mumbai, Maharastra,400059,mobile no.9818662101") ;
//        addressVect.addElement(bank);


        bankList = new BankList("", Constant.bankList, canvas);
        addressList = new AddressList("", addressVect, canvas);
    }

    protected void paint(Graphics g) {
        super.paint(g);

        g.setFont(Constant.font_small);

        g.setColor(255, 255, 255);

        if(type==2){
             bankList.paint(g, 20);
             title=Constant.BANK_SCREEN_TITLE;
             
        }
        else if(type==1){
            addressList.paint(g, 20);
            title="Checkout: Shipping Address";
        }


        if(isLoading)
            Constant.loading(g, this, title);
        drawTitle(title);
        drawBottomMenu();
//        drawTitle(g);
//        drawBottom(g);
    }
    
    protected void keyPressed(int keyCode) {
//        super.keyPressed(keyCode);
        if(type==2)
        bankList.keyPressed(keyCode);
        else if(type==1)
            addressList.keyPressed(keyCode);
        refresh();
    }

    public void sendData(String data,byte requestID) {
        super.sendData(data,requestID);
    }

}
