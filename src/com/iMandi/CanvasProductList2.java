package com.iMandi;

import com.iMandi.bean.CartItems;
import com.iMandi.bean.Product;
import java.util.Vector;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

public class CanvasProductList2 //extends Canvas
{

    protected int linePadding = 2;
    protected int topSpace = 0;
    protected int margin = 2;
    protected int padding = 2;
    public static Font font = Font.getDefaultFont();
    boolean showDetail = false;
    protected int borderWidth = 3;
    // will contain item splitted lines
    String[][] itemLines = null;
    // will hold items image parts
    Image[] images = null;
    // will hold selected item index
    public int selectedItem = 10;
    // these will hold item graphical properties
    int[] itemsTop = null;
    int[] itemsHeight = null;
    // these will hold List vertical scrolling
    int scrollTop = 0;
    final int SCROLL_STEP = 40;
    Image voiceRecord = null;
    Image videoRecord = null;
    Image product = null;
    private ProductCanvas canvas;
    boolean rowMenu = false;
    int rowMenuIndex = 0;
    int maxCommandInRow = 2;
//	public CanvasProductList(String title, String[] items, Image[] imageElements,TabMenuCanvas canvas)
//	{
//            this.canvas = canvas;
//
//            try{
//                    voiceRecord = Image.createImage("voice_record.png");
//                    videoRecord = Image.createImage("video_record.jpeg");
//                            }
//catch(Exception e)
//{
//	e.printStackTrace();
//}
//
////		canvas.setTitle(title);
//
////		this.images = imageElements;
//
//		itemLines = new String[items.length][];
//
//		itemsTop = new int[itemLines.length];
//		itemsHeight = new int[itemLines.length];
//
//		for(int i = 0; i < itemLines.length; i++)
//		{
//			// get image part of this item, if available
//			Image imagePart =product;// getImage(i);
//
//			// get avaiable width for text
//			int w = getItemWidth() - (imagePart != null ? imagePart.getWidth() + padding : 0);
//
//			// and split item text into text rows, to fit available width
//			itemLines[i] = getTextRows((String) items[i], font, w);
//		}
//	}
    Vector products;

    public CanvasProductList2(String title, Vector products, ProductCanvas canvas) {
        this.products = products;
        this.canvas = canvas;

        try {
            voiceRecord = Image.createImage("voice_record.png");
            videoRecord = Image.createImage("video_record.jpeg");
            product = Image.createImage("default.png");
        } catch (Exception e) {
            e.printStackTrace();
        }

//		canvas.setTitle(title);

//		this.images = imageElements;

        itemLines = new String[products.size()][];

        itemsTop = new int[products.size()];
        itemsHeight = new int[products.size()];

        for (int i = 0; i < itemLines.length; i++) {
            // get image part of this item, if available
            Image imagePart = product;//getImage(i);

            // get avaiable width for text
            int w = getItemWidth() - (imagePart != null ? imagePart.getWidth() + padding : 0);

            // and split item text into text rows, to fit available width
            itemLines[i] = getTextRows((String) ((Product) products.elementAt(i)).getProductName(), font, w);
        }
    }

    public int getItemWidth() {
        return canvas.getWidth() - 2 * borderWidth - 2 * padding - 2 * margin;
    }

    public void keyPressed(int key) {
//        if (!canvas.isKnownKey(canvas, key)) {
//         if (key == Constant.KEY_SOFT_LEFT){
////            fireLeftSoftCommand();
//             if(showDetail){
//                 showDetail = false;
//                 canvas.repaint();
//             }
//         }
//         else{
////            fireRightSoftCommand();
//         }
//      }
        int keyCode = canvas.getGameAction(key);

        // is there 1 item at least?
        if (itemLines.length > 0) {
            // going up
            if (keyCode == Canvas.UP) {
                // current item is clipped on top, so can scroll up
                if (itemsTop[selectedItem] < scrollTop) {
                    scrollTop -= SCROLL_STEP;

                    canvas.repaint();
                } // is there a previous item?
                else if (selectedItem > 0) {
                    selectedItem--;

                    canvas.repaint();
                }
                rowMenuIndex = 0;
                rowMenu = false;
            } //going down
            else if (keyCode == Canvas.DOWN) {
                // current item is clipped on bottom, so can scroll down
                if (itemsTop[selectedItem] + itemsHeight[selectedItem] >= scrollTop + canvas.getHeight()) {
                    scrollTop += SCROLL_STEP;

                    canvas.repaint();
                } // is there a following item?
                else if (selectedItem < itemLines.length - 1) {
                    selectedItem++;

                    canvas.repaint();
                }
                rowMenu = false;
                rowMenuIndex = 0;
            } else if (keyCode == Canvas.RIGHT) {
                rowMenu = true;
                rowMenuIndex++;
                if (rowMenuIndex > maxCommandInRow) {
                    rowMenuIndex = 1;
                }
            } else if (keyCode == Canvas.LEFT) {
                if (rowMenu) {
                    rowMenuIndex--;
                    if (rowMenuIndex == 0);
                    rowMenuIndex = 1;
                }
            } else if (keyCode == Canvas.FIRE) {
                if (rowMenu) {
                    if(rowMenuIndex==1){
                        PlayVideo playVideo = new PlayVideo(canvas,"/test.3gp",(Product)products.elementAt(selectedItem));
                    }else if(rowMenuIndex==2){
                        PlayAudio playAudio = new PlayAudio(canvas,"/test.amr",(Product)products.elementAt(selectedItem));
                    }
//                    CartItems.getInstance().addItem(products.elementAt(selectedItem));
//                    Alert alert = new Alert("Info", "Item Added:" + selectedItem + "  :" + products.elementAt(selectedItem).toString(), null, AlertType.INFO);
//                    alert.setTimeout(3000);    // for 3 seconds
//                    HelloMIDlet.display.setCurrent(alert, canvas);    // so that it goes to back to your canvas after displaying the alert
                } else {
//                                   (new ProfileScreen(canvas)).display();
                    showDetail = true;
                    canvas.repaint();
                }
            }
        }
    }

//	Image getImage(int index)
//	{
//		return images != null && images.length > index ? images[index] : null;
//	}
    protected void paint(Graphics g) {

        // paint List background
        g.setColor(Constant.COLOR_bgColor);
        g.fillRect(0, 20, canvas.getWidth(), canvas.getHeight());

        if (showDetail) {
//            canvas.detailView.diaplayDetail(g, canvas);
            return;
        }

        // translate accordingly to current List vertical scroll
        g.translate(0, -scrollTop);

        int top = 20;

        g.setFont(font);

        // loop List items
        for (int i = 0; i < itemLines.length; i++) {
            int itemRows = itemLines[i].length;

            Image imagePart = product;//imgetImage(i);

            int itemHeight = itemRows * font.getHeight() + linePadding * (itemRows - 1);

            itemsTop[i] = top;
            itemsHeight[i] = itemHeight;

            // is image part higher than the text part?
            if (imagePart != null && imagePart.getHeight() > itemHeight) {
                itemHeight = imagePart.getHeight();
            }
            itemHeight += 2 * padding + 2 * borderWidth;

            g.translate(0, top);

            if (borderWidth > 0) {
                // paint item border
                g.setColor(i == selectedItem ? Constant.COLOR_borderSelectedColor : Constant.COLOR_borderColor);
                g.fillRect(margin, margin + topSpace, canvas.getWidth() - 2 * margin, itemHeight);
            }

            // paint item background
            g.setColor(i == selectedItem ? Constant.COLOR_backSelectedColor : Constant.COLOR_backColor);
            g.fillRect(margin + borderWidth, margin + borderWidth + topSpace, canvas.getWidth() - 2 * margin - 2 * borderWidth, itemHeight - 2 * borderWidth);

            // has this item an image part?
            if (imagePart != null) {
                g.drawImage(imagePart, margin + borderWidth + padding, margin + borderWidth + padding + topSpace, Graphics.TOP | Graphics.LEFT);
            }

            // paint item text rows
            g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

            int textLeft = margin + borderWidth + padding + (imagePart != null ? imagePart.getWidth() + padding : 0);

            int nextRow = 0;
            for (int j = 0; j < itemRows; j++) {

                nextRow = 0;
                g.drawString(itemLines[i][j] + rowMenuIndex, textLeft, topSpace + margin + borderWidth + padding + j * (linePadding + font.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
                nextRow = nextRow + font.getHeight();
                g.drawString(itemLines[i][j], textLeft, topSpace + margin + borderWidth + padding + j * (linePadding + font.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
                nextRow = nextRow + font.getHeight();
                g.drawString(itemLines[i][j], textLeft, topSpace + margin + borderWidth + padding + j * (linePadding + font.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
            }

            for (int j = 0; j < itemRows; j++) {

                nextRow = 0;
                g.drawImage(voiceRecord, (canvas.getWidth() - voiceRecord.getWidth()) - 20, topSpace + margin + borderWidth + padding + j * (linePadding + font.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
                nextRow = nextRow + voiceRecord.getHeight() + voiceRecord.getHeight();
                g.drawImage(voiceRecord, (canvas.getWidth() - voiceRecord.getWidth()) - 20, topSpace + margin + borderWidth + padding + j * (linePadding + font.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
                if (rowMenu && i == selectedItem) {
                    if (rowMenuIndex == 1) {
                        nextRow = 0;
                    }
                    if (rowMenuIndex == 2) {
                        nextRow = voiceRecord.getHeight() + voiceRecord.getHeight();
                    }

                    g.setColor(Constant.COLOR_borderSelectedColor);
                    g.drawRect((canvas.getWidth() - voiceRecord.getWidth()) - 20, topSpace + margin + borderWidth + padding + j * (linePadding + font.getHeight()) + nextRow, voiceRecord.getWidth(), voiceRecord.getHeight());
                }
            }

            g.translate(0, -top);

            top += itemHeight + 2 * margin;
        }
        // finally, translate back
        g.translate(0, scrollTop);
    }

    public static String[] getTextRows(String text, Font font, int width) {
        char SPACE_CHAR = ' ';
        String VOID_STRING = "";

        int prevIndex = 0;
        int currIndex = text.indexOf(SPACE_CHAR);

        Vector rowsVector = new Vector();

        StringBuffer stringBuffer = new StringBuffer();

        String currentToken;

        String currentRowText = VOID_STRING;

        while (prevIndex != -1) {
            int startCharIndex = prevIndex == 0 ? prevIndex : prevIndex + 1;

            if (currIndex != -1) {
                currentToken = text.substring(startCharIndex, currIndex);
            } else {
                currentToken = text.substring(startCharIndex);
            }

            prevIndex = currIndex;

            currIndex = text.indexOf(SPACE_CHAR, prevIndex + 1);

            if (currentToken.length() == 0) {
                continue;
            }

            if (stringBuffer.length() > 0) {
                stringBuffer.append(SPACE_CHAR);
            }

            stringBuffer.append(currentToken);

            if (font.stringWidth(stringBuffer.toString()) > width) {
                if (currentRowText.length() > 0) {
                    rowsVector.addElement(currentRowText);
                }
                stringBuffer.setLength(0);

                currentRowText = VOID_STRING;

                stringBuffer.append(currentToken);

                currentRowText = stringBuffer.toString();
            } else {
                currentRowText = stringBuffer.toString();
            }
        }
        if (currentRowText.length() > 0) {
            rowsVector.addElement(currentRowText);
        }
        String[] rowsArray = new String[rowsVector.size()];

        rowsVector.copyInto(rowsArray);

        return rowsArray;
    }
    public void recycleImage(){

    }
}
