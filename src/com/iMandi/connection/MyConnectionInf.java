package com.iMandi.connection;

public interface MyConnectionInf {
	public void sendData(String data);
	public void sendStatus(String Status);
	public void myConnectionBusy(String Status);

        public void sendData(String data,byte requestID);
	public void sendStatus(String Status,byte requestID);
	public void myConnectionBusy(String Status,byte requestID);
        
        public void imageLoaded(String Status,byte requestID);
}
