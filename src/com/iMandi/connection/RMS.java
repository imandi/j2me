package com.iMandi.connection;

import com.iMandi.Constant;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import javax.microedition.io.Connector;
import javax.microedition.io.file.FileConnection;
import javax.microedition.io.file.FileSystemRegistry;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Image;
import javax.microedition.rms.InvalidRecordIDException;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
import javax.microedition.rms.RecordStoreFullException;
import javax.microedition.rms.RecordStoreNotOpenException;

public class RMS {

	private RecordStore rs = null;  
	static final String REC_STORE = "settings_v6";
//	int MAX_SETTING_INDEX = 6;
        public static int STATE = 1;
        public static int PROFILE = 2;
        
	static RMS rms ;
	public static RMS getInstance(){
		if(rms==null){
			rms = new RMS();
                        rms.open();
                        rms.initRMS();
		}
		try
	    {
			
	    }
	    catch (Exception e)
	    {
	    	e.printStackTrace();
	    }
                
		return rms; 
	}
        void open(){
        try {
           rs =  RecordStore.openRecordStore(REC_STORE, true);
        } catch (RecordStoreException ex) {
            ex.printStackTrace();
        }
        }
        void close(){
        try {
            rs.closeRecordStore();
        } catch (RecordStoreException ex) {
            ex.printStackTrace();
        }
        }
	public void initRMS(){
		try
	    {
                    	try
	    {
		   System.out.println("RMS---getNumRecords--------->"+rs.getNumRecords());
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        
		  if(rs.getNumRecords()<=0){
			  System.out.println("Init RMS with Default Value......");	
		      rs.addRecord("IMANDI".getBytes(), 0, "IMANDI".getBytes().length);//setting
		      rs.addRecord("IMANDI".getBytes(), 0, "IMANDI".getBytes().length);///profile
		      rs.addRecord("IMANDI".getBytes(), 0, "IMANDI".getBytes().length);///cart
		      rs.addRecord("IMANDI".getBytes(), 0, "IMANDI".getBytes().length);//
		      rs.addRecord("IMANDI".getBytes(), 0, "IMANDI".getBytes().length);
//                      for (int i = 0; i < 50; i++) {
//                          rs.addRecord("".getBytes(), 0, "".getBytes().length);
//                      }
		  }
close();
	    }
	    catch (Exception e)
	    {
	    	e.printStackTrace();
//	      db("write " + e.toString());
	    }

	}
	public void setRecord(String data,int type){
		try {

                    open();
                      System.out.println("RMS---getNumRecords--------->"+rs.getNumRecords());
                        Constant.printLog("RMS:setRecord>data>", data);
                         Constant.printLog("RMS:setRecord>type", ""+type);
//			rs.setRecord(type,data.getBytes(), 0, data.getBytes().length);
                          rs.setRecord(type,data.getBytes(), 0, data.getBytes().length);//setting
                        close();
		} catch (InvalidRecordIDException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RecordStoreFullException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RecordStoreNotOpenException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RecordStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
        public void saveImage(byte [] imageData,int index){
//           try {
//               open();
//			rs.setRecord(index,imageData, 0, imageData.length);
//                        close();
//		} catch (InvalidRecordIDException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (RecordStoreFullException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (RecordStoreNotOpenException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (RecordStoreException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        }
	public String getRecord(int index){
		try {
                    open();

                       System.out.println("RMS---getRecord--------->"+rs.getNumRecords());
                    byte[] recData = new byte[5];
//                    recData = new byte[rs.getRecordSize(index)];
                    recData = rs.getRecord(index);//, recData, 0);
  
                    Constant.printLog("RMS:setRecord>type", ""+index);
			close();
                        return new String(recData);
                        
                }catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
                return null;
	}
//        public Image getImage(int index){
//		try {
//                    open();
//                        byte d[] = rs.getRecord(index) ;
//			Image img = Image.createImage(d, 0, d.length);
//                        d=null;
//                        close();
//                        return img ;
//                }catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//                return null;
//	}

    public static  String readFile(String path)
    {
        InputStream is = null;
        FileConnection fc = null;
        String str = "";
          StringBuffer sb = new StringBuffer();

        try
        {
                         Enumeration e = FileSystemRegistry.listRoots();
            while (e.hasMoreElements()) {
                String root = (String) e.nextElement();
                sb.append(root);
            }

             //static String fname="file:///c:/logfile.txt";
            fc = (FileConnection)Connector.open("file:///"+sb.toString()+"aaa/myfile.txt", Connector.READ_WRITE);

            if(fc.exists())
            {
                int size = (int)fc.fileSize();
                is= fc.openInputStream();
                byte bytes[] = new byte[size];
                is.read(bytes, 0, size);
                str = new String(bytes, 0, size);
            }
        }
        catch (IOException ioe)
        {
//        Alert error = new Alert("Error", ioe.getMessage(), null, AlertType.INFO);
//        error.setTimeout(1212313123);
//        Display.getDisplay(main).setCurrent(error);
        }
        finally
        {
            try
            {
                if (null != is)
                    is.close();
                if (null != fc)
                    fc.close();
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
                return e.getMessage()+"  "+"file:///"+sb.toString()+"aaa/myfile.txt";
            }
        }
        return str;
    }

    public static void writeTextFile(String fName, String text)
    {
        OutputStream os = null;
        FileConnection fconn = null;
        try
        {
                            Enumeration e = FileSystemRegistry.listRoots();
             StringBuffer sb = new StringBuffer();
            while (e.hasMoreElements()) {
                String root = (String) e.nextElement();
                sb.append(root);
            }
            fconn = (FileConnection) Connector.open("file:///"+sb.toString()+"myfile.txt", Connector.READ_WRITE);
            if (!fconn.exists())
                fconn.create();

            os = fconn.openDataOutputStream();
            os.write(text.getBytes());
            fconn.setHidden(false);
//          fconn.setReadable(true);
        }

        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                if (null != os)
                    os.close();
                if (null != fconn)
                    fconn.close();
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }
        }
    }


}
