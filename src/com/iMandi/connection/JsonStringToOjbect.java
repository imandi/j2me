package com.iMandi.connection;

import com.iMandi.bean.CartItems;
import com.iMandi.bean.Category;
import com.iMandi.bean.CitrusErrors;
import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.iMandi.bean.Product;
import com.iMandi.bean.ProductOption;
import com.iMandi.bean.Profile;

public class JsonStringToOjbect {

    public static Vector getCategoryList(String jsonText) {
        Vector categoryList = new Vector();
//		Product product = null ;

        try {
             JSONObject json1 = new JSONObject(jsonText);


            JSONObject json = new JSONObject(json1.getString("data"));
            System.out.println("getCategoryList -->" + json.getString("categories"));
//                         json = new JSONObject(json.getString("categories"));
//                         System.out.println("list -->"+json.getString("products"));
            JSONArray jsonArr = new JSONArray(json.getString("categories"));
            for (int i = 0; i < jsonArr.length(); i++) {
                //get a single client JSON Object
                json = jsonArr.getJSONObject(i);

                //pass the JSON String to the method in order
                //to get the Client Object
                Category client = getCategory(json.toString());

                categoryList.addElement(client);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        System.out.println("categoryList -->" + categoryList.toString());
        return categoryList;
    }

    public static Vector getProductList(String jsonArray) {
        Vector productList = new Vector();
        Product product = null;

        try {
            JSONObject json = new JSONObject(jsonArray);
            System.out.println("list -->" + json.getString("data"));
            json = new JSONObject(json.getString("data"));
            System.out.println("list -->" + json.getString("products"));
            JSONArray jsonArr = new JSONArray(json.getString("products"));
            for (int i = 0; i < jsonArr.length(); i++) {
                //get a single client JSON Object
                json = jsonArr.getJSONObject(i);

                //pass the JSON String to the method in order
                //to get the Client Object
                Product client = getProduct(json.toString());

                productList.addElement(client);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        System.out.println("productList -->" + productList.toString());
        return productList;
    }

    public static Vector getCartProductList(String jsonText) {
        Vector productList = new Vector();
        Product product = null;

        try {
//                     "message":"Cart Data","status":1,"cart_data":{"product_id":"372","product_name":"Fish\u2019s Amino Nutrients - Organic Fertilizer - 100 ml","product_sku":"MANDI-374","product_qty":8,"product_price":"140.0000"}}

 JSONObject json1 = new JSONObject(jsonText);
            JSONObject json = new JSONObject(json1.getString("data"));
            System.out.println("getCartProductList status -->" + json1.getString("status"));
//                         json = new JSONObject(json.getString("list"));
            CartItems.getInstance().total_amount = json.getString("total_amount");
            CartItems.getInstance().pick_location = json.getString("pick_location");

            System.out.println("total_amount -->" + json.getString("total_amount"));
            JSONArray jsonArr = new JSONArray(json.getString("cart_data"));
            for (int i = 0; i < jsonArr.length(); i++) {
                //get a single client JSON Object
                json = jsonArr.getJSONObject(i);

                //pass the JSON String to the method in order
                //to get the Client Object
                Product client = getProduct(json.toString());

                productList.addElement(client);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        System.out.println("productList -->" + productList.toString());
        return productList;
    }
    /** Name of the client*/
    private static String product_id = "product_id";
    private static String product_sku = "product_sku";
    private static String product_name = "product_name";
    private static String product_price = "product_price";
    private static String product_desc = "product_desc";
    private static String product_short_desc = "product_short_desc";
    private static String product_imagepath = "product_imagepath";
    private static String product_stock_status = "product_detail_url";
    private static String product_detail_url = "product_detail_url";
    private static String product_created_at = "product_detail_url";
    private static String product_category = "product_detail_url";
    private static String total_page = "product_detail_url";
    private static String prod_image_url = "prod_image_url";
    private static String product_qty = "product_qty";

    //{"product_category":"Fertilizer","product_stock_status":"In Stock",
    //"product_name":"Ammonium Phosphare Sulphate","product_sku":"Ammonium","product_id":"9","product_imagepath":
    //"http://velenty.com/imandi/media/catalog/product/5/6/565250.jpg","product_detail_url":"http://127.0.0.1/magentoapi
    ///api_v1/product_details.php?id=9","product_desc":"Ammonium Phosphare Sulphate\r\n","product_short_desc":"Ammonium
    //Phosphare Sulphate\r\n","product_price":"Rs.825","total_page":1,"product_created_at":"08-14-2017"}
    public static Product getProduct(String jsonText) {
        Product product = new Product();
        System.out.println("getProduct -->" + jsonText);
        try {
            JSONObject json = new JSONObject(jsonText);
//                 JSONObject json = new JSONObject(jsonText);
            if (json.has(product_id) ) {
                product.setProductId(json.getString(product_id));
            }
            if (json.has(product_sku)) {
                product.setProductSku(json.getString(product_sku));
            }
            if (json.has(product_name)) {
                product.setProductName(json.getString(product_name));
            }
            if (json.has(product_price)) {
                product.setProductPrice(json.getString(product_price));
            }
            if (json.has(product_desc)) {
                product.setProductDesc(json.getString(product_desc));
            }
            if (json.has(product_short_desc)) {
                product.setProductShortDesc(json.getString(product_short_desc));
            }
            if (json.has(product_imagepath)) {
                product.setProductImagepath(json.getString(product_imagepath));
            }
            if (json.has(product_stock_status)) {
                product.setProductStockStatus(json.getString(product_stock_status));
            }
            if (json.has(product_detail_url)) {
                product.setProductDetailUrl(json.getString(product_detail_url));
            }
            if (json.has(product_created_at)) {
                product.setProductCreatedAt(json.getString(product_created_at));
            }
            if (json.has(product_category)) {
                product.setProductCategory(json.getString(product_category));
            }
            if (json.has(product_qty)) {
                product.setProductCartQuentity(Integer.parseInt(json.getString(product_qty)));
            }
            if (json.has(total_page)) {
//                        product.setProductt(json.getString(total_page));
            }
            if(json.has("product_image_url"))
            try {

                JSONArray jsonArr = new JSONArray(json.getString("product_image_url"));
                for (int i = 0; i < jsonArr.length(); i++) {
                    //get a single client JSON Object
//                            json = jsonArr.getJSONObject(i);
                    product.setProductImage(jsonArr.getString(i));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (json.has(total_page)) {
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        System.out.println("product -->" + product.toString());
        return product;
    }


    public static Product getProductDetail(String jsonText) {

        Product product = new Product();
        System.out.println("getProduct -->" + jsonText);
        try {
            JSONObject json1 = new JSONObject(jsonText);

            JSONObject json = new JSONObject(json1.getString("data"));
//                 JSONObject json = new JSONObject(jsonText);
            if (json.has(product_id) || json.has("prod_id")) {
                product.setProductId(json.getString("prod_id"));
            }
            if (json.has(product_sku) || json.has("prod_sku")) {
                product.setProductSku(json.getString("prod_sku"));
            }
            if (json.has(product_name) || json.has("prod_name")) {
                product.setProductName(json.getString("prod_name"));
            }
            if (json.has(product_price) || json.has("prod_price")) {
                product.setProductPrice(json.getString("prod_price"));
            }
            if (json.has(product_desc) || json.has("prod_description")) {
                product.setProductDesc(json.getString("prod_description"));
            }
            if (json.has(product_short_desc) || json.has("prod_short_description")) {
                product.setProductShortDesc(json.getString("prod_short_description"));
            }
            if (json.has(product_imagepath) || json.has("")) {
//                product.setProductImagepath(json.getString(product_imagepath));
            }
            if (json.has(product_stock_status)) {
                product.setProductStockStatus(json.getString(product_stock_status));
            }
            if (json.has(product_detail_url) || json.has("")) {
//                product.setProductDetailUrl(json.getString(product_detail_url));
            }
            if (json.has(product_created_at) || json.has("")) {
//                product.setProductCreatedAt(json.getString(product_created_at));
            }
            if (json.has(product_category) || json.has("")) {
//                product.setProductCategory(json.getString(product_category));
            }
            if (json.has(product_qty) || json.has("product_qty")) {
                product.setProductCartQuentity(Integer.parseInt(json.getString("product_qty")));
            }
            if (json.has(total_page)) {
//                        product.setProductt(json.getString(total_page));
            }
            if(json.has("product_image_url") || json.has("prod_image_url"))
            try {
                JSONArray jsonArr = null;

                if(json.has("product_image_url"))
                    jsonArr = new JSONArray(json.getString("product_image_url"));
                else if(json.has("prod_image_url"))
                    jsonArr = new JSONArray(json.getString("prod_image_url"));

                for (int i = 0; i < jsonArr.length(); i++) {
                    //get a single client JSON Object
//                            json = jsonArr.getJSONObject(i);
                    JSONObject     jsonT = jsonArr.getJSONObject(i);
                    product.setProductImage(jsonT.getString("imgurl"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(json.has("product_option") || json.has("prod_option"))
            try {

                JSONArray jsonArr = null;

                if(json.has("prod_option")){
                    JSONObject json12 = new JSONObject(json.getString("prod_option"));
                    jsonArr = new JSONArray(json12.getString("pack_size"));
                }
                else if(json.has("product_option")){
                    JSONObject json12 = new JSONObject(json.getString("product_option"));
                    jsonArr = new JSONArray(json12.getString("product_option"));
                }

                for (int i = 0; i < jsonArr.length(); i++) {
                    //get a single client JSON Object
                       JSONObject     jsonT = jsonArr.getJSONObject(i);
                            ProductOption productOption = new ProductOption();
                    productOption.price = jsonT.getString("price");
                    productOption.size = jsonT.getString("size");
                    productOption.prod_stock_status = jsonT.getString("prod_stock_status");
                    product.prodOption.addElement(productOption);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (json.has(total_page)) {
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        System.out.println("product -->" + product.toString());
        return product;
    }

    private static String categoryid = "category_id";
    private static String categoryname = "category_name";

    public static Category getCategory(String jsonText) {
        Category category = new Category();
        System.out.println("getCategory -->" + jsonText);
        try {
            JSONObject json = new JSONObject(jsonText);
            if (json.has(categoryid)) {
                category.setCategoryid(json.getString(categoryid));
            }
            if (json.has(categoryname)) {
                category.setCategoryname(json.getString(categoryname));
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        System.out.println("product -->" + category.toString());
        return category;
    }

    //{&quot;userId&quot;:1,&quot;userName&quot;:&quot;pkr555&quot;,&quot;mobileNumber&quot;:&quot;91-
    //9717098492&quot;,&quot;type&quot;:&quot;user&quot;,&quot;status&quot;:&quot;success&quot;,&quot;message&quot;:&quot;User
    //        {"userId":167,"userName":"91983tx74e4d621","mobileNumber":"91-9818662101",
    //"countryCode":"91","userState":"active","type":"user","imei":"865800226639641020",
    //"activationDate":"Nov 3, 2017 7:07:55 PM","clientVersion":"iMandi_J2me_1_0_0",
    //"createdDate":"Oct 30, 2017 4:49:09 PM","status":"success","message":"User profile data"}
    public static Profile getProfile(String jsonText) {
        Profile profile = Profile.getInstance();
        System.out.println("getProfile -->" + jsonText);
        try {
            JSONObject json = new JSONObject(jsonText);
            if (json.has("userId")) {
                profile.setUserId(json.getString("userId"));
            }
            if (json.has("name")) {
                profile.setName(json.getString("name"));
            }
            if (json.has("aadharCardNumber")) {
                profile.setAadharCardNumber(json.getString("aadharCardNumber"));
            }
                  if (json.has("gender")) {
                profile.setGender(json.getString("gender"));
            }

                    if (json.has("dob")) {
                profile.setDob(json.getString("dob"));
            }
if (json.has("pinCode")) {
                profile.setPinCode(json.getString("pinCode"));
            }
              if (json.has("address")) {
                profile.setAddress(json.getString("address"));
            }
            if (json.has("mobileNumber")) {
                profile.setMobileNumber(json.getString("mobileNumber"));
            }
            if (json.has("username")) {
                profile.setUsername(json.getString("username"));
            }
            if (json.has("password")) {
                profile.setPassword(json.getString("password"));
            }
            if (json.has("countryCode")) {
                profile.setCountryCode(json.getString("countryCode"));
            }

            if (json.has("type")) {
//                profile.setUserId(json.getString("type"));
            }
            profile.getStatus(null);
            if (json.has("status")) {
                profile.getStatus(json.getString("status"));
            }
            profile.setMessage(null);
            if (json.has("message")) {
                profile.setMessage(json.getString("message"));
            }
            profile.messageSub =  null;
            profile.code =  null;
            if (json.has("citrusErrors")) {
                JSONArray jsonArr = new JSONArray(json.getString("citrusErrors"));
                for (int i = 0; i < jsonArr.length(); i++) {
                    json = jsonArr.getJSONObject(i);
                    JSONObject jsonL = new JSONObject(json.toString());
                    if (jsonL.has("message")) {
                        profile.messageSub = json.getString("message");
                    }
                    if (jsonL.has("code")) {
                        profile.code = json.getString("code");
                    }
                }
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        System.out.println("getProfile -->" + profile.toString());
        return profile;
    }

    public static CitrusErrors getMessage(String jsonText) {
        CitrusErrors citrusErrors = new CitrusErrors();
        System.out.println("getMessage jsonText -->" + jsonText);
        try {
            JSONObject json = new JSONObject(jsonText);
            if (json.has("status")) {
                citrusErrors.status = json.getString("status");
            }
            if (json.has("message")) {
                citrusErrors.message = json.getString("message");
            }
            if(json.has("citrusErrors"))
            {
            JSONArray jsonArr = new JSONArray(json.getString("citrusErrors"));
            for (int i = 0; i < jsonArr.length(); i++) {
                json = jsonArr.getJSONObject(i);
                JSONObject jsonL = new JSONObject(json.toString());
                if (jsonL.has("message")) {
                    citrusErrors.messageSub = json.getString("message");
                }
                if (jsonL.has("code")) {
                    citrusErrors.code = json.getString("code");
                }
            }}

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        System.out.println("getMessage -->" + citrusErrors.toString());
        return citrusErrors;
    }
}
