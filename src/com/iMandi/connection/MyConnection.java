package com.iMandi.connection;

import com.iMandi.Constant;
import com.iMandi.bean.ClientParam;
import com.iMandi.bean.Profile;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.ContentConnection;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Image;
import javax.obex.ClientSession;
import org.json.me.util.ImageList;

public class MyConnection {

    static MyConnection myConnection;
    public static String STATUS_IDEL = "STATUS_IDEL";
    public static String STATUS_CONNECTING = "STATUS_CONNECTING";
    public static String STATUS_CONNECTED = "STATUS_CONNECTED";
    public static String STATUS_DONE = "STATUS_DONE";
    public static String STATUS_ERROR = "STATUS_ERROR";
    public static String STATUS_CANCEL_REQUEST = "STATUS_CANCEL_REQUEST";
    public static String STATUS_REQUEST_CANCELED = "STATUS_REQUEST_CANCELED";
    public static String STATUS_CURRENT = STATUS_IDEL;

    public static MyConnection getInstance() {
        if (myConnection == null) {
            myConnection = new MyConnection();
        }
        return myConnection;
    }

    public void fetchData(String url, MyConnectionInf connectionInf,byte requestID) {
        System.out.println("fetchData:" + url);
        if (STATUS_CURRENT != STATUS_IDEL) {
            connectionInf.myConnectionBusy(STATUS_CURRENT,requestID);
            return;
        }
        STATUS_CURRENT = STATUS_CONNECTING;
        connectionInf.sendStatus(STATUS_CURRENT,requestID);
        HttpConnection connection = null;
        InputStream inputstream = null;

        try {
            // connection = (HttpConnection) Connector.open(url);
            // URL urls = new URL(url);
            // connection = (HttpConnection) urls.openConnection();

            // HTTP Request
            connection = (HttpConnection) Connector.open(url, Connector.READ);
            getConnectionInformation(connection);
            connection.setRequestMethod(HttpConnection.GET);
            makeHeader(connection);
//            printHeader(connection);
            // HTTP Response
            System.out.println("Status Line Code: " + connection.getResponseCode());
            System.out.println("Status Line Message: " + connection.getResponseMessage());
            STATUS_CURRENT = STATUS_CONNECTED;
            if (connection.getResponseCode() == HttpConnection.HTTP_OK) {
                System.out.println(connection.getHeaderField(0) + " " + connection.getHeaderFieldKey(0));
                System.out.println("Header Field Date: " + connection.getHeaderField("date"));
                String str;
                inputstream = connection.openInputStream();
                int length = (int) connection.getLength();
                if (length != -1) {
                    byte incomingData[] = new byte[length];
                    inputstream.read(incomingData);
                    str = new String(incomingData);
                } else {
                    ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
                    int ch;
                    while ((ch = inputstream.read()) != -1) {
                        bytestream.write(ch);
                        if (STATUS_CURRENT == STATUS_CANCEL_REQUEST) {
                            System.out.println("Canceling request......");
                            break;
                        }
                    }
                    str = new String(bytestream.toByteArray());
                    bytestream.close();
                }
                if (STATUS_CURRENT != STATUS_CANCEL_REQUEST) {
                    System.out.println(str);
                    connectionInf.sendData(str,requestID);
                } else {
                    STATUS_CURRENT = STATUS_REQUEST_CANCELED;
                }
            }
//            printHeader(connection);
        }
        catch (Exception e) {
            e.printStackTrace();
            STATUS_CURRENT = STATUS_ERROR;
        }
        finally {// Clean up
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (inputstream != null) {
                try {
                    inputstream.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
//            if (bStrm != null) {
//                bStrm.close();
//            }
        }
        connectionInf.sendStatus(STATUS_CURRENT,requestID);
        STATUS_CURRENT = STATUS_IDEL;
        connectionInf.sendStatus(STATUS_CURRENT,requestID);
    }
    public void makeConnection(final String url,final  MyConnectionInf connectionInf,final String postJson,final String method,final byte requestId){
        new Thread(new Runnable() {

                        public void run() {
                            try
                            {
                                Thread.sleep(100);
                                if(method.equalsIgnoreCase("POST"))
                                    fetchDataPost(url, connectionInf, postJson,requestId);
                                else
                                    fetchData(url, connectionInf,requestId) ;
                            }catch(Exception e){

                            }
                        }
                    }).start();
    }
    //http://13.126.89.206/tiger/rest/user/profile/get?userName=91983tx74e4d621
//    ONNECTION HEADER:audata: 167:AAAACu5hh3y0wqlZGvBfs+HgLwQ=
//CONNECTION HEADER:ucid: 86580022663964
//CONNECTION HEADER:cversion iMandi_Android_1_0_0
//CONNECTION HEADER:Content-Type: application/json
    private void makeHeader(HttpConnection connection) throws Exception{
        connection.setRequestProperty("ucid", ClientParam.IMEI);
        connection.setRequestProperty("cversion", ClientParam.VERSION);
        connection.setRequestProperty("audata", "'"+Profile.getInstance().getUserId()+":"+Profile.getInstance().getPasswordEncrepted()+"'");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("pincode", Profile.getInstance().getPinCode());
//        connection.setRequestProperty("pincode", "110030");//Profile.getInstance().getPinCode());


        Constant.printLog("CONNECTION HEADER:audata: ", "'"+Profile.getInstance().getUserId()+":"+Profile.getInstance().getPasswordEncrepted()+"'");
        Constant.printLog("CONNECTION HEADER:ucid: ", ClientParam.IMEI);
        Constant.printLog("CONNECTION HEADER:cversion ", ClientParam.VERSION);
        Constant.printLog("CONNECTION HEADER:Content-Type: ", "application/json");
    }
    private void printHeader(HttpConnection connection) throws Exception{
//        Constant.printLog("CONNECTION HEADER:audata: ", connection.getHeaderField("audata"));
//        Constant.printLog("CONNECTION HEADER:ucid: ", connection.getHeaderField("ucid"));
//        Constant.printLog("CONNECTION HEADER:cversion ", connection.getHeaderField("cversion"));
//        Constant.printLog("CONNECTION HEADER:Content-Type: ", connection.getHeaderField("Content-Type"));
    }


    public void fetchDataPost(String url, MyConnectionInf connectionInf,String postJson,byte requestID) throws IOException {
        StringBuffer sb = new StringBuffer();
        System.out.println("fetchData:" + url+" json data :"+postJson);
        if (STATUS_CURRENT != STATUS_IDEL) {
            connectionInf.myConnectionBusy(STATUS_CURRENT,requestID);
            return;
        }
        STATUS_CURRENT = STATUS_CONNECTING;
        connectionInf.sendStatus(STATUS_CURRENT,requestID);
        HttpConnection connection = null;
        InputStream inputstream = null;
InputStream is = null;
        OutputStream os = null;

        try {
            // connection = (HttpConnection) Connector.open(url);
            // URL urls = new URL(url);
            // connection = (HttpConnection) urls.openConnection();

            // HTTP Request
            connection = (HttpConnection) Connector.open(url);
            getConnectionInformation(connection);
            connection.setRequestMethod(HttpConnection.POST);
            makeHeader(connection);
            
            // HTTP Response
//            System.out.println("Status Line Code: " + connection.getResponseCode());
//            System.out.println("Status Line Message: " + connection.getResponseMessage());
            STATUS_CURRENT = STATUS_CONNECTED;

            if(postJson!=null){
            os = connection.openOutputStream();

            String params;
//            params = "name=" + name;

            os.write(postJson.getBytes());
            }

            /**
             * Caution: os.flush() is controversial. It may create unexpected
             * behavior on certain mobile devices. Try it out for your mobile
             * device
             **/
            // os.flush();
            // Read Response from the Server
//            StringBuffer sb = new StringBuffer();
            is = connection.openDataInputStream();
            int chr;
            while ((chr = is.read()) != -1) {
                sb.append((char) chr);
            }
            if (STATUS_CURRENT != STATUS_CANCEL_REQUEST) {
                    System.out.println(sb.toString());
                    connectionInf.sendData(sb.toString(),requestID);
                } else {
                    STATUS_CURRENT = STATUS_REQUEST_CANCELED;
                }
            Constant.printLog("Post Response::", sb.toString());

//            if (connection.getResponseCode() == HttpConnection.HTTP_OK) {
//                System.out.println(connection.getHeaderField(0) + " " + connection.getHeaderFieldKey(0));
//                System.out.println("Header Field Date: " + connection.getHeaderField("date"));
//                String str;
//                inputstream = connection.openInputStream();
//                int length = (int) connection.getLength();
//                if (length != -1) {
//                    byte incomingData[] = new byte[length];
//                    inputstream.read(incomingData);
//                    str = new String(incomingData);
//                } else {
//                    ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
//                    int ch;
//                    while ((ch = inputstream.read()) != -1) {
//                        bytestream.write(ch);
//                        if (STATUS_CURRENT == STATUS_CANCEL_REQUEST) {
//                            System.out.println("Canceling request......");
//                            break;
//                        }
//                    }
//                    str = new String(bytestream.toByteArray());
//                    bytestream.close();
//                }
//                if (STATUS_CURRENT != STATUS_CANCEL_REQUEST) {
//                    System.out.println(str);
//                    connectionInf.sendData(str);
//                } else {
//                    STATUS_CURRENT = STATUS_REQUEST_CANCELED;
//                }
//            }
        }
        catch (Exception e) {
            e.printStackTrace();
            STATUS_CURRENT = STATUS_ERROR;
//            Constant.showError("get post:"+e.getMessage());
        }
        finally {// Clean up
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (inputstream != null) {
                try {
                    inputstream.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
            if (connection != null) {
                connection.close();
            }
//            if (bStrm != null) {
//                bStrm.close();
//            }
        }
        connectionInf.sendStatus(STATUS_CURRENT,requestID);
        STATUS_CURRENT = STATUS_IDEL;
        connectionInf.sendStatus(STATUS_CURRENT,requestID);
    }

    public String postData(String url,String postJson) throws Exception {
        System.out.println("postData:" + url+" json data :"+postJson);
StringBuffer sb = new StringBuffer();
        HttpConnection httpConn = null;
//        String url = "http://localhost:8080/examples/servlet/GetBirthday";
        InputStream is = null;
        OutputStream os = null;

        try {
            // Open an HTTP Connection object
            httpConn = (HttpConnection) Connector.open(url);
            // Setup HTTP Request to POST
            httpConn.setRequestMethod(HttpConnection.POST);
            makeHeader(httpConn);
//            httpConn.setRequestProperty("User-Agent", "Profile/MIDP-1.0 Confirguration/CLDC-1.0");
//            httpConn.setRequestProperty("Accept_Language", "en-US");
//            // Content-Type is must to pass parameters in POST Request
//            httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            // This function retrieves the information of this connection
            getConnectionInformation(httpConn);

            os = httpConn.openOutputStream();

//            String params;
//            params = "name=" + name;

            os.write(postJson.getBytes());
//            os.flush();

            /**
             * Caution: os.flush() is controversial. It may create unexpected
             * behavior on certain mobile devices. Try it out for your mobile
             * device
             **/
            // os.flush();
            // Read Response from the Server
//            StringBuffer sb = new StringBuffer();
            is = httpConn.openDataInputStream();
            int chr;
            while ((chr = is.read()) != -1) {
                sb.append((char) chr);
            }
            Constant.printLog("Post Response::", sb.toString());

            // Web Server just returns the birthday in mm/dd/yy format.
//            System.out.println(name + "'s Birthday is " + sb.toString());

        } finally {
            if (is != null) {
                is.close();
            }
            if (os != null) {
                os.close();
            }
            if (httpConn != null) {
                httpConn.close();
            }
        }

        return sb.toString() ;

    }
    /*--------------------------------------------------
     * Open connection and download png into a byte array.
     *-------------------------------------------------*/

    private Image getImage(String url) throws IOException {
        ContentConnection connection = (ContentConnection) Connector.open(url);
        DataInputStream iStrm = connection.openDataInputStream();
        ByteArrayOutputStream bStrm = null;
        Image im = null;
        try {
// ContentConnection includes a length method
            byte imageData[];
            int length = (int) connection.getLength();
            if (length != -1) {
                imageData = new byte[length];
// Read the png into an array
                iStrm.readFully(imageData);
            } else // Length not available...
            {
                bStrm = new ByteArrayOutputStream();
                int ch;
                while ((ch = iStrm.read()) != -1) {
                    bStrm.write(ch);
                }
                imageData = bStrm.toByteArray();
                bStrm.close();
            }
// Create the image from the byte array
            im = Image.createImage(imageData, 0, imageData.length);
            imageData = null;
        } finally {// Clean up
            if (iStrm != null) {
                iStrm.close();
            }
            if (connection != null) {
                connection.close();
            }
            if (bStrm != null) {
                bStrm.close();
            }
        }
        return (im == null ? null : im);
    }
    public boolean downloadAndSaveImage(String url,int index, MyConnectionInf connectionInf,byte requestId) throws IOException {
        Constant.printLog("downloadAndSaveImage::", url);
        ContentConnection connection = (ContentConnection) Connector.open(url);
        DataInputStream iStrm = connection.openDataInputStream();
        ByteArrayOutputStream bStrm = null;
        Image im = null;
        try {
            
// ContentConnection includes a length method
            byte imageData[];
            int length = (int) connection.getLength();
            System.out.print("length:"+length);
            if(length <30000)///839137Image
            {
            if (length != -1) {
                imageData = new byte[length];
// Read the png into an array
                iStrm.readFully(imageData);
            } else // Length not available...
            {
                bStrm = new ByteArrayOutputStream();
                int ch;
                while ((ch = iStrm.read()) != -1) {
                    bStrm.write(ch);
                }
                imageData = bStrm.toByteArray();
                bStrm.close();
            }
// Create the image from the byte array
            try{
//                Thread.sleep(5000);
            Constant.printLog("Image byte length:",""+ imageData.length);
                im = Image.createImage(imageData, 0, imageData.length);

//            RMS.getInstance().saveImage(imageData, index+RMS.getInstance().MAX_SETTING_INDEX);
            ImageList.images.addElement(im);//resizeImage(im,70,));
            connectionInf.imageLoaded(STATUS_CURRENT,requestId);
            }catch(Exception e){
                e.printStackTrace();
                connectionInf.imageLoaded(STATUS_CURRENT,requestId);
            }
            }else{
                connectionInf.imageLoaded(STATUS_CURRENT,requestId);
            }

            imageData = null;
        } finally {// Clean up
            if (iStrm != null) {
                iStrm.close();
            }
            if (connection != null) {
                connection.close();
            }
            if (bStrm != null) {
                bStrm.close();
            }
        }
        return true;//(im == null ? null : im);
    }

    protected static Image resizeImage(Image image, int resizedWidth, int resizedHeight) {

    int width = image.getWidth();
    int height = image.getHeight();

    int[] in = new int[width];
    int[] out = new int[resizedWidth * resizedHeight];

    int dy, dx;
    for (int y = 0; y < resizedHeight; y++) {

        dy = y * height / resizedHeight;
        image.getRGB(in, 0, width, 0, dy, width, 1);

        for (int x = 0; x < resizedWidth; x++) {
            dx = x * width / resizedWidth;
            out[(resizedWidth * y) + x] = in[dx];
        }

    }

    return Image.createRGBImage(out, resizedWidth, resizedHeight, true);

}

    void getConnectionInformation(HttpConnection hc) {
//        System.out.println("Request Method for this connection is " + hc.getRequestMethod());
//        System.out.println("URL in this connection is " + hc.getURL());
//        System.out.println("Protocol for this connection is " + hc.getProtocol());
//        System.out.println("This object is connected to " + hc.getHost() + " host");
//        System.out.println("HTTP Port in use is " + hc.getPort());
//        System.out.println("Query parameter in this request are  " + hc.getQuery());
    }
}
