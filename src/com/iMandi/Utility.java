package com.iMandi;

import com.iMandi.bean.Category;
import com.iMandi.connection.DataEncryption;
import com.iMandi.connection.MyBase64;
import com.iMandi.connection.RMS;
import java.io.UnsupportedEncodingException;
import java.util.Vector;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author nagendrakumar
 */
public class Utility {

    public static Vector screeStack = new Vector();
    public static void openLoginScreen(Object object){
        ((ImandiCanvas)object).recycleImage();
//        ((ImandiCanvas)object).prevScreen = object;
        if(RMS.getInstance().getRecord(RMS.STATE).equalsIgnoreCase("OTP"))
            iMandiMIDlet.display.setCurrent(new MobileVerificationScreen("Login",(byte)2));
        else
            iMandiMIDlet.display.setCurrent(new MobileVerificationScreen("Login",(byte)1));
    }
    public static void openLoginAsGuest(Object object){
        if(object != null)
        ((ImandiCanvas)object).recycleImage();
//        ((ImandiCanvas)object).prevScreen = object;
        GridCanvas gridCanvas = new GridCanvas();
        gridCanvas.CENTER_MENU = "Choose" ;
//        gridCanvas.LEFT_MENU = "Back" ;
//        gridCanvas.prevScreen = object ;
        iMandiMIDlet.display.setCurrent(gridCanvas);
        if(object != null)
        screeStack.addElement(object);
    }
   

    public static void openMarketPlaceScreen(Object object){
        ((ImandiCanvas)object).recycleImage();
//        ((ImandiCanvas)object).prevScreen = object;
        GridCanvas gridCanvas = new GridCanvas();
        gridCanvas.type = 2;
        gridCanvas.column = 3;
        gridCanvas.padding = 35;
        gridCanvas.CENTER_MENU = "Choose" ;
        gridCanvas.LEFT_MENU = "Back" ;
        gridCanvas.init();
//        gridCanvas.prevScreen = object ;
        CategoryScreen categoryScreen = new CategoryScreen();
        iMandiMIDlet.display.setCurrent(categoryScreen);
        screeStack.addElement(object);
    }
    public static boolean  displayPrevScreen(){
        try{
        Constant.printLog("", "Displaying previous screen : "+screeStack.elementAt(screeStack.size()-1).getClass().getName());
        iMandiMIDlet.display.setCurrent((Displayable)screeStack.elementAt(screeStack.size()-1));
        screeStack.removeElementAt(screeStack.size()-1);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return true;

    }
    public static void openProfileScreen(Object object){
        ((ImandiCanvas)object).recycleImage();
//        ((ImandiCanvas)object).pr/evScreen = object;

         ProfileCanvas profileScreen =new ProfileCanvas("Profile");
//         friendsCanvas.LEFT_MENU = "Back" ;
//         friendsCanvas.CENTER_MENU = "Choose" ;
//         friendsCanvas.prevScreen = object ;
//         profileScreen.display();
         iMandiMIDlet.display.setCurrent(profileScreen);
         screeStack.addElement(object);
    }
    public static String encriptPass(String password) {
	boolean newProtocol = true ;
		System.out.println("--------------password : "+password);
//		userId =1574403;
//		password = "a" ;

		StringBuffer builder = new StringBuffer();
		if (newProtocol) {

		}

		if (newProtocol) {
			DataEncryption encrptor = null;
			try {
				encrptor = new DataEncryption(Constant.ENCRYPTION_KEY.getBytes("utf-8"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			byte[] isoPassword = null;
			try {
				isoPassword = password.getBytes("utf-8");
				byte[] encryptedPassword = encrptor.iMandiEncrypt(isoPassword);
				String base64Password = MyBase64.encode(encryptedPassword);
				builder.append(base64Password);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			builder.append(password);
		}
		if (newProtocol) {

		}

		System.out.println("-----------------------encrepted pass-------------------"+builder.toString());
		return builder.toString().trim();
	}

    public static void openProductList(Object object,Category category){
        if(object != null)
        ((ImandiCanvas)object).recycleImage();
        
            Constant.currentCategory = new Vector();
            Constant.currentCategory.addElement(category);
            ProductCanvas productCanvas = new ProductCanvas();
            iMandiMIDlet.display.setCurrent(productCanvas);
            if(object!=null)
            screeStack.addElement(object);
    }
    public static void openFourmChat(Object object,Category category){
        if(object != null)
        ((ImandiCanvas)object).recycleImage();
        
            FourmChat fourmChat = new FourmChat();
            iMandiMIDlet.display.setCurrent(fourmChat);
            if(object!=null)
            screeStack.addElement(object);
    }
    public static void openFourmCategory(Object object,Category category){
        if(object != null)
        ((ImandiCanvas)object).recycleImage();

            FourmCatgory fourmChat = new FourmCatgory();
            iMandiMIDlet.display.setCurrent(fourmChat);
            if(object!=null)
            screeStack.addElement(object);
    }


}

