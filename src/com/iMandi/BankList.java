package com.iMandi;

/**
 *
 * @author nagendrakumar
 */

import com.iMandi.bean.Product;
import com.iMandi.bean.Bank;
import com.iMandi.bean.CartItems;
import com.iMandi.bean.Category;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.MyConnection;
import com.iMandi.connection.MyConnectionInf;
import java.util.Vector;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import org.json.me.util.ImageList;

public class BankList implements MyConnectionInf
{

    ProductCanvas productCanvas;
    protected int linePadding = 2;
    protected int topSpace = 0;
    protected int margin = 2;
    protected int padding = 2;
//    public static Font font = Font.getDefaultFont();
    boolean showDetail = false;
    protected int borderWidth = 1;
    // will contain item splitted lines
    String[][] itemLines = null;
    // will hold items image parts
    Image[] images = null;
    // will hold selected item index
    public int selectedItem = 0;
    // these will hold item graphical properties
    int[] itemsTop = null;
    int[] itemsHeight = null;
    // these will hold List vertical scrolling
    int scrollTop = 0;
    final int SCROLL_STEP = 40;
    private BankScreen canvas;
    boolean rowMenu = false;
    boolean bottomMenu = false;
    int rowMenuIndex = 0;
    int bottomMenuIndex = 0;
    int maxCommandInRow = 3;
    int maxCommandInBottom = 3;
    int cellWidth = 75 ;
    int cellHeight = 20 ;
    int startTextYInCell = 0 ;
    int maxChar = 0 ;

    Vector banks;
//    public static Font font;
    public BankList( BankScreen canvas){
         this.canvas = canvas;
//        font = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
    }
    public BankList(String title, Vector banks, BankScreen canvas) {
        this.banks = banks;
        this.canvas = canvas;
//        font = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);


//		canvas.setTitle(title);

//		this.images = imageElements;

        itemLines = new String[banks.size()][];

        itemsTop = new int[banks.size()];
        itemsHeight = new int[banks.size()];

        for (int i = 0; i < itemLines.length; i++) {
            // get image part of this item, if available
            Image imagePart = ImageList.product;//getImage(i);

            // get avaiable width for text
            int w = getItemWidth() - (imagePart != null ? imagePart.getWidth() + padding : 0);

            // and split item text into text rows, to fit available width
            itemLines[i] = getTextRows((String) ((Bank) banks.elementAt(i)).getBankname(), Constant.font_small, w);
        }
        new Thread(new Runnable() {

            public void run() {
                for (int i = 0; i < 10; i++) {
                    try {
                        Thread.sleep(300);
                        keyPressed(-2);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });//.start();
        cellWidth = canvas.getWidth()-45;
        cellWidth = cellWidth-15;
        cellWidth = cellWidth-(margin*5);
        cellWidth = cellWidth /2;
        Constant.printLog("cellWidth:", ""+cellWidth);
        Constant.printLog("canvas width:", ""+canvas.getWidth());
    }

    public int getItemWidth() {
        return canvas.getWidth() - 2 * borderWidth - 2 * padding - 2 * margin;
    }

    public void keyPressed(int key) {
        if (!Constant.isKnownKey(canvas, key)) {
         if (key == Constant.KEY_SOFT_LEFT){
//            fireLeftSoftCommand();
//           CartCanvas canvas = new CartCanvas();
//                canvas.prevDisplay = canvas ;
//                iMandiMIDlet.display.setCurrent(canvas);

             ((BankScreen)canvas).type=1;
         }
         else{
//            fireRightSoftCommand();
         }
      }
        System.out.println("key:"+key);
        int keyCode = canvas.getGameAction(key);

        // is there 1 item at least?
        if (itemLines.length > 0) {
            // going up
            if (keyCode == Canvas.UP) {
                // current item is clipped on top, so can scroll up
                if (itemsTop[selectedItem] < scrollTop) {
                    scrollTop -= SCROLL_STEP;

                    canvas.repaint();
                } // is there a previous item?
                else if (selectedItem > 0) {
                    selectedItem--;

                    canvas.repaint();
                }
                rowMenuIndex = 0;
                rowMenu = false;
                bottomMenu = false;
            } //going down
            else if (keyCode == Canvas.DOWN) {
                // current item is clipped on bottom, so can scroll down
                if (itemsTop[selectedItem] + itemsHeight[selectedItem] >= scrollTop + getHeight()) {
                    scrollTop += SCROLL_STEP;

                    canvas.repaint();
                } // is there a following item?
                else if (selectedItem < itemLines.length - 1) {
                    selectedItem++;

                    canvas.repaint();
                }
                rowMenu = false;
                bottomMenu = false;
                rowMenuIndex = 0;
            } else if (keyCode == Canvas.RIGHT) {
                if(!bottomMenu){
                    rowMenu = true;
                    rowMenuIndex++;
                    if (rowMenuIndex > maxCommandInRow) {
                        rowMenuIndex = 1;
                    }
                }
                if(bottomMenu){

                    bottomMenuIndex++;
                    if (bottomMenuIndex > maxCommandInBottom) {
                        bottomMenuIndex = 1;
                    }
                }

            } else if (keyCode == Canvas.LEFT) {
                if (rowMenu) {
                    rowMenuIndex--;
                    if (rowMenuIndex < 1)
                    rowMenuIndex = maxCommandInRow;
                }
                if (!rowMenu)
                {
                    bottomMenu = true;
                    bottomMenuIndex--;
                    if (bottomMenuIndex < 1) {
                        bottomMenuIndex = maxCommandInBottom;
                    }
                }

            } else if (keyCode == Canvas.FIRE) {

//                Constant.currentCategory = new Vector();
//                Constant.currentCategory.addElement((Category)Constant.category.elementAt(selectedItem)) ;
//                Constant.currentCategory.addElement((Category)Constant.category.elementAt(selectedItem)) ;
//                productCanvas = new ProductCanvas();
                iMandiMIDlet.display.setCurrent(new PaymentSuccess());

            }
        }
        Constant.printLog("rowMenuIndex:",rowMenuIndex+"");
    }

//	Image getImage(int index)
//	{
//		return images != null && images.length > index ? images[index] : null;
//	}
    FormHelper formHelper = new FormHelper();
    protected void paint(Graphics g,int startY) {

        Constant.lineNumber = "1";
        g.setFont(Constant.font_small);
        // paint List background
        g.setColor(Constant.COLOR_bgColor);
        g.fillRect(0, startY, canvas.getWidth(), canvas.getHeight());

//        if (showDetail) {
//            canvas.detailView.diaplayDetail(g, canvas,productDetail);
//            return;
//        }

//      scrollTop = startY;
        if(banks != null && banks.size()>0){
//        drawProductList(g, startY);
            int bX = 10;
            int bY = 40;
            int bW = canvas.getWidth() - 20;
            int bH = 20;
            bY = formHelper.drawButton(g,bX,bY,bW,bH,(byte)1,"Cash On Delivery",canvas.getWidth(),getHeight(),(byte)10, 0x000FF,(byte)2);
            formHelper.drawLabel(g, bX, bY, "Your goods will be delivered to you once you have paid the amount in full at the time of delivery.", canvas.getWidth(), canvas.getHeight(), (byte)10, topSpace, (byte)-1);

        }

        Constant.lineNumber = "2";
        // translate accordingly to current List vertical scroll
        g.translate(0, -scrollTop);


        g.setFont(Constant.font_small);


        // finally, translate back
        g.translate(0, scrollTop);
//        drawBottom(g);
        Constant.lineNumber = "8";
    }

    private void drawProductList(Graphics g,int startY)
    {
        int top = startY;
         // loop List items
        for (int i = 0; i < itemLines.length; i++) {
            Bank p = (Bank)banks.elementAt(i);
            int itemRows = itemLines[i].length;
            Constant.lineNumber = "3";
            Image imagePart = null;

            Constant.lineNumber = "4";
            int itemHeight = itemRows * Constant.font_small.getHeight() + linePadding * (itemRows - 1);

            itemsTop[i] = top;
            itemsHeight[i] = itemHeight;

            // is image part higher than the text part?
            if (imagePart != null && imagePart.getHeight() > itemHeight) {
                itemHeight = imagePart.getHeight();
            }
            itemHeight += 2 * padding + 2 * borderWidth;

            g.translate(0, top);

            if (borderWidth > 0) {
                // paint item border
                g.setColor(i == selectedItem ? Constant.COLOR_borderColor : Constant.COLOR_borderColor);
                g.fillRect(margin, margin + topSpace, canvas.getWidth() - 2 * margin, itemHeight);
            }
Constant.lineNumber = "5";
            // paint item background
            g.setColor(i == selectedItem ? Constant.COLOR_backSelectedColor : Constant.COLOR_backColor);
            g.drawRect(margin + borderWidth, margin + borderWidth + topSpace, canvas.getWidth() - 2 * margin - 2 * borderWidth, itemHeight - 2 * borderWidth);

            // has this item an image part?
            if (imagePart != null) {
                g.drawImage(imagePart, margin + borderWidth + padding, margin + borderWidth + padding + topSpace, Graphics.TOP | Graphics.LEFT);
            }
if(i == selectedItem)
                {
                    g.setColor(Constant.COLOR_backSelectedColor);
                    g.fillRect(margin + borderWidth, margin + borderWidth + topSpace,3, itemHeight - 2 * borderWidth);
                }
            // paint item text rows
            g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

            int textLeft = margin + borderWidth + padding + (imagePart != null ? imagePart.getWidth() + padding : 0);
//            g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

            if(maxChar<=0){
                maxChar = g.getFont().charWidth('A') ;
                maxChar = cellWidth/maxChar;
//                p.maxChar = maxChar;
            }
            if(startTextYInCell<=0){
                startTextYInCell = (cellHeight-g.getFont().getHeight())/2;
            }
            Constant.lineNumber = "6";
//            g.setColor(Constant.COLOR_ITEM_BG);
//            g.fillRect(textLeft,  margin + borderWidth + padding + topSpace,cellWidth , cellHeight);
            g.setColor(Constant.COLOR_ITEM_TEXT);
        g.drawString(p.getBankname(),textLeft+5,margin + borderWidth + padding + topSpace, cellHeight);

Constant.lineNumber = "7";
            int nextRow = 0;

             nextRow = 0;
                g.drawImage(ImageList.more, (canvas.getWidth() - ImageList.voiceRecord.getWidth()) - margin, topSpace + margin + borderWidth + padding + 0 * (linePadding + Constant.font_small.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);


Constant.lineNumber = "8";
            g.translate(0, -top);

            top += itemHeight + 2 * margin;
        }
    }

    public static String[] getTextRows(String text, Font font, int width) {
        char SPACE_CHAR = ' ';
        String VOID_STRING = "";

        int prevIndex = 0;
        int currIndex = text.indexOf(SPACE_CHAR);

        Vector rowsVector = new Vector();

        StringBuffer stringBuffer = new StringBuffer();

        String currentToken;

        String currentRowText = VOID_STRING;

        while (prevIndex != -1) {
            int startCharIndex = prevIndex == 0 ? prevIndex : prevIndex + 1;

            if (currIndex != -1) {
                currentToken = text.substring(startCharIndex, currIndex);
            } else {
                currentToken = text.substring(startCharIndex);
            }

            prevIndex = currIndex;

            currIndex = text.indexOf(SPACE_CHAR, prevIndex + 1);

            if (currentToken.length() == 0) {
                continue;
            }

            if (stringBuffer.length() > 0) {
                stringBuffer.append(SPACE_CHAR);
            }

            stringBuffer.append(currentToken);

            if (font.stringWidth(stringBuffer.toString()) > width) {
                if (currentRowText.length() > 0) {
                    rowsVector.addElement(currentRowText);
                }
                stringBuffer.setLength(0);

                currentRowText = VOID_STRING;

                stringBuffer.append(currentToken);

                currentRowText = stringBuffer.toString();
            } else {
                currentRowText = stringBuffer.toString();
            }
        }
        if (currentRowText.length() > 0) {
            rowsVector.addElement(currentRowText);
        }
        String[] rowsArray = new String[rowsVector.size()];

        rowsVector.copyInto(rowsArray);

        return rowsArray;
    }

    void drawBottom(Graphics g){
        g.setColor(Constant.COLOR_BOTTOM);
//        if(bottomMenu)
//            g.setColor(Constant.COLOR_TAB_SELECTED);

        g.fillRect(0, canvas.getHeight()-(ImageList.cartImage.getHeight()+10), canvas.getWidth(), ImageList.cartImage.getHeight()+10);
        g.drawImage(ImageList.cartImage, (canvas.getWidth()/2)-(ImageList.cartImage.getWidth()+(ImageList.cartImage.getWidth()/2)), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), Graphics.TOP | Graphics.LEFT);
        g.drawImage(ImageList.cartImage, (canvas.getWidth()/2)-(ImageList.videoRecord.getWidth()/2), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), Graphics.TOP | Graphics.LEFT);
        g.drawImage(ImageList.cartImage, (canvas.getWidth()/2)+(ImageList.cartImage.getWidth()), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), Graphics.TOP | Graphics.LEFT);

        if(CartItems.getInstance().cartItemTable.size()>0){
            g.setColor(Constant.COLOR_borderSelectedColor);
            g.setColor(0xe4de33);
            g.fillArc((canvas.getWidth()/2)-(ImageList.videoRecord.getWidth()/2), canvas.getHeight()-20, g.getFont().stringWidth(""+CartItems.getInstance().cartItemTable.size())+5, g.getFont().stringWidth(""+CartItems.getInstance().cartItemTable.size())+5,0,360);
            g.setColor(Constant.COLOR_borderSelectedColor);
            g.drawString(""+CartItems.getInstance().cartItemTable.size(), 3+(canvas.getWidth()/2)-(ImageList.videoRecord.getWidth()/2), canvas.getHeight()-20, 20);
        }
        if(bottomMenu){
            g.setColor(Constant.COLOR_borderSelectedColor);
            if(bottomMenuIndex==1)
            g.drawRect((canvas.getWidth()/2)-(ImageList.cartImage.getWidth()+(ImageList.cartImage.getWidth()/2)), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), ImageList.cartImage.getWidth(), ImageList.cartImage.getHeight());
            if(bottomMenuIndex==2)
            g.drawRect((canvas.getWidth()/2)-(ImageList.videoRecord.getWidth()/2), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), ImageList.cartImage.getWidth(), ImageList.cartImage.getHeight());
            if(bottomMenuIndex==3)
            g.drawRect((canvas.getWidth()/2)+(ImageList.cartImage.getWidth()), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), ImageList.cartImage.getWidth(), ImageList.cartImage.getHeight());
        }

    }

    int getHeight(){
        return canvas.getHeight()-(ImageList.cartImage.getHeight()*2) ;
    }
    public void recycleImage(){
        banks.removeAllElements();;
        System.gc();
    }
    Product productDetail ;
    public void sendData(String data) {
        System.out.println("sendData:" + data);
        productDetail = JsonStringToOjbect.getProduct(data);
        System.out.println("productDetail -->" + productDetail.toString());
    }

    public void sendStatus(String Status) {
//        canvas.sendStatus(Status);
    }

    public void myConnectionBusy(String Status) {
//        canvas.myConnectionBusy(Status);
    }

    public void imageLoaded(String Status,byte requestId) {

    }

    public void sendData(String data, byte requestID) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void sendStatus(String Status, byte requestID) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void myConnectionBusy(String Status, byte requestID) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
