package com.iMandi;

import com.iMandi.bean.Product;
import java.io.InputStream;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.midlet.MIDlet;

public class PlayAudio implements CommandListener, Runnable {
//  private Display mDisplay;

  private List mMainScreen;
 Canvas prevDisplay ;
 String url ;
 Product product ;
  public PlayAudio( Canvas prevDisplay,String url,Product product) {
      this.url = url ;
      this.product = product ;
//    mDisplay = Display.getDisplay(this);
this.prevDisplay =prevDisplay ;
    if (mMainScreen == null) {
      mMainScreen = new List("Mandi", List.IMPLICIT);

//      mMainScreen.append("Via HTTP", null);
//      mMainScreen.append("From resource", null);
      mMainScreen.addCommand(new Command("Back", Command.EXIT, 0));
//      mMainScreen.addCommand(new Command("Play", Command.SCREEN, 0));
      mMainScreen.setCommandListener(this);
    }

    iMandiMIDlet.display.setCurrent(mMainScreen);
     Thread t = new Thread(this);
      t.start();
  }

  public void pauseApp() {
  }

  public void destroyApp(boolean unconditional) {
  }

  public void commandAction(Command c, Displayable s) {
    if (c.getCommandType() == Command.EXIT){
            try {
//      notifyDestroyed();
                player.stop();
            } catch (MediaException ex) {
                ex.printStackTrace();
            }
        iMandiMIDlet.display.setCurrent(prevDisplay);
    }
    else {
      Form waitForm = new Form("Loading...");
      iMandiMIDlet.display.setCurrent(waitForm);
      Thread t = new Thread(this);
      t.start();
    }
  }

  public void run() {
//    String selection = mMainScreen.getString(mMainScreen.getSelectedIndex());
//    boolean viaHttp = selection.equals("Via HTTP");
//
//    if (viaHttp)
//      playViaHttp();
//    else
      playFromResource();
  }
Player player ;
  private void playViaHttp() {
    try {
      Player player = Manager.createPlayer("http://www.y.com/a.wav");
      player.start();
    } catch (Exception e) {
      showException(e);
      return;
    }
    iMandiMIDlet.display.setCurrent(mMainScreen);
  }

  private void playFromResource() {
    try {
      InputStream in = getClass().getResourceAsStream(url);//"/test.amr"
      player = Manager.createPlayer(in, "audio/amr");
      player.start();
    } catch (Exception e) {
      showException(e);
      return;
    }
    iMandiMIDlet.display.setCurrent(mMainScreen);
  }

  private void showException(Exception e) {
    Alert a = new Alert("Exception", e.toString(), null, null);
    a.setTimeout(Alert.FOREVER);
    iMandiMIDlet.display.setCurrent(a, mMainScreen);
  }
}