package com.iMandi;

import com.iMandi.bean.CartItems;
import com.iMandi.bean.Product;
import com.iMandi.bean.Profile;
import com.iMandi.bean.ServerMessage;
import com.iMandi.connection.MyConnection;
import com.iMandi.connection.RMS;
import java.io.IOException;
import java.util.Vector;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.ImageItem;
import javax.microedition.lcdui.TextField;
import org.json.me.JSONException;
import org.json.me.JSONObject;

/**
 *
 * @author nagendrakumar
 */
public class LoginScreen  implements CommandListener {

    private TextField fname,lname, address, state, town, pin, mobile, cooperative, district,userName,password,email;
    public Form form;
    private Command submit, cancel,registration;
    private Image img, imge, img2;
    Object displayPrev;
    Product product;
    public LoginScreen(Object displayPrev,Product product) {
         System.out.println("LoginScreen......");
        this.product = product;
        this.displayPrev = displayPrev;
        form = new Form(Constant.TITLE_LOGIN_SCREEN);
//         userName = new TextField("LoginID:", "", 10, TextField.ANY);
        fname = new TextField(Constant.FIELD_F_NAME, Profile.getInstance().getFirstName(), 50, TextField.ANY);
        lname = new TextField(Constant.FIELD_L_NAME, Profile.getInstance().getLastName(), 50, TextField.ANY);
        
         password = new TextField("Password:", Profile.getInstance().getPassword(), 10, TextField.PASSWORD);
         mobile = new TextField(Constant.FIELD_MOBILE_NUMBER, Profile.getInstance().getMobileNumber(), 10, TextField.NUMERIC);
         email = new TextField(Constant.FIELD_EMAIL, Profile.getInstance().getEmailId(), 30, TextField.EMAILADDR);
         town = new TextField(Constant.FIELD_LOCALITY_NO, Profile.getInstance().getLocalityTown(), 150, TextField.ANY);


//        fname = new TextField(Constant.FIELD_NAME, Profile.getInstance().getName(), 30, TextField.ANY);
//        lname = new TextField(Constant.FIELD_NAME, Profile.getInstance().getName(), 30, TextField.ANY);
        address = new TextField(Constant.FIELD_ADDRESS, Profile.getInstance().getAddress(), 30, TextField.ANY);
        pin = new TextField(Constant.FIELD_PIN_CODE, Profile.getInstance().getPinCode(), 30, TextField.ANY);
        state = new TextField(Constant.FIELD_STATE, Profile.getInstance().getStatus(), 30, TextField.ANY);
        cooperative = new TextField(Constant.FIELD_COOPERATIVE_NO, Profile.getInstance().getCooperativeNo(), 30, TextField.ANY);
        district = new TextField(Constant.FIELD_DISTRICT, Profile.getInstance().getDistrict(), 30, TextField.ANY);
        
        cancel = new Command("Back", Command.CANCEL, 2);
        submit = new Command("Sumbit", Command.OK, 2);
        registration = new Command("Registration", Command.OK, 2);
        try {
            img = Image.createImage("logo.png");
            imge = Image.createImage("/front_left1_bad.png");
            img2 = Image.createImage("/Congratulations-1.png");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void display() {
        try {
            form.append(new ImageItem("", img, ImageItem.LAYOUT_CENTER, null));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
//        form.append(name);
//        form.append(address);
//        form.append(pin);
//        form.append(district);
//        form.append(state);
//        form.append(town);
//        form.append(cooperative);

        form.append(fname);
        form.append(lname);
        form.append(password);
        form.append(mobile);
        form.append(email);
        form.append(town);


        form.addCommand(cancel);
        form.addCommand(submit);
//        form.addCommand(registration);
        form.setCommandListener(this);
        iMandiMIDlet.display.setCurrent(form);
    }
Alert REGISTRING_ALERT ;
    public void commandAction(Command c, Displayable d) {
        if (c == cancel) {
            iMandiMIDlet.display.setCurrent((Canvas) displayPrev);
        }
        else if (c == submit) {
            try {
                JSONObject inner = new JSONObject();
                try {
                    if (1 == 1) {
                        if (fname.getString() == null || fname.getString().trim().length() < 3) {
                            Alert alert = new Alert("Info", "Enter First Name,  min 3 character!", null, AlertType.INFO);
//                    alert.setTimeout(2000);    // for 3 seconds
                            iMandiMIDlet.display.setCurrent(alert);
                            return;
                        } else if (lname.getString() == null || lname.getString().trim().length() < 3) {
                            Alert alert = new Alert("Info", "Enter last name, min 3 character!", null, AlertType.INFO);
//                    alert.setTimeout(2000);    // for 3 seconds
                            iMandiMIDlet.display.setCurrent(alert);
                            return;
                        } else if (password.getString() == null || password.getString().trim().length() < 4) {
                            Alert alert = new Alert("Info", "Enter password min 4 character!", null, AlertType.INFO);
//                    alert.setTimeout(2000);    // for 3 seconds
                            iMandiMIDlet.display.setCurrent(alert);
                            return;
                        } else if (mobile.getString() == null || (mobile.getString().trim().length() < 10 || mobile.getString().trim().length() > 10)) {
                            Alert alert = new Alert("Info", "Enter valid mobile,min and max 10 character ", null, AlertType.INFO);
//                    alert.setTimeout(2000);    // for 3 seconds
                            iMandiMIDlet.display.setCurrent(alert);
                            return;
                        } else if (email.getString() == null || email.getString().trim().length() < 0 || email.getString().trim().indexOf("@") == -1 || email.getString().trim().indexOf(".") == -1) {
                            Alert alert = new Alert("Info", "Enter valid email", null, AlertType.INFO);
//                    alert.setTimeout(2000);    // for 3 seconds
                            iMandiMIDlet.display.setCurrent(alert);
                            return;
                        }
//                        else if(town.getString() == null || town.getString().trim().length()<10){
//                            Alert alert = new Alert("Info", "Enter valid email", null, AlertType.INFO);
//                            HelloMIDlet.display.setCurrent(alert);
//                            return;
//                        }

                    }

//
                    REGISTRING_ALERT = new Alert("Info", "", null, AlertType.INFO);
                    REGISTRING_ALERT.setString(Constant.REGISTRING);
                    REGISTRING_ALERT.setTimeout(1000);
                    REGISTRING_ALERT.setType(AlertType.INFO);
                    REGISTRING_ALERT.addCommand(Alert.DISMISS_COMMAND);
                    iMandiMIDlet.display.setCurrent(REGISTRING_ALERT);
                    Thread.sleep(100);

                    inner.put("customer_email", email.getString());
                    inner.put("customer_fname", fname.getString());
                    inner.put("customer_lname", lname.getString());
                    inner.put("customer_password", password.getString());
                    inner.put("address", town.getString());

                    inner.put("latitude", "0.0");
                    inner.put("longitude", "0.0");
                    inner.put("profilepic", "");
                    inner.put("regId", "1020");
                    inner.put("devicemake", "j2me");
                    inner.put("devicemodel", "J2me");
                    inner.put("devicemake", "j2me");
                    inner.put("devicetype", "J2me");
                    inner.put("device_width", "220");
                    inner.put("device_height", "200");
                    Vector v = new Vector();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                System.out.println(inner.toString());
                Constant.printLog("Registration::", inner.toString());
                ServerMessage serverMessage = registration(inner.toString());
                REGISTRING_ALERT.setTimeout(10);
                Thread.sleep(100);
//                    REGISTRING_ALERT.
                if (serverMessage.getStatus() == 1) {
                    addToCart();
//                HelloMIDlet.display.setCurrent((Canvas)displayPrev);
                    Alert alert = new Alert("Info", serverMessage.getMessage(), null, AlertType.INFO);
                    alert.setTimeout(2000); // for 3 seconds
                    iMandiMIDlet.display.setCurrent(alert, (Canvas) displayPrev); // so that it goes to back to your canvas after displaying the alert
                    ((Canvas) displayPrev).repaint();
//                Profile.getInstance().setUserId("1020");
                } else {
                    Profile.getInstance().setUserId(null);
//                serverMessage
                    Alert alert = new Alert("Info", serverMessage.getMessage(), null, AlertType.INFO);
                    alert.setTimeout(2000); // for 3 seconds
                    iMandiMIDlet.display.setCurrent(alert); // so that it goes to back to your canvas after displaying the alert
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            
        }
       else if (c == registration) {
        RegistrationScreen registrationScreen = new RegistrationScreen(displayPrev, product) ;
        registrationScreen.display();
       }

    }

    private void addToCart(){
        if(product == null)
            return; 
         product.setProductCartQuentity(1);

                    if(product.getProductCartQuentity()>0){
                        CartItems.getInstance().addItem(product,null);
                    }
                    else{
                         CartItems.getInstance().removeItem(product,null);
                    }
    }

    ServerMessage registration(String jsonData) {
//        JSONObject inner = new JSONObject();
        
        ServerMessage serverMessage = new ServerMessage() ;
        try {
            String response = MyConnection.getInstance().postData(Constant.URL_BASE + Constant.URL_REGISTRATION, jsonData);
//           ic Product ge]tProduct(String jsonText)
//           {
            Profile profile = Profile.getInstance();
            Constant.printLog(Constant.URL_BASE + Constant.URL_REGISTRATION, response);
            try {
                JSONObject json = new JSONObject(response);

//                 JSONObject json = new JSONObject(jsonText);


                if (json.has("status")) {
                        serverMessage.setStatus(json.getInt("status"));
                }
                if (json.has("message")) {
                        serverMessage.setMessage(json.getString("message"));
                }
                if (json.has("session")) {
                        profile.setSession(json.getString("session"));
                    }
                if (json.has("user_data")) {
                    JSONObject user_data = new JSONObject(json.getString("user_data"));
                    if (user_data.has("user_id")) {
                        profile.setUserId(user_data.getString("user_id"));
                    }
                    if (user_data.has("first_name")) {
                        profile.setFirstName(user_data.getString("first_name"));
                    }
                    if (user_data.has("last_name")) {
                        profile.setLastName(user_data.getString("last_name"));
                    }
                    if (user_data.has("profile_pic")) {
                        profile.setProfilepic(user_data.getString("profile_pic"));
                    }
                    if (user_data.has("email_id")) {
                        profile.setEmailId(user_data.getString("email_id"));
                    }
                    if (user_data.has("address")) {
                        profile.setAddress(user_data.getString("address"));
                    }

                    RMS.getInstance().setRecord(response, 1);
                    try{
//                    RMS.getInstance().writeTextFile("setting.txt",response);
//                    System.out.println("RMS----FILE-------->"+RMS.getInstance().readFile("setting.txt"));
//                    String responsee = RMS.getInstance().readFile("setting.txt");
//            Alert alert = new Alert("Info",responsee, null, AlertType.INFO);
//                    HelloMIDlet.display.setCurrent(alert);

                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    
                }
                Constant.printLog("Profile ", Profile.getInstance().toString());
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
//		return jsonArray.toString();
            return serverMessage ;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null ;
//		return jsonArray.toString();
    }

}