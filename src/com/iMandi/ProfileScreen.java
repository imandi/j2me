package com.iMandi;

import com.iMandi.Constant;
import com.iMandi.bean.Profile;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.*;

/**
 *
 * @author nagendrakumar
 */
public class ProfileScreen implements CommandListener {

    private TextField name, address, state, town, pin, mobile, cooperative, district;
    public Form form;
    private Command submit, cancel;
    private Image img, imge, img2;
    Object displayPrev;

    public ProfileScreen(Object displayPrev) {
        this.displayPrev = displayPrev;
        form = new Form(Constant.TITLE_PROFILE_SCREEN);
        name = new TextField(Constant.FIELD_NAME, Profile.getInstance().getName(), 30, TextField.ANY);
        address = new TextField(Constant.FIELD_ADDRESS, Profile.getInstance().getAddress(), 30, TextField.ANY);
        pin = new TextField(Constant.FIELD_PIN_CODE, Profile.getInstance().getPinCode(), 30, TextField.ANY);
        state = new TextField(Constant.FIELD_STATE, Profile.getInstance().getStatus(), 30, TextField.ANY);
        mobile = new TextField(Constant.FIELD_MOBILE_NUMBER, Profile.getInstance().getMobileNumber(), 30, TextField.ANY);
        cooperative = new TextField(Constant.FIELD_COOPERATIVE_NO, Profile.getInstance().getCooperativeNo(), 30, TextField.ANY);
        district = new TextField(Constant.FIELD_DISTRICT, Profile.getInstance().getDistrict(), 30, TextField.ANY);
        town = new TextField(Constant.FIELD_LOCALITY_NO, Profile.getInstance().getLocalityTown(), 30, TextField.ANY);

        cancel = new Command("Back", Command.CANCEL, 2);
        submit = new Command("Sumbit", Command.OK, 2);
        try {
            img = Image.createImage("logo.png");
            imge = Image.createImage("/front_left1_bad.png");
            img2 = Image.createImage("/Congratulations-1.png");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    

    public void display() {
        try {
            form.append(new ImageItem("", img, ImageItem.LAYOUT_CENTER, null));
        } catch (Exception e) {
        }
        form.append(name);
        form.append(address);
        form.append(pin);
        form.append(district);
        form.append(state);
        form.append(town);
        form.append(cooperative);

        form.addCommand(cancel);
        form.addCommand(submit);
        form.setCommandListener(this);
        iMandiMIDlet.display.setCurrent(form);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cancel) {
            iMandiMIDlet.display.setCurrent((Canvas) displayPrev);
        }
        else if (c == submit) {
            CartCanvas canvas = new CartCanvas();
            canvas.prevDisplay = displayPrev ;
            iMandiMIDlet.display.setCurrent(canvas);
        }


    }
}
