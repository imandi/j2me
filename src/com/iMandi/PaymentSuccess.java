package com.iMandi;

import com.iMandi.bean.CartItems;
import java.util.Timer;
import java.util.TimerTask;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class PaymentSuccess extends ImandiCanvas {

    public PaymentSuccess() {
        title="Order confirmation";
        LEFT_MENU = "Home";
        setFullScreenMode(true);
        processing = false;

       
    }

    boolean processing = true ;

    protected void paint(Graphics g) {
        super.paint(g);
        String s1 = "Congratulations!" ;
        String s2 = "Your order has been Placed" ;
        String s3 = "Please pay "+CartItems.getInstance().total_amount+"/-" ;
        String s4 = "at the time of delivery!" ;
        String s5 = "Estimated Delivery Date" ;
        String s6 = "12/12/2017" ;

        int margin = 10;
        g.setFont(Constant.font_medium_bold);
        g.setColor(Constant.COLOR_BLACK);
        String s = s1;//+System.getProperty("microedition.platform");
        int stringWidth = g.getFont().stringWidth(s);
        int stringHeight = g.getFont().getHeight();
        int x = (getWidth() - stringWidth) / 2;
        int y = getHeight() / 2;


        
        g.drawString(s1, x, 30, Graphics.TOP | Graphics.LEFT);


        g.setFont(Constant.font_small);
        stringWidth = g.getFont().stringWidth(s2);
        stringHeight = g.getFont().getHeight();
        x = (getWidth() - stringWidth) / 2;
        y = getHeight() / 2;
        g.drawString(s2, x, 30+g.getFont().getHeight()+margin, Graphics.TOP | Graphics.LEFT);

//        g.setColor(Constant.COLOR_LIST_CART_HEADING);
        stringWidth = g.getFont().stringWidth(s3);
        stringHeight = g.getFont().getHeight();
        x = (getWidth() - stringWidth) / 2;
        y = getHeight() / 2;
        g.drawString(s3, x, 30+(g.getFont().getHeight()*2)+margin, Graphics.TOP | Graphics.LEFT);

        g.setColor(Constant.COLOR_BLACK);
        stringWidth = g.getFont().stringWidth(s4);
        stringHeight = g.getFont().getHeight();
        x = (getWidth() - stringWidth) / 2;
        y = getHeight() / 2;
        g.drawString(s4, x, 30+(g.getFont().getHeight()*3)+margin, Graphics.TOP | Graphics.LEFT);

//        g.setFont(Constant.font_small);
//        g.setColor(Constant.COLOR_backSelectedColor);
        stringWidth = g.getFont().stringWidth(s5);
        stringHeight = g.getFont().getHeight();
        x = (getWidth() - stringWidth) / 2;
        y = getHeight() / 2;
        g.drawString(s5, x, 30+(g.getFont().getHeight()*5)+margin, Graphics.TOP | Graphics.LEFT);

//        g.setColor(Constant.COLOR_backSelectedColor);
        stringWidth = g.getFont().stringWidth(s6);
        stringHeight = g.getFont().getHeight();
        x = (getWidth() - stringWidth) / 2;
        y = getHeight() / 2;
        g.drawString(s6, x, 30+(g.getFont().getHeight()*6)+margin, Graphics.TOP | Graphics.LEFT);

        drawBottomMenu();
    }


    protected void paintXXX(Graphics g) {
        super.paint(g);
        g.setFont(Constant.font_small);
        g.setColor(Constant.COLOR_PAYMENT_SCREEN_BG);
        g.fillRect(0, 0, getWidth(), getHeight());

        g.setColor(255, 255, 255);

//        Font mFont = g.getFont();
        if(!processing){
        String s = Constant.ORDER_SUCCESSFULLY_PLACED;//+System.getProperty("microedition.platform");
        int stringWidth = Constant.font_small.stringWidth(s);
        int stringHeight = Constant.font_small.getHeight();
        int x = (getWidth() - stringWidth) / 2;
        int y = getHeight() / 2;

        g.setFont(Constant.font_small);
        g.drawString(s, x, (y-(g.getFont().getHeight())), Graphics.TOP | Graphics.LEFT);

        s = Constant.ORDER_SUCCESSFULLY_PLACED_2;//+System.getProperty("microedition.platform");
        stringWidth = Constant.font_small.stringWidth(s);
        stringHeight = Constant.font_small.getHeight();
        x = (getWidth() - stringWidth) / 2;
        y = getHeight() / 2;
        g.drawString(s, x, y, Graphics.TOP | Graphics.LEFT);

        s = Constant.ORDER_SUCCESSFULLY_PLACED_3;//+System.getProperty("microedition.platform");
        stringWidth = Constant.font_small.stringWidth(s);
        stringHeight = Constant.font_small.getHeight();
        x = (getWidth() - stringWidth) / 2;
        y = getHeight() / 2;
        g.drawString(s, x, y+(g.getFont().getHeight()*1), Graphics.TOP | Graphics.LEFT);

        s = Constant.ORDER_SUCCESSFULLY_PLACED_4;//+System.getProperty("microedition.platform");
        stringWidth = Constant.font_small.stringWidth(s);
        stringHeight = Constant.font_small.getHeight();
        x = (getWidth() - stringWidth) / 2;
        y = getHeight() / 2;
        g.drawString(s, x, y+(g.getFont().getHeight()*3), Graphics.TOP | Graphics.LEFT);


        stringWidth = ImageList.logo.getWidth();
        stringHeight = ImageList.logo.getHeight();
        x = (getWidth() - stringWidth) / 2;
        y = getHeight() / 2;
        g.drawImage(ImageList.logo, x, y - 60, Graphics.TOP | Graphics.LEFT);

        stringWidth = ImageList.thumbs_up.getWidth();
        stringHeight = ImageList.thumbs_up.getHeight();
        x = (getWidth() - stringWidth) / 2;
        y = getHeight() / 2;

        g.drawImage(ImageList.thumbs_up, x, y +(g.getFont().getHeight()*5), Graphics.TOP | Graphics.LEFT);
//        drawBottom(g);
        }else{
            String s = Constant.ORDER_PROCESSING;//+System.getProperty("microedition.platform");
            int stringWidth = Constant.font_small.stringWidth(s);
            int stringHeight = Constant.font_small.getHeight();
            int x = (getWidth() - stringWidth) / 2;
            int y = getHeight() / 2;

            g.setFont(Constant.font_small);
            g.drawString(s, x, y, Graphics.TOP | Graphics.LEFT);
        }
//        drawTitle(title=Constant.CATEGORY_TITLE);
        drawBottomMenu();
    }

    public void keyPressed(int key) {
        if(processing){
            return ;
        }
        int keyCode = getGameAction(key);

        if (!Constant.isKnownKey(this, key)) {
            if (key == Constant.KEY_SOFT_LEFT) {
                removeAllItem();
                Utility.openLoginAsGuest(null);
            } else //if (key == canvas.KEY_SOFT_LEFT)
            {
                removeAllItem();
                Utility.openLoginAsGuest(null);
            }
        }
    }

    private void removeAllItem(){
         Timer aTimer = new Timer();
            TimerTask ttask = new TimerTask() {
                public void run() {
                   repaint();
                     CartItems.getInstance().removeItem(null, null);
                     CartItems.getInstance().cartItemTable.removeAllElements();
                   processing = false ;
                }
            };
            aTimer.schedule(ttask, 100);
    }

//    private void drawTitle(Graphics g) {
//        Font o = g.getFont();
//        g.setFont(Constant.font_medium);
//        g.setColor(Constant.COLOR_HEADER);
//        g.fillRect(0, 0, getWidth(), Constant.TITLE_HEIGHT);
//
//        g.setColor(255, 255, 255);
//
//
//        String s = Constant.CATEGORY_TITLE;
//        int stringWidth = g.getFont().stringWidth(s);
//        int stringHeight = g.getFont().getHeight();
//        int x = (getWidth() - stringWidth) / 2;
//        int y = getHeight() / 2;
//
////        g.setFont(mFont);
//        g.drawString(s, x, 0, Graphics.TOP | Graphics.LEFT);
//        g.drawImage(ImageList.logo_20x20, 0, 0, Graphics.TOP | Graphics.LEFT);
//        g.setFont(o);
//    }

//    void drawBottom(Graphics g) {
//        g.setColor(Constant.COLOR_BOTTOM);
//        g.fillRect(0, getHeight() - (Constant.HEIGHT_BOTTOM), getWidth(), Constant.HEIGHT_BOTTOM);
//        g.setColor(Constant.COLOR_MENU_TEXT);
//        g.drawString("Continue Shopping", 2, getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
//
//    }
}
