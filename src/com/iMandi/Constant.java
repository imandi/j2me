package com.iMandi;

import com.iMandi.bean.Bank;
import com.iMandi.bean.ClientParam;
import java.util.Vector;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author nagendrakumar
 */
public class Constant {

    public static final boolean CHEAT_CODE = false;

    public static final String VERSION = "v 1.3.5";

    public static final String REGISTRING = "Registring....";
    public static final String TO_DO = "Kaam chal raha hai!";

    public static final String LOADING = "Processing...";
    public static final String LOADING_CART = "Checking cart...";
    public static final String LOADING_IMAGES = "Loading...";

    public static final String TITLE_PROFILE_SCREEN = " Confirm User DEtails";
    public static final String TITLE_LOGIN_SCREEN = " Login";
    public static final String TITLE_REGISTRATION_SCREEN = " Registration";
    public static final String TITLE_CART_DETAIL = "Confirm Order";
    public static final String TITLE_CONFIRM_DELIVERY = "Confirm Delivery";
    public static final String TITLE_PAYMENT = "Payment";
    public static final String TITLE_PAYMENT_VERIFICATION = "Payment Verification";
    public static final String TITLE_ORDER_CONFIRMATION = "Order Confirmation";
    public static final String FIELD_NAME = "Name";
    public static final String FIELD_F_NAME = "First Name";
    public static final String FIELD_L_NAME = "Last Name";
    public static final String FIELD_EMAIL = "Email";
    public static final String FIELD_ADDRESS = "Address";
    public static final String FIELD_PIN_CODE = "Pin Code";
    public static final String FIELD_DISTRICT = "District";
    public static final String FIELD_STATE = "State";
    public static final String FIELD_MOBILE_NUMBER = "Mobile Number";
    public static final String FIELD_COOPERATIVE_NO = "Cooperative No.";
    public static final String FIELD_LOCALITY_NO = "Locality/Town";
    public static final String LIST_TITLE_PRODUCT_NAME = "PRODUCT";
    public static final String LIST_TITLE_PRODUCT_QUENTITY = "QTY";
    public static final String LIST_TITLE_PRODUCT_SIZE = "SIZE";
    public static final String LIST_TITLE_PRODUCT_PRICE = "PRICE";
    public static final String LIST_TITLE_CONTINUE_SHOPING = "Add item";
    public static final String DELETE_ALL = "Delete";
    public static final String PLACE_ORDER = "Checkout";
    public static final String LIST_TITLE_SUB_TOTAL = "Cart Total: ";
    public static final String CATEGORY_TITLE = "Welcome to iMandi";
    public static final String[] TITLE_TAB = new String[]{"Fertilizer"};
    public static final String BANK_SCREEN_TITLE = "Select Payment Option";
    public static final String ORDER_SUCCESSFULLY_PLACED = "Thank you for the payment";
    public static final String ORDER_SUCCESSFULLY_PLACED_2 = "Please collect your order";
    public static final String ORDER_SUCCESSFULLY_PLACED_3 = "from the below address ";
    public static final String ORDER_SUCCESSFULLY_PLACED_4 = "IFFCO Cooperative";
    public static final String ORDER_PROCESSING = "Processing, Please wait...";


    public static final int COLOR_WHITE = 0xffffff;
    public static final int COLOR_BLACK = 0x000000;
    public static final int[] COLOR_SELECTED_BORDER = new int[]{1, 2, 3};
    public static final int[] COLOR_SELECTED_ROW = new int[]{1, 2, 3};
    public static final int[] COLOR_TITLE_BG = new int[]{1, 2, 3};
    public static final int[] COLOR_TITLE_TEXT = new int[]{1, 2, 3};
    public static final int[] COLOR_FOOTER_BG = new int[]{1, 2, 3};
    public static final int[] COLOR_FOOTR_TEXT = new int[]{1, 2, 3};
    public static final int COLOR_bgColor = 0xf2f2f2;
    public static final int COLOR_foreColor = 0x000000;
    public static final int COLOR_foreSelectedColor = 0x000000;//ow selected text color
    public static final int COLOR_backColor = 0xffffff;
    public static final int COLOR_backSelectedColor = 0x4bbcd4;//0x90f652;//row selected color
    public static final int COLOR_borderColor = 0xffffff;
    public static final int COLOR_borderSelectedColor = 0x28a5c9;
    public static final int COLOR_ITEM_BG = 0xff9a00;
    public static final int COLOR_ITEM_TEXT = 0x000000;
    public static final int COLOR_BOTTOM = 0xe7e8ed;
    public static final int COLOR_ALERT_BG = 0xd6641b;
    public static final int COLOR_ALERT_HEADER = 0x424242;
    public static final int COLOR_HEADER_FONT = COLOR_BLACK;//0xffffff;
    public static final int COLOR_BORDER = 0xbbbbbb;
    public static final int COLOR_CART_COUNTER = 0xe4de33;
    public static final int COLOR_TAB_SELECTED = 0x61be1a;//splash bh
    public static final int COLOR_FORM_FIELD_SELECTED = 0x000000;//splash bh
    public static final int COLOR_MENU_TEXT = 0xFFFFFF;//splash bh
    public static final int COLOR_PAYMENT_SCREEN_BG = 0xcccccc;

    public static final int COLOR_TAB_UN_SELECTED = 0xcccccc;

    public static final int COLOR_LIST_CART_HEADING = 0xffb732;
    public static final int COLOR_LOADING_BG = COLOR_WHITE;
    public static final int COLOR_LOADING_TEXT = COLOR_BLACK;

    public static final int COLOR_HEADER = COLOR_LIST_CART_HEADING;//0x423d47;
    

//product_details_j2mi
    public static final int HEIGHT_BOTTOM = 20;
    public static final int TITLE_HEIGHT = 23;
    public static final String QUENTITY_VALUE[] = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    public static final String URL_BASE = "http://velenty.com/imandi/api_v2/";
//    public static final String URL_PRODUCT_LIST = URL_BASE+"productslist.php?device=j2me&categoryid=";
    public static final String URL_PRODUCT_LIST = URL_BASE+"productslist.php?device=j2me&categoryid=";
//    http://velenty.com/imandi/api_v2/productslist.php?categoryid=3
    //http://velenty.com/imandi/api_v2/get_home_category.php
    //http://velenty.com/imandi/api_v2/product_detail_view_j2me.php?id=455&customer_id=45
//    public static final String URL_PRODUCT_DETAIL = URL_BASE+"product_detail_view.php?device=j2me&clientVersion=iMandi_J2me_1_0_0&customer_id=71&id=";
    public static final String URL_PRODUCT_DETAIL = URL_BASE+"product_detail_view_j2me.php?device=j2me&clientVersion=iMandi_J2me_1_0_0&customer_id=71&id=";
//    public static final String URL_PRODUCT_CATEGORY = URL_BASE+"get_category.php";
        public static final String URL_PRODUCT_CATEGORY = URL_BASE+"get_home_category.php";
//    public static final String URL_REGISTRATION = "user_register.php";
    public static final String URL_ADD_TO_CART = "add_cart.php";
    //http://velenty.com/imandi/api_v2/add_cart.php
    public static final String URL_REMOVE_TO_CART = "set_cart_empty.php";
    public static final String URL_GET_CART_DATA = "get_cart_data.php";
    public static final String URL_APP_VALIDITY = "http://104.236.120.112:8010/mood/API/api.php?_api_call=api.app.validity&api_version=v_1&app_name=imandi-j2me";

    public static final String URL_REGISTRATION = "http://13.126.89.206/tiger/rest/user/register";
    public static final String URL_GENERATE_OTP = "http://13.126.89.206/tiger/rest/user/mobileverification/generatecode?userId=";
    public static final String URL_VERIFY_OTP = "http://13.126.89.206/tiger/rest/user/mobileverification/matchcode?userId=#userId#&code=#OTP#&token=865800226639641020&imei=865800226639641020&clientVersion=iMandi_J2me_1_0_0";
//    public static final String URL_SET_VERIFY = "http://13.126.89.206/tiger/rest/user/mobileverification/verify?userId=#userId#&token="+ClientParam.IMEI+"&imei="+ClientParam.IMEI+"&clientVersion="+ClientParam.VERSION;
    public static final String URL_PROFILE = "http://13.126.89.206/tiger/rest/user/profile/get?userName=";
    public static final String URL_PROFILE_UPDATE = "http://13.126.89.206/tiger/rest/user/profile/update?userId=";

    public static void printLog(String tag, String message) {
        System.out.println(tag + message);
    }
    public static long initFreeMemory = 0;
    public static long currentFreeMemory = 0;
    public static void printMemory() {
        Runtime r = Runtime.getRuntime();
        long mem1, mem2;
        Integer someints[] = new Integer[1000];
        System.out.println("Total memory is: " +
                r.totalMemory());
        mem1 = r.freeMemory();
        System.out.println("Initial free memory: " + mem1);
        r.gc();
        mem1 = r.freeMemory();
        if(initFreeMemory==0)
            initFreeMemory = mem1 ;
        currentFreeMemory = mem1;
        System.out.println("Free memory after garbage collection: " + mem1);
    }
    public static String error = null ;
    public static String lineNumber = null ;
    public static Vector category;
    public static Vector currentCategory;

    public static Vector bankList;
    public static Vector currentBank;

    public static void loading(Graphics g,Canvas canvas,String s)
    {
//          String s = Constant.LOADING;//"Loading";
            g.setFont(Constant.font_medium);
            int stringWidth = g.getFont().stringWidth(s);
            int stringHeight = g.getFont().getHeight();
            int x = (canvas.getWidth() - stringWidth) / 2;
            int y = canvas.getHeight() / 2;

            
            g.setColor(Constant.COLOR_LOADING_BG);
            g.fillRect(x-15, y+2, stringWidth+30, stringHeight+5);
            g.setColor(Constant.COLOR_LOADING_TEXT);

            g.drawString(s, x-2, y+5, Graphics.TOP | Graphics.LEFT);
    }

    public static boolean isKnownKey(Canvas canvas, int keyCode) {
        return (keyCode >= Canvas.KEY_NUM0 && keyCode <= Canvas.KEY_NUM9) ||
                keyCode == Canvas.KEY_POUND || keyCode ==Canvas. KEY_STAR ||
                canvas.getGameAction(keyCode) != 0;
    }

    public static int KEY_SOFT_LEFT = -6;

    static{
        Bank bank = new Bank();
        bank.setBankid("1");
        bank.setBankname("COD") ;

        bankList = new Vector();
        bankList.addElement(bank);
    }
   public static Font font_small = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
   public static Font font_medium = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_MEDIUM);
   public static Font font_medium_bold = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
   public static Font font_large = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_LARGE);

   public static void showError(String msg){
                           Alert alert = new Alert("Info", msg, null, AlertType.INFO);
                    alert.setTimeout(3000);    // for 3 seconds
                    iMandiMIDlet.display.setCurrent(alert);    // so that it goes to back to your canvas after displaying the alert

   }
   public static void showError(String msg,final Displayable next){
                           Alert alert = new Alert("Info", msg, null, AlertType.INFO);
                    alert.setTimeout(3000);    // for 3 seconds
                           
                    iMandiMIDlet.display.setCurrent(alert,next);

   }
   public static void sleep(long time){
       try{
            Thread.sleep(time);
            }catch(Exception e){}
   }
   public static String replace( String str, String pattern, String replace )
{
    int s = 0;
    int e = 0;
    StringBuffer result = new StringBuffer();

    while ( (e = str.indexOf( pattern, s ) ) >= 0 )
    {
        result.append(str.substring( s, e ) );
        result.append( replace );
        s = e+pattern.length();
    }
    result.append( str.substring( s ) );
    return result.toString();
}
   public static String ENCRYPTION_KEY = "Sleep is essential to human body as during the time of sleeping." ;

   public static boolean isEmpty(String str){
       Constant.printLog("isEmpty::", str);
       if(str==null || str.trim().length()<=0)
           return true;
       else
           return false;
   }
}
