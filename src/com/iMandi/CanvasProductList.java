package com.iMandi;

import com.iMandi.bean.Product;
import com.iMandi.bean.CartItems;
import com.iMandi.bean.Profile;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.MyConnection;
import com.iMandi.connection.MyConnectionInf;
import com.iMandi.bean.CartItems;
import java.util.Vector;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import org.json.me.util.ImageList;

public class CanvasProductList implements MyConnectionInf {

    protected int linePadding = 2;
    protected int topSpace = 0;
    protected int margin = 2;
    protected int padding = 2;
//    public static Font font = Font.getDefaultFont();
    boolean showDetail = false;
    protected int borderWidth = 1;
    // will contain item splitted lines
    String[][] itemLines = null;
    // will hold items image parts
    Image[] images = null;
    // will hold selected item index
    public int selectedItem = 0;
    // these will hold item graphical properties
    int[] itemsTop = null;
    int[] itemsHeight = null;
    // these will hold List vertical scrolling
    int scrollTop = 0;
    final int SCROLL_STEP = 40;
    private ProductCanvas canvas;
    boolean rowMenu = false;
    boolean bottomMenu = false;
    int rowMenuIndex = 0;
    int bottomMenuIndex = 0;
    int maxCommandInRow = 3;
    int maxCommandInBottom = 3;
    int cellWidth = 75;
    int cellHeight = 20;
    int startTextYInCell = 0;
    int maxChar = 0;
//	public CanvasProductList(String title, String[] items, Image[] imageElements,TabMenuCanvas canvas)
//	{
//            this.canvas = canvas;
//
//            try{
//                    voiceRecord = Image.createImage("voice_record.png");
//                    videoRecord = Image.createImage("video_record.jpeg");
//                            }
//catch(Exception e)
//{
//	e.printStackTrace();
//}
//
////		canvas.setTitle(title);
//
////		this.images = imageElements;
//
//		itemLines = new String[items.length][];
//
//		itemsTop = new int[itemLines.length];
//		itemsHeight = new int[itemLines.length];
//
//		for(int i = 0; i < itemLines.length; i++)
//		{
//			// get image part of this item, if available
//			Image imagePart =product;// getImage(i);
//
//			// get avaiable width for text
//			int w = getItemWidth() - (imagePart != null ? imagePart.getWidth() + padding : 0);
//
//			// and split item text into text rows, to fit available width
//			itemLines[i] = getTextRows((String) items[i], font, w);
//		}
//	}
    Vector products;
//    public static Font font;

    public CanvasProductList(ProductCanvas canvas) {
        this.canvas = canvas;
//        font = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);
    }

    public CanvasProductList(String title, Vector products, ProductCanvas canvas) {
        this.products = products;
        this.canvas = canvas;
//        font = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_SMALL);


//		canvas.setTitle(title);

//		this.images = imageElements;

        itemLines = new String[products.size()][];

        itemsTop = new int[products.size()];
        itemsHeight = new int[products.size()];

        for (int i = 0; i < itemLines.length; i++) {
            // get image part of this item, if available
            Image imagePart = ImageList.product;//getImage(i);

            // get avaiable width for text
            int w = getItemWidth() - (imagePart != null ? imagePart.getWidth() + padding : 0);

            // and split item text into text rows, to fit available width
            itemLines[i] = getTextRows((String) ((Product) products.elementAt(i)).getProductName(), Constant.font_small, w);
        }
        new Thread(new Runnable() {

            public void run() {
                for (int i = 0; i < 10; i++) {
                    try {
                        Thread.sleep(300);
                        keyPressed(-2);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });//.start();
        cellWidth = canvas.getWidth() - 45;
        cellWidth = cellWidth - 15;
        cellWidth = cellWidth - (margin * 5);
        cellWidth = cellWidth / 2;
        Constant.printLog("cellWidth:", "" + cellWidth);
        Constant.printLog("canvas width:", "" + canvas.getWidth());
    }

    public int getItemWidth() {
        return canvas.getWidth() - 2 * borderWidth - 2 * padding - 2 * margin;
    }

    public void keyPressed(int key) {
        if (!Constant.isKnownKey(canvas, key)) {
            if (key == Constant.KEY_SOFT_LEFT) {
//            fireLeftSoftCommand();
                if (showDetail) {
                     detailY = 0;
                     detailDown = true ;
                    ImageList.images.removeAllElements();

                    showDetail = false;
                    canvas.repaint();
productDetail = null ;
System.gc(); ;
                   

                } else {
                    iMandiMIDlet.display.setCurrent(new CategoryScreen());
                }
            } else //if (key == canvas.KEY_SOFT_LEFT)
            {
                 detailY = 0;
//                    ImageList.images.removeAllElements();
                   
//                 (new ProfileScreen(canvas)).display();
                 if(Profile.getInstance().getUserId()!= null){
                    CartCanvas canvas = new CartCanvas();
                canvas.prevDisplay = canvas ;
                iMandiMIDlet.display.setCurrent(canvas);
                    }else{
                        CartCanvas canvas = new CartCanvas();
                        LoginScreen loginScreen = new LoginScreen(canvas,null);
                        loginScreen.display();
//                        HelloMIDlet.display.setCurrent();
                    }
            }
        }
        System.out.println("key:" + key);
        int keyCode = canvas.getGameAction(key);

        // is there 1 item at least?
        if (itemLines.length > 0) {
            // going up
            if (keyCode == Canvas.UP) {
                if(showDetail){
                    detailY = detailY +100;
                    if(detailY>0)
                        detailY =0;
                    return;
                }
                // current item is clipped on top, so can scroll up
                if (itemsTop[selectedItem] < scrollTop) {
                    scrollTop -= SCROLL_STEP;

                    canvas.repaint();
                } // is there a previous item?
                else if (selectedItem > 0) {
                    selectedItem--;

                    canvas.repaint();
                }
                rowMenuIndex = 0;
                rowMenu = false;
                bottomMenu = false;
            } //going down
            else if (keyCode == Canvas.DOWN) {
                if(showDetail){
                    if(detailDown)
                    detailY = detailY -100;

                    return;
                }
                // current item is clipped on bottom, so can scroll down
                if (itemsTop[selectedItem] + itemsHeight[selectedItem] >= scrollTop + getHeight()) {
                    scrollTop += SCROLL_STEP;

                    canvas.repaint();
                } // is there a following item?
                else if (selectedItem < itemLines.length - 1) {
                    selectedItem++;

                    canvas.repaint();
                }
                rowMenu = false;
                bottomMenu = false;
                rowMenuIndex = 0;
            } else if (keyCode == Canvas.RIGHT) {
                if (!bottomMenu) {
                    rowMenu = true;
                    rowMenuIndex++;
                    if (rowMenuIndex > maxCommandInRow) {
                        rowMenuIndex = 1;
                    }
                }
                if (bottomMenu) {

                    bottomMenuIndex++;
                    if (bottomMenuIndex > maxCommandInBottom) {
                        bottomMenuIndex = 1;
                    }
                }

            } else if (keyCode == Canvas.LEFT) {
                if (rowMenu) {
                    rowMenuIndex--;
                    if (rowMenuIndex < 1) {
                        rowMenuIndex = maxCommandInRow;
                    }
                }
                if (!rowMenu) {
                    bottomMenu = true;
                    bottomMenuIndex--;
                    if (bottomMenuIndex < 1) {
                        bottomMenuIndex = maxCommandInBottom;
                    }
                }

            } else if (keyCode == Canvas.FIRE) {

                if(showDetail){
                    if(Profile.getInstance().getUserId()!= null){
                    ((Product) products.elementAt(selectedItem)).setProductCartQuentity(1);

                    if(CartItems.getInstance().getItem((Product) products.elementAt(selectedItem)) != null)
                    {
//                        ((Product) products.elementAt(selectedItem)).setProductCartQuentity(0);//removing item
                    }
                    if(((Product) products.elementAt(selectedItem)).getProductCartQuentity()>0){
                        CartItems.getInstance().addItem((Product) products.elementAt(selectedItem),canvas);
                    }
                    else{
                         CartItems.getInstance().removeItem((Product) products.elementAt(selectedItem),canvas);
                    }
                    }else{
                        LoginScreen loginScreen = new LoginScreen(canvas,(Product) products.elementAt(selectedItem));
                        loginScreen.display();
//                        HelloMIDlet.display.setCurrent();
                    }
                    canvas.repaint();
                    return;
                }
                showDetail = true;
                canvas.repaint();
                 new Thread(new Runnable() {

                        public void run() {
                            try {
                                MyConnection.getInstance().fetchData(Constant.URL_BASE + Constant.URL_PRODUCT_DETAIL+((Product) products.elementAt(selectedItem)).getProductId(), CanvasProductList.this,(byte)1);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }).start();

//                if(1==1)
//                    return;
//                if (rowMenu) {
//                    if (rowMenuIndex == 2 && 1 == 2) {
//                        PlayVideo playVideo = new PlayVideo(canvas, "/test.3gp", (Product) products.elementAt(selectedItem));
//                    } else if (rowMenuIndex == 3 && 1 == 2) {
//                        PlayAudio playAudio = new PlayAudio(canvas, "/test.amr", (Product) products.elementAt(selectedItem));
//                    } else if (rowMenuIndex == 1) {
//                        ChoiceGroupView choiceGroup = new ChoiceGroupView();
//                        choiceGroup.showDropDown(canvas, Constant.QUENTITY_VALUE, "Select Quentity", (Product) products.elementAt(selectedItem));
//                    }
//                } else if (bottomMenu) {
//                    (new ProfileScreen(canvas)).display();
//                } else {
//                    showDetail = true;
//                    canvas.repaint();
//                }
            }
        }
        Constant.printLog("rowMenuIndex:", rowMenuIndex + "");
    }

//	Image getImage(int index)
//	{
//		return images != null && images.length > index ? images[index] : null;
//	}
    int detailY = 0;
    static public boolean detailDown = true;
    protected void paint(Graphics g, int startY) {

        Constant.lineNumber = "1";
        g.setFont(Constant.font_small);
        // paint List background
        g.setColor(Constant.COLOR_bgColor);
        g.fillRect(0, startY, canvas.getWidth(), canvas.getHeight());

        if (showDetail) {
//            canvas.detailView.diaplayDetail(g, canvas, productDetail,detailY);
            return;
        }
        Constant.printLog("scrollTop:", ""+scrollTop);
 g.translate(0, -scrollTop);

//      scrollTop = startY;
        if (products != null && products.size() > 0) {
            drawProductList(g, startY);
        }

        Constant.lineNumber = "2";
        // translate accordingly to current List vertical scroll
       

        g.setFont(Constant.font_small);


        // finally, translate back
        g.translate(0, scrollTop);
        drawBottom(g);
        Constant.lineNumber = "8";
    }

    private void drawProductList(Graphics g, int startY) {
        int top = startY;
        // loop List items
        for (int i = 0; i < itemLines.length; i++) {
            Product p = (Product) products.elementAt(i);
            int itemRows = itemLines[i].length;
            Constant.lineNumber = "3";
            Image imagePart = null;
//            try{
//             imagePart = (Image)ImageList.images.elementAt(i);
//            }catch(Exception e){
//                imagePart = ImageList.product;
//            }
            Constant.lineNumber = "4";
            int itemHeight = 60;//itemRows * font.getHeight() + linePadding * (itemRows - 1);

            itemsTop[i] = top;
            itemsHeight[i] = itemHeight;

            // is image part higher than the text part?
            if (imagePart != null && imagePart.getHeight() > itemHeight) {
                itemHeight = imagePart.getHeight();
            }
            itemHeight += 2 * padding + 2 * borderWidth;

            g.translate(0, top);

            if (borderWidth > 0) {
                // paint item border
                g.setColor(i == selectedItem ? Constant.COLOR_borderSelectedColor : Constant.COLOR_borderColor);
                g.fillRect(margin, margin + topSpace, canvas.getWidth() - 2 * margin, itemHeight);
            }
            Constant.lineNumber = "5";
            // paint item background
            g.setColor(i == selectedItem ? Constant.COLOR_backSelectedColor : Constant.COLOR_backColor);
            g.fillRect(margin + borderWidth, margin + borderWidth + topSpace, canvas.getWidth() - 2 * margin - 2 * borderWidth, itemHeight - 2 * borderWidth);
            g.drawImage(ImageList.more, canvas.getWidth()-30, 23+margin + borderWidth + padding + topSpace, Graphics.TOP | Graphics.LEFT);
           

            // has this item an image part?
            if (imagePart != null) {
                g.drawImage(imagePart, margin + borderWidth + padding, margin + borderWidth + padding + topSpace, Graphics.TOP | Graphics.LEFT);
            }

            // paint item text rows
            g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

            int textLeft = margin + borderWidth + padding + (imagePart != null ? imagePart.getWidth() + padding : 0);
//            g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

            if (maxChar <= 0) {
                maxChar = g.getFont().charWidth('A');
                maxChar = cellWidth / maxChar;
                p.maxChar = maxChar;
            }
            if (startTextYInCell <= 0) {
                startTextYInCell = (cellHeight - g.getFont().getHeight()) / 2;
            }
            Constant.lineNumber = "6";
//            g.setColor(Constant.COLOR_ITEM_BG);
//            g.fillRect(textLeft, margin + borderWidth + padding + topSpace, cellWidth, cellHeight);
//            g.setColor(Constant.COLOR_ITEM_TEXT);
            g.drawString(p.getProductName(), textLeft + 5, startTextYInCell + margin + borderWidth + padding + topSpace, cellHeight);
//            textLeft = textLeft + cellWidth;
//            g.setColor(Constant.COLOR_ITEM_BG);
//            g.fillRect(textLeft + padding, margin + borderWidth + padding + topSpace, cellWidth, cellHeight);
//            g.setColor(Constant.COLOR_ITEM_TEXT);
            g.drawString(p.getProductSku(), textLeft + padding + 5,g.getFont().getHeight()+margin+ startTextYInCell + margin + borderWidth + padding + topSpace, cellHeight);
            g.drawString(p.getProductPrice(), textLeft + padding + 5,(g.getFont().getHeight()*2)+margin+ startTextYInCell + margin + borderWidth + padding + topSpace, cellHeight);

             

//            g.setColor(Constant.COLOR_ITEM_BG);
//            textLeft = margin + borderWidth + padding + (imagePart != null ? imagePart.getWidth() + padding : 0);
//            g.fillRect(textLeft, margin + borderWidth + padding + topSpace + 25, cellWidth, cellHeight);
//            g.setColor(Constant.COLOR_ITEM_TEXT);
//            g.drawString(p.getProductPrice(), textLeft + 5, startTextYInCell + margin + borderWidth + padding + topSpace + cellHeight + 5, cellHeight);
//
//            textLeft = textLeft + cellWidth;
//            g.setColor(Constant.COLOR_ITEM_BG);
//            g.fillRect(textLeft + padding, margin + borderWidth + padding + topSpace + cellHeight + 5, cellWidth, cellHeight);
//            g.setColor(Constant.COLOR_ITEM_TEXT);
//            g.drawString("QTY " + p.getProductCartQuentity() + " >", textLeft + padding + 5, margin + borderWidth + padding + topSpace + 25, 20);
//            if (rowMenu && i == selectedItem) {
//                if (rowMenuIndex == 1) {
//                    g.setColor(Constant.COLOR_borderSelectedColor);
//                    g.drawRect(textLeft + padding, margin + borderWidth + padding + topSpace + cellHeight + 5, cellWidth, cellHeight);
//                }
//            }
//
//            Constant.lineNumber = "7";
//            int nextRow = 0;
//
//            for (int j = 0; j < itemRows; j++) {
//
//                nextRow = 0;
//                g.drawImage(ImageList.voiceRecord, (canvas.getWidth() - ImageList.voiceRecord.getWidth()) - margin, topSpace + margin + borderWidth + padding + j * (linePadding + font.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
//                nextRow = nextRow + ImageList.voiceRecord.getHeight() + ImageList.voiceRecord.getHeight();
//                g.drawImage(ImageList.voiceRecord, (canvas.getWidth() - ImageList.voiceRecord.getWidth()) - margin, topSpace + margin + borderWidth + padding + j * (linePadding + font.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
//                if (rowMenu && i == selectedItem) {
//                    if (rowMenuIndex == 2) {
//                        nextRow = 0;
//                        g.setColor(Constant.COLOR_borderSelectedColor);
//                        g.drawRect((canvas.getWidth() - ImageList.voiceRecord.getWidth()) - margin, topSpace + margin + borderWidth + padding + j * (linePadding + font.getHeight()) + nextRow, ImageList.voiceRecord.getWidth(), ImageList.voiceRecord.getHeight());
//                    }
//                    if (rowMenuIndex == 3) {
//                        nextRow = ImageList.voiceRecord.getHeight() + ImageList.voiceRecord.getHeight();
//                        g.setColor(Constant.COLOR_borderSelectedColor);
//                        g.drawRect((canvas.getWidth() - ImageList.voiceRecord.getWidth()) - margin, topSpace + margin + borderWidth + padding + j * (linePadding + font.getHeight()) + nextRow, ImageList.voiceRecord.getWidth(), ImageList.voiceRecord.getHeight());
//                    }
//
//                }
//            }
//            Constant.lineNumber = "8";
            g.translate(0, -top);

            top += itemHeight + 2 * margin;
        }
    }
    private void drawProductListXXX(Graphics g, int startY) {
        int top = startY;
        // loop List items
        for (int i = 0; i < itemLines.length; i++) {
            Product p = (Product) products.elementAt(i);
            int itemRows = itemLines[i].length;
            Constant.lineNumber = "3";
            Image imagePart = null;
//            try{
//             imagePart = (Image)ImageList.images.elementAt(i);
//            }catch(Exception e){
//                imagePart = ImageList.product;
//            }
            Constant.lineNumber = "4";
            int itemHeight = 45;//itemRows * font.getHeight() + linePadding * (itemRows - 1);

            itemsTop[i] = top;
            itemsHeight[i] = itemHeight;

            // is image part higher than the text part?
            if (imagePart != null && imagePart.getHeight() > itemHeight) {
                itemHeight = imagePart.getHeight();
            }
            itemHeight += 2 * padding + 2 * borderWidth;

            g.translate(0, top);

            if (borderWidth > 0) {
                // paint item border
                g.setColor(i == selectedItem ? Constant.COLOR_borderSelectedColor : Constant.COLOR_borderColor);
                g.fillRect(margin, margin + topSpace, canvas.getWidth() - 2 * margin, itemHeight);
            }
            Constant.lineNumber = "5";
            // paint item background
            g.setColor(i == selectedItem ? Constant.COLOR_backSelectedColor : Constant.COLOR_backColor);
            g.fillRect(margin + borderWidth, margin + borderWidth + topSpace, canvas.getWidth() - 2 * margin - 2 * borderWidth, itemHeight - 2 * borderWidth);

            // has this item an image part?
            if (imagePart != null) {
                g.drawImage(imagePart, margin + borderWidth + padding, margin + borderWidth + padding + topSpace, Graphics.TOP | Graphics.LEFT);
            }

            // paint item text rows
            g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

            int textLeft = margin + borderWidth + padding + (imagePart != null ? imagePart.getWidth() + padding : 0);
//            g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

            if (maxChar <= 0) {
                maxChar = g.getFont().charWidth('A');
                maxChar = cellWidth / maxChar;
                p.maxChar = maxChar;
            }
            if (startTextYInCell <= 0) {
                startTextYInCell = (cellHeight - g.getFont().getHeight()) / 2;
            }
            Constant.lineNumber = "6";
            g.setColor(Constant.COLOR_ITEM_BG);
            g.fillRect(textLeft, margin + borderWidth + padding + topSpace, cellWidth, cellHeight);
            g.setColor(Constant.COLOR_ITEM_TEXT);
            g.drawString(p.getProductName(), textLeft + 5, startTextYInCell + margin + borderWidth + padding + topSpace, cellHeight);
            textLeft = textLeft + cellWidth;
            g.setColor(Constant.COLOR_ITEM_BG);
            g.fillRect(textLeft + padding, margin + borderWidth + padding + topSpace, cellWidth, cellHeight);
            g.setColor(Constant.COLOR_ITEM_TEXT);
            g.drawString(p.getProductSku(), textLeft + padding + 5, startTextYInCell + margin + borderWidth + padding + topSpace, cellHeight);

            g.setColor(Constant.COLOR_ITEM_BG);
            textLeft = margin + borderWidth + padding + (imagePart != null ? imagePart.getWidth() + padding : 0);
            g.fillRect(textLeft, margin + borderWidth + padding + topSpace + 25, cellWidth, cellHeight);
            g.setColor(Constant.COLOR_ITEM_TEXT);
            g.drawString(p.getProductPrice(), textLeft + 5, startTextYInCell + margin + borderWidth + padding + topSpace + cellHeight + 5, cellHeight);

            textLeft = textLeft + cellWidth;
            g.setColor(Constant.COLOR_ITEM_BG);
            g.fillRect(textLeft + padding, margin + borderWidth + padding + topSpace + cellHeight + 5, cellWidth, cellHeight);
            g.setColor(Constant.COLOR_ITEM_TEXT);
            g.drawString("QTY " + p.getProductCartQuentity() + " >", textLeft + padding + 5, margin + borderWidth + padding + topSpace + 25, 20);
            if (rowMenu && i == selectedItem) {
                if (rowMenuIndex == 1) {
                    g.setColor(Constant.COLOR_borderSelectedColor);
                    g.drawRect(textLeft + padding, margin + borderWidth + padding + topSpace + cellHeight + 5, cellWidth, cellHeight);
                }
            }

            Constant.lineNumber = "7";
            int nextRow = 0;

            for (int j = 0; j < itemRows; j++) {

                nextRow = 0;
                g.drawImage(ImageList.voiceRecord, (canvas.getWidth() - ImageList.voiceRecord.getWidth()) - margin, topSpace + margin + borderWidth + padding + j * (linePadding + Constant.font_small.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
                nextRow = nextRow + ImageList.voiceRecord.getHeight() + ImageList.voiceRecord.getHeight();
                g.drawImage(ImageList.voiceRecord, (canvas.getWidth() - ImageList.voiceRecord.getWidth()) - margin, topSpace + margin + borderWidth + padding + j * (linePadding + Constant.font_small.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
                if (rowMenu && i == selectedItem) {
                    if (rowMenuIndex == 2) {
                        nextRow = 0;
                        g.setColor(Constant.COLOR_borderSelectedColor);
                        g.drawRect((canvas.getWidth() - ImageList.voiceRecord.getWidth()) - margin, topSpace + margin + borderWidth + padding + j * (linePadding + Constant.font_small.getHeight()) + nextRow, ImageList.voiceRecord.getWidth(), ImageList.voiceRecord.getHeight());
                    }
                    if (rowMenuIndex == 3) {
                        nextRow = ImageList.voiceRecord.getHeight() + ImageList.voiceRecord.getHeight();
                        g.setColor(Constant.COLOR_borderSelectedColor);
                        g.drawRect((canvas.getWidth() - ImageList.voiceRecord.getWidth()) - margin, topSpace + margin + borderWidth + padding + j * (linePadding + Constant.font_small.getHeight()) + nextRow, ImageList.voiceRecord.getWidth(), ImageList.voiceRecord.getHeight());
                    }

                }
            }
            Constant.lineNumber = "8";
            g.translate(0, -top);

            top += itemHeight + 2 * margin;
        }
    }

    public static String[] getTextRows(String text, Font font, int width) {
        char SPACE_CHAR = ' ';
        String VOID_STRING = "";

        int prevIndex = 0;
        int currIndex = text.indexOf(SPACE_CHAR);

        Vector rowsVector = new Vector();

        StringBuffer stringBuffer = new StringBuffer();

        String currentToken;

        String currentRowText = VOID_STRING;

        while (prevIndex != -1) {
            int startCharIndex = prevIndex == 0 ? prevIndex : prevIndex + 1;

            if (currIndex != -1) {
                currentToken = text.substring(startCharIndex, currIndex);
            } else {
                currentToken = text.substring(startCharIndex);
            }

            prevIndex = currIndex;

            currIndex = text.indexOf(SPACE_CHAR, prevIndex + 1);

            if (currentToken.length() == 0) {
                continue;
            }

            if (stringBuffer.length() > 0) {
                stringBuffer.append(SPACE_CHAR);
            }

            stringBuffer.append(currentToken);

            if (font.stringWidth(stringBuffer.toString()) > width) {
                if (currentRowText.length() > 0) {
                    rowsVector.addElement(currentRowText);
                }
                stringBuffer.setLength(0);

                currentRowText = VOID_STRING;

                stringBuffer.append(currentToken);

                currentRowText = stringBuffer.toString();
            } else {
                currentRowText = stringBuffer.toString();
            }
        }
        if (currentRowText.length() > 0) {
            rowsVector.addElement(currentRowText);
        }
        String[] rowsArray = new String[rowsVector.size()];

        rowsVector.copyInto(rowsArray);

        return rowsArray;
    }

    void drawBottom(Graphics g) {
        g.setColor(Constant.COLOR_BOTTOM);
//        if(bottomMenu)
//            g.setColor(Constant.COLOR_TAB_SELECTED);


        g.fillRect(0, canvas.getHeight() - (Constant.HEIGHT_BOTTOM), canvas.getWidth(), Constant.HEIGHT_BOTTOM);
        g.setColor(Constant.COLOR_MENU_TEXT);
         g.drawString("Back", 2, canvas.getHeight() - 2,Graphics.BOTTOM|Graphics.LEFT);
        //   g.drawString("Add to cart", (canvas.getWidth()/2)-(g.getFont().stringWidth("Add to cart")/2), canvas.getHeight() - 2,Graphics.BOTTOM|Graphics.LEFT);
          
        if(CartItems.getInstance().cartItemTable.size()>0){
            g.drawString("Cart["+CartItems.getInstance().cartItemTable.size()+"]", canvas.getWidth()-g.getFont().stringWidth("Cack["+CartItems.getInstance().cartItemTable.size()+"]"), canvas.getHeight() - 2,Graphics.BOTTOM|Graphics.LEFT);
//            g.setColor(Constant.COLOR_borderSelectedColor);
//            g.setColor(0xe4de33);
//            g.fillArc((canvas.getWidth()-10)-(g.getFont().stringWidth("ww")/2), canvas.getHeight()-20, g.getFont().stringWidth(""+CartItems.getInstance().cartItemTable.size())+5, g.getFont().stringWidth(""+CartItems.getInstance().cartItemTable.size())+5,0,360);
//            g.setColor(Constant.COLOR_borderSelectedColor);
//            g.drawString(""+CartItems.getInstance().cartItemTable.size(), 3+(canvas.getWidth()-10)-(ImageList.videoRecord.getWidth()/2), canvas.getHeight()-20, 20);
        }else
             g.drawString("Cart", canvas.getWidth()-g.getFont().stringWidth("Cack"), canvas.getHeight() - 2,Graphics.BOTTOM|Graphics.LEFT);


//        g.fillRect(0, canvas.getHeight() - (ImageList.cartImage.getHeight() + 10), canvas.getWidth(), ImageList.cartImage.getHeight() + 10);
//        g.drawImage(ImageList.home, 5, (canvas.getHeight() - (ImageList.home.getHeight() + 5)), Graphics.TOP | Graphics.LEFT);
//        g.drawImage(ImageList.cartImage, (canvas.getWidth() - ImageList.cartImage.getWidth()) - 5, (canvas.getHeight() - (ImageList.cartImage.getHeight() + 5)), Graphics.TOP | Graphics.LEFT);
//
//        if(CartItems.getInstance().cartItemTable.size()>0){
//            g.setColor(Constant.COLOR_borderSelectedColor);
//            g.setColor(0xe4de33);
//            g.fillArc((canvas.getWidth())-(ImageList.videoRecord.getWidth()/2), canvas.getHeight()-20, g.getFont().stringWidth(""+CartItems.getInstance().cartItemTable.size())+5, g.getFont().stringWidth(""+CartItems.getInstance().cartItemTable.size())+5,0,360);
//            g.setColor(Constant.COLOR_borderSelectedColor);
//            g.drawString(""+CartItems.getInstance().cartItemTable.size(), 3+(canvas.getWidth())-(ImageList.videoRecord.getWidth()/2), canvas.getHeight()-20, 20);
//        }

//        g.drawImage(ImageList.cartImage, (canvas.getWidth()/2)-(ImageList.cartImage.getWidth()+(ImageList.cartImage.getWidth()/2)), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), Graphics.TOP | Graphics.LEFT);
//        g.drawImage(ImageList.cartImage, (canvas.getWidth()/2)-(ImageList.videoRecord.getWidth()/2), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), Graphics.TOP | Graphics.LEFT);
//        g.drawImage(ImageList.cartImage, (canvas.getWidth()/2)+(ImageList.cartImage.getWidth()), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), Graphics.TOP | Graphics.LEFT);
//
//        if(CartItems.getInstance().cartItemTable.size()>0){
//            g.setColor(Constant.COLOR_borderSelectedColor);
//            g.setColor(0xe4de33);
//            g.fillArc((canvas.getWidth()/2)-(ImageList.videoRecord.getWidth()/2), canvas.getHeight()-20, g.getFont().stringWidth(""+CartItems.getInstance().cartItemTable.size())+5, g.getFont().stringWidth(""+CartItems.getInstance().cartItemTable.size())+5,0,360);
//            g.setColor(Constant.COLOR_borderSelectedColor);
//            g.drawString(""+CartItems.getInstance().cartItemTable.size(), 3+(canvas.getWidth()/2)-(ImageList.videoRecord.getWidth()/2), canvas.getHeight()-20, 20);
//        }
//        if(bottomMenu){
//            g.setColor(Constant.COLOR_borderSelectedColor);
//            if(bottomMenuIndex==1)

//            g.drawRect((canvas.getWidth()/2)-(ImageList.cartImage.getWidth()+(ImageList.cartImage.getWidth()/2)), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), ImageList.cartImage.getWidth(), ImageList.cartImage.getHeight());
//            if(bottomMenuIndex==2)
//            g.drawRect((canvas.getWidth()/2)-(ImageList.videoRecord.getWidth()/2), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), ImageList.cartImage.getWidth(), ImageList.cartImage.getHeight());
//            if(bottomMenuIndex==3)
//            g.drawRect((canvas.getWidth()/2)+(ImageList.cartImage.getWidth()), (canvas.getHeight()-(ImageList.cartImage.getHeight()+5)), ImageList.cartImage.getWidth(), ImageList.cartImage.getHeight());
//        }

    }

    int getHeight() {
        return canvas.getHeight() - (ImageList.cartImage.getHeight() * 2);
    }

    public void recycleImage() {
        products.removeAllElements();
        ;
        System.gc();
    }
    Product productDetail;

    public void sendData(String data,byte requestedId) {
        System.out.println("sendData:" + data);
        productDetail = JsonStringToOjbect.getProduct(data);
        System.out.println("productDetail -->" + productDetail.toString());
        startImageLoading();
    }

    public void sendStatus(String Status,byte requestedId) {
        canvas.sendStatus(Status,requestedId);
    }

    public void myConnectionBusy(String Status,byte requestedId) {
        canvas.myConnectionBusy(Status,requestedId);
    }

    public void imageLoaded(String Status,byte requestID) {
        Constant.printLog("imageLoaded", " total image loaded sixe"+ImageList.images.size()) ;
        canvas.repaint();
    }
     Thread imageLoading ;
    private void startImageLoading() {
        Constant.printLog("startImageLoading", productDetail.productImage.toString()) ;
        imageLoading = new Thread(new Runnable() {
            public void run() {
                try {
                    ImageList.images.removeAllElements(); 
                    for (int i = 0; i < productDetail.productImage.size(); i++)
                    {
//                        showLoading();
                        MyConnection.getInstance().downloadAndSaveImage((String)productDetail.productImage.elementAt(i), i, CanvasProductList.this,(byte)1);
                    }                    
                } catch (Exception ex) {
                     
                    ex.printStackTrace();
                }
            }
        });
        imageLoading.start();
    }

    public void sendData(String data) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void sendStatus(String Status) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void myConnectionBusy(String Status) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
