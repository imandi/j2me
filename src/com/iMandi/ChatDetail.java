package com.iMandi;

import com.iMandi.bean.ChatMessage;
import com.iMandi.bean.Product;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class ChatDetail {
    Image image ;
 public void diaplayDetail(Graphics g, Canvas canvas, ChatMessage chat, int startY) {
        try {
            Constant.printLog("chat>", chat.toString());
            int width = canvas.getWidth();
            int height = canvas.getHeight();
            String s[] = CanvasProductList.getTextRows(chat.message, Constant.font_small, canvas.getWidth());

            int y = (Constant.TITLE_HEIGHT + 10)+(startY);

            if(chat.contentType.indexOf("i") != -1){
                    Image image = ImageList.loading;
                if (ImageList.images.size() > 0) {
                    image = (Image) ImageList.images.elementAt(0);
                }
                image = Image.createImage("/aaa.jpeg");
                g.drawImage(image, width / 2, (Constant.TITLE_HEIGHT + 10)+(startY), 17);
                y = (Constant.TITLE_HEIGHT + 10 + image.getHeight())+(startY);
            }
            if(chat.contentType.indexOf("a") != -1){
                image = Image.createImage("/player.png");
                g.drawImage(image, width / 2, (Constant.TITLE_HEIGHT + 10)+(startY), 17);
                y = (Constant.TITLE_HEIGHT + 10 + image.getHeight())+(startY);
            }
            //player.png

            g.setColor(0);

            
            int lastY = y;

//            g.drawString("Sender: " + chat.senderName, 5, lastY, Graphics.TOP | Graphics.LEFT);
//            y = lastY + 20;
//            g.drawString("Date: " + chat.date, 5, lastY=y, Graphics.TOP | Graphics.LEFT);
//            y = lastY + 20;


            for (int j = 0; j < s.length; j++) {
                if(s[j] != null && !s[j].trim().equalsIgnoreCase("null"))
                g.drawString(s[j], 5, lastY = y + (j * Constant.font_small.getHeight()), Graphics.TOP | Graphics.LEFT);
            }
            y = lastY + 20;
            if(y>canvas.getHeight()-Constant.HEIGHT_BOTTOM) {
               ( (ImandiCanvas)canvas).screenDown = true;
            }
            else
                ( (ImandiCanvas)canvas).screenDown = false;

            g.setColor(Constant.COLOR_BOTTOM);


            g.fillRect(0, canvas.getHeight() - (Constant.HEIGHT_BOTTOM), canvas.getWidth(), Constant.HEIGHT_BOTTOM);


            g.setColor(Constant.COLOR_BLACK);

            g.drawString("Back", 2, canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);


        } catch (Exception ex) {
            g.setColor(0xffffff);
            g.drawString("Failed to load image!", 0, 0, 17);
            return;
        }

    }
 public void recycle(){
     image=null ;
 }
}
