package com.iMandi;

import com.iMandi.bean.CartItems;
import com.iMandi.bean.Profile;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.MyConnection;
import com.iMandi.connection.MyConnectionInf;
import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class Splash extends ImandiCanvas{

    public Splash(){
        setFullScreenMode(true);
    }
    protected void paint(Graphics g) {
       super.paint(g);

        g.setColor(0);

        try{
//            iMandiMIDlet.initProfille();
//            Profile.getInstance().setUserId("40");
           

        }catch(Exception e){
            e.printStackTrace();
        }
//        Font mFont = g.getFont() ;
         String s = "Initializing...";//+System.getProperty("microedition.platform");
            int stringWidth = Constant.font_small.stringWidth(s);
            int stringHeight = Constant.font_small.getHeight();
            int x = (getWidth() - stringWidth) / 2;
            int y = getHeight() / 2;

            g.setFont(Constant.font_medium_bold);
//            if(Profile.getInstance().getFirstName() == null || Profile.getInstance().getFirstName().trim().length()<=0)
//                g.drawString("Welcome, Guest ", ((getWidth() - Constant.font_small.stringWidth("Welcome, Guest")) / 2), y-14, Graphics.TOP | Graphics.LEFT);
//            else
//            g.drawString("Welcome, "+Profile.getInstance().getFirstName()+" "+Profile.getInstance().getLastName(), ((getWidth() - Constant.font_small.stringWidth("Welcome, "+Profile.getInstance().getFirstName()+" "+Profile.getInstance().getLastName())) / 2), y-14, Graphics.TOP | Graphics.LEFT);

            g.drawString("IMANDI", ((getWidth() - g.getFont().stringWidth("IMANDI")) / 2), y-6, Graphics.TOP | Graphics.LEFT);

//            g.drawString(s, x, y, Graphics.TOP | Graphics.LEFT);
            
              stringWidth = ImageList.logo.getWidth();
             stringHeight = ImageList.logo.getHeight();
             x = (getWidth() - stringWidth) / 2;
             y = getHeight() / 2;
            g.drawImage(ImageList.logo, x, y-60, Graphics.TOP | Graphics.LEFT);

            g.drawString(Constant.VERSION, (getWidth() / 2) - (g.getFont().stringWidth(Constant.VERSION) / 2), getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
    }

}
