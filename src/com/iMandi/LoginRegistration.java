package com.iMandi;

import com.iMandi.bean.Product;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class LoginRegistration extends ImandiCanvas {

    Image image;
    int type;
    byte totalItem = 1;//2
    byte currentSelected = 1;
    byte buttonMargin = 2;
    FormHelper formHelper = new FormHelper() ;

    public LoginRegistration(String title, int type) {
        this.title = title;
        this.type = type;

        
        CENTER_MENU = "Choose";
    }

    void init(){
        if(image == null)
         try {
            image = Image.createImage("/login.png");
        } catch (Exception e) {
        }
        if(formHelper == null)
        formHelper = new FormHelper() ;
        productDetail = new Product() ;
        productDetail.setProductName("nagenra");
        productDetail.setProductSku("nagenra");
        productDetail.setProductDesc("nagenra");
        productDetail.setProductPrice("1020202");
    }
    Product productDetail;
    ItemDetailView detailView = new ItemDetailView();
    protected void paint(Graphics g) {
        super.paint(g);

//        detailView.diaplayDetail(g, this, productDetail, screenMoveY);
//
//        if(true)
//            return ;
        init();
        int stringWidth = image.getWidth();
        int stringHeight = image.getHeight();
        int x = (getWidth() - stringWidth) / 2;
        int y = getHeight() / 2;
        g.drawImage(image, x, 40, Graphics.TOP | Graphics.LEFT);
        formHelper.drawImage(g,x,40,getWidth(),getHeight(),image) ;

        int bX = 10;
        int bY = getHeight() - 75;
        int bW = getWidth() - 20;
        int bH = 20;
        String buttonTitle = "Login";
        formHelper.drawButton(g,bX,bY,bW,bH,currentSelected,buttonTitle,getWidth(),getHeight(),buttonMargin, 0x000FF,(byte)1);

        buttonTitle = "Guest";
        bY = bY + 25;
//        formHelper.drawButton(g,bX,bY,bW,bH,currentSelected,buttonTitle,getWidth(),getHeight(),buttonMargin, 0x000FF,(byte)2);
        drawBottomMenu();
        

    }

    protected void keyPressed(int keyCode) {
        super.keyPressed(keyCode);
         keyCode = getGameAction(keyCode);
        if (keyCode == UP) {
                    currentSelected--;
                    if(currentSelected<=0)
                        currentSelected = totalItem;
                } //going down
                else if (keyCode == DOWN) {
                    currentSelected++;
                    if(currentSelected>totalItem)
                        currentSelected = 1 ;

                } else if (keyCode == RIGHT) {

                } else if (keyCode == LEFT) {

                } else if (keyCode == FIRE) {
                    if(showAlert){
                        showAlert = false;
                    }
//                    if(!showAlert)
                    {
                        recycleImage();
//                        showAlert = true;
                        if (currentSelected == 1)
//                                alertText = "Login";
                        Utility.openLoginScreen(LoginRegistration.this);
//                            Utility.openLoginAsGuest(LoginRegistration.this);

                        else if (currentSelected == 2)
//                            alertText = "Guest" ;
                            Utility.openLoginAsGuest(LoginRegistration.this);
                    }
                }
        refresh();
    }

    public void recycleImage() {
        super.recycleImage();
        image= null ;
        System.gc();
        CENTER_MENU = null ;
        formHelper = null ;
    }
    


}
