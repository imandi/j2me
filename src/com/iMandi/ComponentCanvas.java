package com.iMandi;

import com.ui.components.TextBox;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author nagendrakumar
 */
public class ComponentCanvas extends Canvas{

    TextBox textBox = new TextBox("hi", "alalaal", 10, 10, 100);
    protected void paint(Graphics g) {
        g.setColor(Constant.COLOR_TAB_SELECTED);
        g.fillRect(0, 0, getWidth(), getHeight());

        textBox.setFocus(true);
        g.setColor(255,255,255);
        textBox.paint(g);
    }

    protected void keyPressed(int keyCode) {
        super.keyPressed(keyCode);
        textBox.keyPressed(keyCode, keyCode);
        repaint();
    }

    protected void keyReleased(int keyCode) {
        super.keyReleased(keyCode);
        textBox.keyReleased(keyCode, keyCode);
        repaint();
    }

    protected void keyRepeated(int keyCode) {
        super.keyRepeated(keyCode);
        textBox.keyRepeated(keyCode, keyCode);
        repaint();
    }




}
