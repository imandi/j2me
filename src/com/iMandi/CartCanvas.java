package com.iMandi;

import com.iMandi.bean.CartItems;
import com.iMandi.bean.Product;
import com.iMandi.bean.Profile;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.MyConnection;
import com.iMandi.connection.MyConnectionInf;
import java.io.IOException;
import java.util.Vector;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class CartCanvas extends ImandiCanvas {

    TabMenu menu = null;
public boolean isLoading = false;
public boolean isEmpty = false;
    public CartCanvas() {
        init();
        setFullScreenMode(true);
    }

    public Object prevDisplay;

    protected void keyPressed(int key) {
        if(isLoading){
            return;
        }
        int gameAction = getGameAction(key);

        if (gameAction == Canvas.RIGHT) {

            repaint();
        } else if (gameAction == Canvas.LEFT) {

            repaint();
        }
        myCanvas.keyPressed(key);

    }
    Vector productList = new Vector();
Thread imageLoading ;
    private void init() {
        Product p = new Product();

        myCanvas = new CanvasProductCartList("Cart canvas", productList= CartItems.getInstance().cartItemTable, this);

        imageLoading = new Thread(new Runnable() {
            public void run() {
                try {
                    showLoading();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                      JSONObject inner = new JSONObject();
                    try {
                        inner.put("customer_id", Profile.getInstance().getUserId());
                    } catch (JSONException ex) {
                        ex.printStackTrace();
//                        Constant.showError(ex.getMessage(),HelloMIDlet.display);
                    }

                    
                    MyConnection.getInstance().fetchDataPost(Constant.URL_BASE + Constant.URL_GET_CART_DATA, CartCanvas.this, inner.toString(),(byte)1);
                } catch (IOException ex) {
//                    Constant.showError(ex.getMessage());
                    ex.printStackTrace();
                }
            }
        });
        imageLoading.start();

//        myCanvas = new CanvasProductCartList("Cart canvas", productList, this);
    }
    CanvasProductCartList myCanvas;

    protected void paint(Graphics g) {
        super.paint(g);
        g.setFont(Constant.font_small);
//        if(productList!=null  && productList.size()>0){
            myCanvas.paint(g,Constant.TITLE_HEIGHT+titelStartY);
//        }
            if(CartItems.getInstance().cartItemTable!=null  && CartItems.getInstance().cartItemTable.size()<=0){            
//                g.setColor(Constant.COLOR_bgColor);
//        g.fillRect(0, 0, getWidth(), getHeight());

                emptyCart(g);
            }
            if (isLoading || (imageLoading!=null && imageLoading.isAlive())) {
//                g.setColor(Constant.COLOR_bgColor);
//        g.fillRect(0, 0, getWidth(), getHeight());

                Constant.loading(g, this,Constant.LOADING_CART);

        }
        drawTitle(title=Constant.TITLE_CART_DETAIL);
        if(CartItems.getInstance().cartItemTable!=null  && CartItems.getInstance().cartItemTable.size()>0)
        drawListTitle(g);
    }

    private void emptyCart(Graphics g){
        g.setColor(0x0);
        Font f = g.getFont();
        g.setFont(Constant.font_small);
         String s = "Empty cart :(";//+System.getProperty("microedition.platform");
            int stringWidth = g.getFont().stringWidth(s);
            int stringHeight = g.getFont().getHeight();
            int x = (getWidth() - stringWidth) / 2;
            int y = getHeight() / 2;
            g.drawString(s, x, y, Graphics.TOP | Graphics.LEFT);
            g.setFont(f);
    }
    private void drawTitle(String title, Graphics g) {
        g.setColor(Constant.COLOR_HEADER);
//        g.fillRect(0, 0, getWidth(), Constant.HEIGHT_BOTTOM);
//
//        g.setColor(255, 255, 255);
//
//        g.drawString(title, 0, 0, Graphics.LEFT | Graphics.TOP);
//        g.drawImage(ImageList.logo_20x20, 0, 0, Graphics.TOP | Graphics.LEFT);
//         g.setColor(0);
        g.fillRect(0, 0, getWidth(), Constant.TITLE_HEIGHT);

        g.setColor(255, 255, 255);
Font o = g.getFont();
g.setFont(Constant.font_small);
        String s = Constant.CATEGORY_TITLE;
        int stringWidth = g.getFont().stringWidth(s);
        int stringHeight = g.getFont().getHeight();
        int x = (getWidth() - stringWidth) / 2;
        int y = getHeight() / 2;

//        g.setFont(mFont);
        g.drawString(s, x, 0, Graphics.TOP | Graphics.LEFT);
        g.drawImage(ImageList.logo_20x20, 0, 0, Graphics.TOP | Graphics.LEFT);
g.setFont(o);
    }
    public int titelStartY = Constant.TITLE_HEIGHT ;
    public int titelHeight = Constant.TITLE_HEIGHT ;
    private void drawListTitle(Graphics g) {

        g.setColor(Constant.COLOR_LIST_CART_HEADING);
        g.fillRect(0, titelStartY, getWidth(), 20);

        g.setColor(255,255,255);

        Font f = g.getFont() ;
        g.setFont(Constant.font_medium);
        g.drawString("Cart: "+CartItems.getInstance().cartItemTable.size()+" item in cart", 5, titelStartY+((titelHeight-g.getFont().getHeight())/2), Graphics.LEFT | Graphics.TOP);
//        f.stringWidth(null)
//        g.drawString(Constant.LIST_TITLE_PRODUCT_NAME, 5, titelStartY+((titelHeight-f.getHeight())/2), Graphics.LEFT | Graphics.TOP);
//        g.drawString(Constant.LIST_TITLE_PRODUCT_PRICE, ((getWidth()-g.getFont().stringWidth(Constant.LIST_TITLE_PRODUCT_PRICE))), titelStartY+((titelHeight-f.getHeight())/2), Graphics.LEFT | Graphics.TOP);

        g.setFont(f);
    }

   public void sendData(String data,byte requestId) {
//       Constant.showError(data);
        System.out.println("getCartProductList>sendData:" + data);
        productList = JsonStringToOjbect.getCartProductList(data);
        System.out.println("getCartProductList -->" + productList.toString());
        myCanvas = new CanvasProductCartList("Test canvas", productList, this);
        CartItems.getInstance().cartItemTable = productList;
        if( CartItems.getInstance().cartItemTable!=null  && CartItems.getInstance().cartItemTable.size()<=0)
            isEmpty = true ;
        repaint();
        //startImageLoading();
    }

    public void sendStatus(String Status,byte requestId) {
//        Constant.showError("Status:"+Status);
        System.out.println("sendStatus:" + Status);
        if (Status.equals(MyConnection.STATUS_CONNECTING)) {
            showLoading();
        } else if (Status.equals(MyConnection.STATUS_IDEL)) {
            hideLoading();
        }
    }

    public void myConnectionBusy(String Status,byte requestId) {
       System.out.println("myConnectionBusy:" + Status);
    }

    public void imageLoaded(String Status,byte requestId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    public void showLoading() {
        isLoading = true;
        repaint();
    }

    public void hideLoading() {
        isLoading = false;
        repaint();
    }

}
