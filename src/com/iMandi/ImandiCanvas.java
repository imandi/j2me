package com.iMandi;

import com.ui.components.Component;
import com.ui.components.ComponentContainer;
import com.iMandi.connection.MyConnection;
import com.iMandi.connection.MyConnectionInf;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class ImandiCanvas extends ComponentContainer   implements MyConnectionInf{

    public boolean screenDown = false;
    int screenMoveY = 0;
    public String title = "iMandi" ;
    public int type = 1 ;
    boolean isLoading = false;
    boolean showMenu = false;
    boolean hasMenu = false;
    boolean showAlert = false;
    Font prevFont ;
    Graphics g;
    String alertText ;
    boolean isKeyEventConsume = false;
//    public Object prevScreen ;

    public String LEFT_MENU ;
    public String RIGHT_MENU ;
    public String CENTER_MENU ;
    public ImandiCanvas(){
        setFullScreenMode(true);
        System.out.print(System.getProperty("phone.imei"));
    }
    protected void paint(Graphics g) {
        g.setFont(Component.getDefaultFont());
        this.g = g ;
        drawBG();
        drawTitle(title);
       
//        paintLoading();

    }

    public void paintLoading(){
         if(isLoading)
        {
           Constant.loading(g, this,Constant.LOADING);
        }
    }

    public void showAlert(){
        if(!showAlert)
        return;
        
        String s[] = CanvasProductList.getTextRows(alertText, Constant.font_small, getWidth()-85);
        g.setColor(Constant.COLOR_ALERT_BG);
        g.fillRect(20, (getHeight()/2)-50, getWidth()-40, 100);
        g.setColor(Constant.COLOR_ALERT_HEADER);
        g.fillRect(20, (getHeight()/2)-30, getWidth()-40, 100);

        g.setColor(0xFFFFFF);
        int y = (getHeight()/2)-30 ;
        for (int j = 0; j < s.length; j++) {
                if(s[j] != null && !s[j].trim().equalsIgnoreCase("null"))
                g.drawString(s[j], 25,  y + (j * Constant.font_small.getHeight()), Graphics.TOP | Graphics.LEFT);
        }
    }

    public void drawBottomMenu(){

        g.setColor(Constant.COLOR_BOTTOM);
        g.fillRect(0, getHeight()-Constant.HEIGHT_BOTTOM, getWidth(), Constant.HEIGHT_BOTTOM);
        getPrevFont();
        g.setFont(Constant.font_medium) ;

         g.setColor(Constant.COLOR_BLACK);
         if(LEFT_MENU!=null)
         g.drawString(LEFT_MENU, 2, getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
         if(CENTER_MENU!=null)
         g.drawString(CENTER_MENU, (getWidth() / 2) - (g.getFont().stringWidth(CENTER_MENU) / 2), getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
         if(RIGHT_MENU!=null)
         g.drawString(RIGHT_MENU , getWidth() - g.getFont().stringWidth(RIGHT_MENU), getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
         setPrevFont();  
    }
    public void showMenu(){
    }

    public void drawBG(){
        g.setColor(Constant.COLOR_bgColor);
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    public void sendData(String data) {
       throw new UnsupportedOperationException("Not supported yet.");
    }

    public void sendStatus(String Status) {
       throw new UnsupportedOperationException("Not supported yet.");
    }
    public void sendData(String data, byte requestID) {
        showLoading();
        Constant.printLog("sendData::", data+"requestID "+requestID);
    }

    public void sendStatus(String Status, byte requestID) {
         Constant.printLog("sendStatus::", Status+"requestID "+requestID);
       if (Status.equals(MyConnection.STATUS_CONNECTING)) {
            showLoading();
        } else if (Status.equals(MyConnection.STATUS_IDEL)) {
            hideLoading();
        }
    }

    public void myConnectionBusy(String Status, byte requestID) {
        Constant.printLog("myConnectionBusy::", Status+"requestID "+requestID);
    }

    public void myConnectionBusy(String Status) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void imageLoaded(String Status, byte requestID) {
//        throw new UnsupportedOperationException("Not supported yet.");
        hideLoading();
        refresh();
    }
     public void showLoading() {
        isLoading = true;
        repaint();
    }

    public void hideLoading() {
        isLoading = false;
        repaint();
    }
     public void drawTitle(String title) {
        g.setColor(Constant.COLOR_HEADER);
        g.fillRect(0, 0, getWidth(), Constant.TITLE_HEIGHT);

        g.setColor(Constant.COLOR_HEADER_FONT);
        getPrevFont();
        g.setFont(Constant.font_medium_bold);
        int stringWidth = g.getFont().stringWidth(title);
        int stringHeight = g.getFont().getHeight();
        int x = (getWidth() - stringWidth) / 2;
        int y = getHeight() / 2;
//        g.drawString(title+" : "+(Constant.initFreeMemory/1024)+" : "+(Constant.currentFreeMemory/1024)+" : "+(Constant.initFreeMemory-Constant.currentFreeMemory)/1024, 5, 0, Graphics.TOP | Graphics.LEFT);
        g.drawString(title, x, 0, Graphics.TOP | Graphics.LEFT);
//        g.drawImage(ImageList.logo_20x20, 0, 0, Graphics.TOP | Graphics.LEFT);
        setPrevFont();
    }

    public void getPrevFont(){
        prevFont = g.getFont();
    }
    public void setPrevFont(){
         g.setFont(prevFont);
    }
    public void recycleImage(){
        ImageList.recycleImage();
    }

    protected void keyPressed(int keyCode) {
        super.keyPressed(keyCode);
//        if(isKeyEventConsume){
//            isKeyEventConsume = false;
//            return;
//        }
//        Constant.printMemory();
        if(isLoading){
            return;
        }
        if(showMenu && !showAlert){
            return;
        }

        if (!Constant.isKnownKey(this, keyCode)) {
            if (keyCode == Constant.KEY_SOFT_LEFT) {
//                if(hasMenu  && !showAlert){
//                    showMenu = true;
//                    popupMenu.setDirty(true);
//                    refresh();
//                }else if (LEFT_MENU != null){
//                if(prevScreen instanceof GridCanvas){
//                    ((GridCanvas)prevScreen).init();
//                }
//                    iMandiMIDlet.display.setCurrent((Displayable)prevScreen);
                Utility.displayPrevScreen();
                    
//                }
            } else //if (keyCode == Constant.KEY_SOFT_LEF)
            {
            }
        }
        keyCode = getGameAction(keyCode);
        if (keyCode == Canvas.UP) {
               
                } //going down
                else if (keyCode == Canvas.DOWN) {

                } else if (keyCode == Canvas.RIGHT) {

                } else if (keyCode == Canvas.LEFT) {

                } else if (keyCode == Canvas.FIRE) {
                    if(showAlert)
                        showAlert = false;
                }
        refresh();
    }

    public void menuItemSelected(String menuItemLabel) {
        showMenu = false ;
//        popupMenu.hideMenu();

    }

    

}
