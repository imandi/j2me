package com.iMandi.bean;

import com.iMandi.Constant;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.RMS;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.RMS;
import org.json.me.JSONObject;

/**
 *
 * @author nagendrakumar
 */
public class Profile extends CitrusErrors{

    static Profile profile;

    public static Profile getInstance(){
        if(profile==null)
            profile = new Profile();
        return profile;
    }
    private String userId ;//= "40" ;
    private String username ;//= "40" ;
    private String passwordEncrepted  ;//= "40" ;

    private String emailId ;//="@gmail.com";
    private String name ="";
    private String firstName ="";
    private String lastName ="";
    private String profilepic ;
    private String session ;
    private String password ;

    private String aadharCardNumber ;
    private String gender ;
    private String dob ;
    private String countryCode ="91";




    public String address ="";
    public String pinCode ="";
    public String district ="";
    public String state ="";
    public String mobileNumber ="";
    public String cooperativeNo ="";
    public String localityTown ="";

public String getCountryCode() {
		return (countryCode== null)?"91":countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

            public String getAadharCardNumber() {
		return (aadharCardNumber== null)?"":aadharCardNumber;
	}
	public void setAadharCardNumber(String aadharCardNumber) {
		this.aadharCardNumber = aadharCardNumber;
	}
         public String getDob() {
		return (dob== null)?"":dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
         public String getGender() {
                if(gender== null)
                    gender="Male";
                else if(gender.equalsIgnoreCase("Male"))
                    gender="Male";
                else if(gender.equalsIgnoreCase("Female"))
                    gender="Female";
		return (gender== null)?"":gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

    public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmailId() {
		return (emailId== null)?"":emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getName() {
		return (name== null)?"":name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return (address== null)?"":address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPinCode() {
            	return (pinCode== null)?"":pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	
	public String getMobileNumber() {
		return (mobileNumber== null)?"":mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getCooperativeNo() {
		return cooperativeNo;
	}
	public void setCooperativeNo(String cooperativeNo) {
		this.cooperativeNo = cooperativeNo;
	}
	public String getLocalityTown() {
		return localityTown;
	}
	public void setLocalityTown(String localityTown) {
		this.localityTown = localityTown;
	}
	public static Profile getProfile() {
		return profile;
	}
	public static void setProfile(Profile profile) {
		Profile.profile = profile;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getProfilepic() {
		return profilepic;
	}
	public void setProfilepic(String profilepic) {
		this.profilepic = profilepic;
	}
	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
        public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
        public String getPasswordEncrepted() {
		return passwordEncrepted;
	}
	public void setPasswordEncrepted(String passwordEncrepted) {
		this.passwordEncrepted = passwordEncrepted;
	}
	public String toString() {
		return "Profile [userId=" + userId + ", username=" + username + ", password=" + password + ", status" + status
				+ ", message=" + message + ", messageSub=" + messageSub + ", session=" + session + ", address="
				+ address + ", pinCode=" + pinCode + ", district=" + district + ", state=" + state + ", mobileNumber="
				+ mobileNumber + ", cooperativeNo=" + cooperativeNo + ", localityTown=" + localityTown + "]";
	}
	
	

        public void saveMe(){
            try{
                    JSONObject inner = new JSONObject();
                    inner.put("username", Profile.getInstance().getUsername());
                    inner.put("userId", Profile.getInstance().getUserId());
                    inner.put("mobileNumber", Profile.getInstance().getMobileNumber());
                    inner.put("password", Profile.getInstance().getPassword());

                    inner.put("name", Profile.getInstance().getName());
                    inner.put("gender", Profile.getInstance().getGender());
                    inner.put("dob", Profile.getInstance().getDob());
                    inner.put("aadharCardNumber", Profile.getInstance().getAadharCardNumber());
                    inner.put("address", Profile.getInstance().getAddress());

                    RMS.getInstance().setRecord(inner.toString(), RMS.PROFILE);
                    Constant.printLog("saveMe", inner.toString());
                }catch(Exception e){
e.printStackTrace();
                }
        }
        public void restoreMe(){
            try{
                Profile profile=null;
                    if(Constant.CHEAT_CODE){
                        String temp = "{\"name\":\"Baba Baba11\",\"aadharCardNumber\":\"1919199191\",\"username\":\"91983tx74e4d621\",\"userId\":\"167\",\"gender\":\"Male\",\"address\":\"Mumbai\",\"dob\":\"05-Jun-1980\",\"password\":\"oFTpVzFIs3\",\"mobileNumber\":\"91-9818662101\"}";
                        profile = JsonStringToOjbect.getProfile(temp);
                    }else{
                        profile = JsonStringToOjbect.getProfile(RMS.getInstance().getRecord(RMS.PROFILE));
                    }
                    Constant.printLog("restoreMe", profile.toString());
                }catch(Exception e){
e.printStackTrace();
                }
        }



}
