package com.iMandi.bean;

/**
 *
 * @author nagendrakumar
 */
public class Bank {
 private String bankid;
    private String bankname;

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String categoryid) {
        this.bankid = bankid;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String toString() {
		return "bankname=" + bankname+" bankid:"+bankid ;
	}
}
