package com.iMandi.bean;

/**
 *
 * @author nagendrakumar
 */
public class ServerMessage {
    public String message ;
    public int status ;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String toString() {
		return "ServerMessage [message=" + message + ", status=" + status + "]";
	}
    
    
}
