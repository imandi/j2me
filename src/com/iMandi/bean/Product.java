package com.iMandi.bean;


import com.iMandi.Constant;
import java.util.Vector;

/**
 *
 * @author nagendrakumar
 */
public class Product {

     private String productId;
    private String productSku;
    private String productName;
    private String productPrice;
    private String productDesc;
    private String productShortDesc;
    public String productImagepath;
    private String productDetailUrl;
    private String productStockStatus;
    private String productCreatedAt;
    private String productCategory;
    private Integer totalPage;
    public static int maxChar;
    public Vector productImage = new Vector();
    public Vector prodOption = new Vector();



    public String getProductImage() {
        return productId;
    }

    public void setProductImage(String image) {
        this.productImage.addElement(image);
    }
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductSku() {
//         if(productSku != null && productSku.length()>maxChar)
//            return productSku .substring(0, maxChar)+"..";
        return productSku;
    }

    public void setProductSku(String productSku) {
        this.productSku = productSku;
    }

    public String getProductName() {
//       .substring(0, charWidth)
//        if(productName != null && productName.length()>maxChar)
//            return productName .substring(0, maxChar)+"..";
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductShortDesc() {
        return productShortDesc;
    }

    public void setProductShortDesc(String productShortDesc) {
        this.productShortDesc = productShortDesc;
    }

    public String getProductImagepath() {
        return productImagepath;
    }

    public void setProductImagepath(String productImagepath) {
        this.productImagepath = productImagepath;
    }

    public String getProductDetailUrl() {
        return productDetailUrl;
    }

    public void setProductDetailUrl(String productDetailUrl) {
        this.productDetailUrl = productDetailUrl;
    }

    public String getProductStockStatus() {
        return productStockStatus;
    }

    public void setProductStockStatus(String productStockStatus) {
        this.productStockStatus = productStockStatus;
    }

    public String getProductCreatedAt() {
        return productCreatedAt;
    }

    public void setProductCreatedAt(String productCreatedAt) {
        this.productCreatedAt = productCreatedAt;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }
    public String productCartPrice ;
    public int productCartQuentity = 0;
    public float productBasePrice = 0 ;

    public float  getProductBasePrice() {
        Constant.printLog("", getProductPrice());
	return Float.parseFloat(getProductPrice().substring(3));
    }
    public String getProductCartPrice() {
            return getProductBasePrice()*getProductCartQuentity()+" Rs" ;
    }
//	public void getProductCartPrice(String productPrice) {
//		this.productPrice = productPrice;
//
//	}
	public int getProductCartQuentity() {
		return productCartQuentity;
	}
	public void setProductCartQuentity(int productCartQuentity) {
		this.productCartQuentity = productCartQuentity;
	}

//     public String productName ;
//    public String productWeight ;
    
//    public float productBasePrice ;
    
//    public String productImage ;
//    public String productId ;
//	public String getProductName() {
//		return productName;
//	}
//	public void setProductName(String productName) {
//		this.productName = productName;
//	}
//        public float  getProductBasePrice() {
//		return productBasePrice;
//	}
//	public void setProductBasePrice(float  productName) {
//		this.productBasePrice = productName;
//	}
//	public String getProductWeight() {
//		return productWeight;
//	}
//	public void setProductWeight(String productWeight) {
//		this.productWeight = productWeight;
//	}
	
//	public String getProductImage() {
//		return productImage;
//	}
//	public void setProductImage(String productImage) {
//		this.productImage = productImage;
//	}
//	public String getProductId() {
//		return productId;
//	}
//	public void setProductId(String productId) {
//		this.productId = productId;
//	}
//
       
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}
	public String toString() {
		return "Product [productId=" + productId + ", productSku=" + productSku + ", productName=" + productName
				+ ", productPrice=" + productPrice + ", productDesc=" + productDesc + ", productShortDesc="
				+ productShortDesc + ", productImagepath=" + productImagepath + ", productDetailUrl=" + productDetailUrl
				+ ", productStockStatus=" + productStockStatus + ", productCreatedAt=" + productCreatedAt
				+ ", productCategory=" + productCategory + ", totalPage=" + totalPage + ", productImage=" + productImage
				+ ", productCartPrice=" + productCartPrice + ", productCartQuentity=" + productCartQuentity
				+ ", productBasePrice=" + productBasePrice + "]";
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		return true;
	}


}
