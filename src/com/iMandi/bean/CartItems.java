package com.iMandi.bean;

import com.iMandi.Constant;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.MyConnection;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.lcdui.Canvas;
import org.json.me.JSONException;
import org.json.me.JSONObject;

/**
 *
 * @author nagendrakumar
 */
public class CartItems {
    public Vector cartItemTable = new Vector();
    public String total_amount;
    public String pick_location;
    static CartItems cartItems;

    public static CartItems getInstance(){
        if(cartItems==null)
            cartItems = new CartItems();
        return cartItems;
    }

    public void addItem(Object item,final Canvas canvas){
        System.out.println("item going to add :"+item.toString());
        if(item instanceof  Product){
            Product pItem = (Product)item;
//            if(cartItemTable.contains(pItem)){
//                for (int i = 0; i < cartItemTable.size(); i++) {
//                    Product lItem = (Product)cartItemTable.elementAt(i);
//                   if(item.equals(lItem)){
////                       lItem.setProductQuentity(lItem.getProductQuentity()+pItem.getProductQuentity());
//                       lItem.setProductQuentity(lItem.getProductQuentity()+pItem.getProductQuentity());
//                   }
//                }
//            }else{
//            if(cartItemTable.contains(pItem))
//            {
//                cartItemTable.removeElement(pItem);
//            }
//                cartItemTable.addElement(pItem);
                 final JSONObject inner = new JSONObject();
            try {
                inner.put("product_id", pItem.getProductId());
                 inner.put("product_qty", "1");//+pItem.getProductCartQuentity());
                 inner.put("customer_id", Profile.getInstance().getUserId());
                 inner.put("pack_size", ((ProductOption)pItem.prodOption.elementAt(0)).size);
//{"product_id":"355","customer_id":"7","product_qty":"2","pack_size":"10 kg."}
                 new Thread(new Runnable() {

                    public void run() {
                         addToCartSendToServer(inner.toString(),canvas) ;
                    }
                }).start();
                   
                     } catch (JSONException ex) {
                ex.printStackTrace();
            }
           
//            }
        }
        System.out.println("Cart after add Item"+cartItemTable.toString());
    }

   /*Post Response::{"message":"Product Added to Cart Successfully.","status":1,
    "cart_data":[{"product_id":"439","product_name":"Hybrid Bajra-RHB-177","product_sku"
    :"MANDI-444","product_qty":"1","product_price":"135.0000"},{"product_id":"377",
    "product_name":"NPK(102:32:26)","product_sku":"MANDI-379","product_qty":"15",
    "product_price":"1035.0000"},{"product_id":"398","product_name":"Urea Phosphate (17:44:0)",
    "product_sku":"MANDI-402","product_qty":"1","product_price":"1847.0000"},{"product_id":"397",
    "product_name":"Urea Phosphate (17:44:0)","product_sku":"MANDI-401","product_qty":"1","product_price":
    "79.0000"},{"product_id":"396","product_name":"Urea Phosphate (17:44:0)","product_sku":"MANDI-400",
    "product_qty":"1","product_price":"65.0000"},{"product_id":"395","product_name"
    :"Urea Phosphate With SOP (18:18:18)","product_sku":"MANDI-399","product_qty":"2","product_price":"1998.0000"}
    ,{"product_id":"312","product_name":"Sagarika","product_sku":"MANDI-313","product_qty":"6","product_price":"500.0000"}
    ,{"product_id":"409","product_name":"Yamato","product_sku":"MANDI-413","product_qty":"1","product_price":"140.0000"},
    {"product_id":"408","product_name":"Yamato","product_sku":"MANDI-412","product_qty":"1","product_price":"290.0000"}
    ,{"product_id":"407","product_name":"Kaguya","product_sku":"MANDI-411","product_qty":"1","product_price":"290.0000"},
    {"product_id":"406","product_name":"Kaguya","product_sku":"MANDI-410","product_qty":"2","product_price":"150.0000"}
    ,{"product_id":"404","product_name":"Azotobacter","product_sku":"MANDI-408","product_qty":"2","product_price":"90.0000"}],"total_amount":25847}
    */

    ServerMessage addToCartSendToServer(String jsonData,Canvas canvas) {
        ServerMessage serverMessage = new ServerMessage() ;
        try {
            String response = MyConnection.getInstance().postData(Constant.URL_BASE + Constant.URL_ADD_TO_CART, jsonData);
            Constant.printLog(Constant.URL_BASE + Constant.URL_ADD_TO_CART, response);
            try {
                JSONObject json = new JSONObject(response);
                JSONObject data = new JSONObject(json.getString("data"));
                if (json.has("code")) {
                        serverMessage.setStatus(json.getInt("code"));
                }
                if (json.has("message")) {
                        serverMessage.setMessage(json.getString("message"));
                }                
                Constant.printLog("serverMessage> ", serverMessage.toString());

                try{
                    int cart_item_count = data.getInt("cart_item_count");
                    CartItems.getInstance().cartItemTable = new Vector();
                    for (int i = 0; i < cart_item_count; i++) {
                       if(CartItems.getInstance().cartItemTable !=null)
                        CartItems.getInstance().cartItemTable.addElement(new String());

                    }
                }catch(Exception e){

                }
                
//                CartItems.getInstance().cartItemTable = JsonStringToOjbect.getCartProductList(response);
                if(canvas !=null)
                    canvas.repaint();

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            return serverMessage ;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null ;
    }
    ServerMessage removeToCartSendToServer(String jsonData,Canvas canvas) {
        ServerMessage serverMessage = new ServerMessage() ;
        try {
            String response = MyConnection.getInstance().postData(Constant.URL_BASE + Constant.URL_REMOVE_TO_CART, jsonData);
            Constant.printLog(Constant.URL_BASE + Constant.URL_REMOVE_TO_CART, response);
            try {
                JSONObject json = new JSONObject(response);
                if (json.has("status")) {
                        serverMessage.setStatus(json.getInt("status"));
                }
                if (json.has("message")) {
                        serverMessage.setMessage(json.getString("message"));
                }
                Constant.printLog("serverMessage> ", serverMessage.toString());
                  CartItems.getInstance().cartItemTable.removeAllElements();;
                  if(canvas!=null)
                  canvas.repaint(); ;
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            return serverMessage ;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null ;
    }

//    public void removeItemIndex(int index,id){
//        cartItemTable.removeElementAt(index);
//        final JSONObject inner = new JSONObject();
//        try {
//                inner.put("product_id", pItem.getProductId());
//                 inner.put("product_qty", ""+pItem.getProductCartQuentity());
//                 inner.put("customer_id", Profile.getInstance().getUserId());
//
//                 new Thread(new Runnable() {
//
//                    public void run() {
//                         removeToCartSendToServer(inner.toString()) ;
//                    }
//                }).start();
//
//                     } catch (JSONException ex) {
//                ex.printStackTrace();
//            }
//    }
    public void removeItem(Object item,final Canvas canvas){

        final JSONObject inner = new JSONObject();
        try {
//                inner.put("product_id", pItem.getProductId());
//                 inner.put("product_qty", ""+pItem.getProductCartQuentity());
                 inner.put("customer_id", Profile.getInstance().getUserId());

                 new Thread(new Runnable() {

                    public void run() {
                         removeToCartSendToServer(inner.toString(),canvas) ;
                    }
                }).start();

                     } catch (JSONException ex) {
                ex.printStackTrace();
            }

        /*if(item instanceof  Product){
              cartItemTable.removeElement(item);
Product pItem = (Product)item;
               final JSONObject inner = new JSONObject();
            try {
                inner.put("product_id", pItem.getProductId());
                 inner.put("product_qty", ""+pItem.getProductCartQuentity());
                 inner.put("customer_id", Profile.getInstance().getUserId());

                 new Thread(new Runnable() {

                    public void run() {
                         removeToCartSendToServer(inner.toString()) ;
                    }
                }).start();

                     } catch (JSONException ex) {
                ex.printStackTrace();
            }*/

//            Product pItem = (Product)item;
//            if(cartItemTable.contains(pItem)){
//                for (int i = 0; i < cartItemTable.size(); i++) {
//                    Product lItem = (Product)cartItemTable.elementAt(i);
//                   if(item.equals(lItem)){
//                       if(lItem.getProductQuentity()==1)
//                       {
//                           cartItemTable.removeElement(pItem);
//                       }
//                       lItem.setProductQuentity(lItem.getProductQuentity()+1);
//                   }
//                }
//            }
//        }
//        System.out.println("Cart After Remove Item:"+cartItemTable.toString());
    }
     public Product getItem(Product item){
         Constant.printLog("CartItems>getItem>", item.toString());
        for (int i = 0; i < cartItemTable.size(); i++) {
            Product lItem = (Product)cartItemTable.elementAt(i);
             if(item.equals(lItem)){
                 return lItem ;
             }
        }
        return null ;
    }

}
