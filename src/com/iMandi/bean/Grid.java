package com.iMandi.bean;

import javax.microedition.lcdui.Image;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class Grid {
    public String title ;
    public String id;
    public String action;
    public Image image;
    public Grid(String title,String id,String action,Image image){
        this.action = action;
        this.id = id;
        this.title = title;
        this.image=image ;
        ImageList.images.addElement(image);
    }
}
