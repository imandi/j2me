package com.iMandi;

import com.ui.components.TextBox;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author nagendrakumar
 */
public class FormHelper {

    public boolean screenDown = false;
    int screenMoveY = 0;

    public int drawButton(Graphics g, int bX, int bY, int bW, int bH, byte currentSelected, String buttonTitle, int screenWidht, int screenHeight, byte buttonMargin, int buttonColor, byte myIndex) {
        g.setColor(buttonColor);
        g.fillRect(bX, bY, bW, bH);
        g.setColor(Constant.COLOR_WHITE);
        g.drawString(buttonTitle, (screenWidht / 2) - (g.getFont().stringWidth(buttonTitle) / 2), bY + ((bH / 2) - (g.getFont().getHeight() / 2)), 20);
        if (currentSelected == myIndex) {
            g.setColor(Constant.COLOR_FORM_FIELD_SELECTED);
            g.drawRect(bX - buttonMargin, bY - buttonMargin, bW + buttonMargin + buttonMargin, bH + buttonMargin + buttonMargin);
            if ((bY + buttonMargin + bH) > screenHeight - Constant.HEIGHT_BOTTOM) {
                screenDown = true;
            } else {
                screenDown = false;
            }
        }
        return bY + buttonMargin + bH;
    }

    public int drawEditField(Graphics g, int bX, int bY, int bW, int bH, byte currentSelected, String value, int screenWidht, int screenHeight, byte buttonMargin, int buttonColor, byte myIndex, TextBox box) {

//    TextBox box = new TextBox(value, value, bH, bH, bH) ;


//        g.setColor(buttonColor);
//        g.drawRect(bX, bY, bW, bH);
//        g.setColor(Constant.COLOR_WHITE);
//        g.drawString(value, (screenWidht / 2) - (g.getFont().stringWidth(value) / 2), bY + ((bH / 2) - (g.getFont().getHeight() / 2)), 20);
        if (currentSelected == myIndex) {
            box.setFocus(true);
            if ((bY + buttonMargin + bH) > screenHeight - Constant.HEIGHT_BOTTOM) {
                screenDown = true;
            } else {
                screenDown = false;
            }
//            g.setColor(Constant.COLOR_TAB_SELECTED);
//            g.drawRect(bX-buttonMargin, bY-buttonMargin, bW+buttonMargin+buttonMargin, bH+buttonMargin+buttonMargin);
        } else {
            box.setFocus(false);
        }
        value = null;
        box.paint(g);
        return bY + buttonMargin + bH;
    }

    public int drawLabel(Graphics g, int bX, int bY, String value, int screenWidht, int screenHeight, byte buttonMargin, int buttonColor, byte myIndex) {
        g.setColor(buttonColor);

        String s[] = CanvasProductList.getTextRows(value, g.getFont(), screenWidht);

        int lastY = 0;
        for (int j = 0; j < s.length; j++) {
            g.drawString(s[j], 5, lastY = bY + (j * g.getFont().getHeight()), Graphics.TOP | Graphics.LEFT);
            s[j] = null;
        }

        return lastY + g.getFont().getHeight() + buttonMargin;
    }
    /*
     *align 1 = left 2 = center 3 right
     */

    public int drawOther(Graphics g, int bX, int bY, int bW, int bH, byte currentSelected, String buttonTitle, int screenWidht, int screenHeight, byte buttonMargin, int buttonColor, byte myIndex, int align) {
        g.setColor(buttonColor);
        g.drawRect(bX, bY, bW, bH);
        g.setColor(Constant.COLOR_BLACK);
        if (align == 1) {
            g.drawString(buttonTitle, bX+buttonMargin, bY + ((bH / 2) - (g.getFont().getHeight() / 2)), 20);
        } else if (align == 2) {
            g.drawString(buttonTitle, (screenWidht / 2) - (g.getFont().stringWidth(buttonTitle) / 2), bY + ((bH / 2) - (g.getFont().getHeight() / 2)), 20);
        }

        if (currentSelected == myIndex) {
            g.setColor(Constant.COLOR_FORM_FIELD_SELECTED);
            g.drawRect(bX - buttonMargin, bY - buttonMargin, bW + buttonMargin + buttonMargin, bH + buttonMargin + buttonMargin);
            if ((bY + buttonMargin + bH) > screenHeight - Constant.HEIGHT_BOTTOM) {
                screenDown = true;
            } else {
                screenDown = false;
            }
        }
        return bY + buttonMargin + bH;
    }

    public int drawImage(Graphics g, int x, int y, int screenWidht, int screenHeight, Image image) {
        g.drawImage(image, x, y, Graphics.TOP | Graphics.LEFT);
        return y + image.getHeight();
    }
}
