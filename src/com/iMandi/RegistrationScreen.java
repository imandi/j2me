package com.iMandi;

import com.iMandi.bean.Product;
//import com.imandi.bean.CartItems;
import com.iMandi.bean.CartItems;
import com.iMandi.bean.Product;
import com.iMandi.bean.Profile;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.*;
import javax.microedition.m3g.PolygonMode;

/**
 *
 * @author nagendrakumar
 */
public class RegistrationScreen implements CommandListener {

    private TextField name, mobile, email, password, city;
    public Form form;
    private Command submit, cancel;
    private Image img, imge, img2;
    Object displayPrev;
    Product product ;
    public RegistrationScreen(Object displayPrev,Product product) {
        this.product =product ;
        this.displayPrev = displayPrev;
        form = new Form(Constant.TITLE_REGISTRATION_SCREEN);
        name = new TextField(Constant.FIELD_NAME, Profile.getInstance().getName(), 30, TextField.ANY);
        password = new TextField(Constant.FIELD_NAME, Profile.getInstance().getName(), 30, TextField.ANY);
        mobile = new TextField(Constant.FIELD_NAME, Profile.getInstance().getName(), 30, TextField.ANY);
//        address = new TextField(Constant.FIELD_ADDRESS, Profile.getInstance().getAddress(), 30, TextField.ANY);
//        pin = new TextField(Constant.FIELD_PIN_CODE, Profile.getInstance().getPinCode(), 30, TextField.ANY);
        city = new TextField(Constant.FIELD_STATE, Profile.getInstance().getStatus(), 30, TextField.ANY);
        mobile = new TextField(Constant.FIELD_MOBILE_NUMBER, Profile.getInstance().getMobileNumber(), 30, TextField.ANY);
//        cooperative = new TextField(Constant.FIELD_COOPERATIVE_NO, Profile.getInstance().getCooperativeNo(), 30, TextField.ANY);
//        district = new TextField(Constant.FIELD_DISTRICT, Profile.getInstance().getDistrict(), 30, TextField.ANY);
//        town = new TextField(Constant.FIELD_LOCALITY_NO, Profile.getInstance().getLocalityTown(), 30, TextField.ANY);

        cancel = new Command("Back", Command.CANCEL, 2);
        submit = new Command("Sumbit", Command.OK, 2);
        try {
            img = Image.createImage("logo.png");
            imge = Image.createImage("/front_left1_bad.png");
            img2 = Image.createImage("/Congratulations-1.png");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void display() {
        try {
            form.append(new ImageItem("", img, ImageItem.LAYOUT_CENTER, null));
        } catch (Exception e) {
        }
        form.append(name);
//        form.append(address);
//        form.append(pin);
//        form.append(district);
//        form.append(state);
//        form.append(town);
//        form.append(cooperative);

        form.addCommand(cancel);
        form.addCommand(submit);
        form.setCommandListener(this);
        iMandiMIDlet.display.setCurrent(form);
    }

      public void commandAction(Command c, Displayable d) {
        if (c == cancel) {
//            HelloMIDlet.display.setCurrent((Canvas) displayPrev);
            LoginScreen loginScreen = new LoginScreen(displayPrev, product) ;
            loginScreen.display();
        }
        else if (c == submit) {
            addToCart();
//            CartCanvas canvas = new CartCanvas();
//            canvas.prevDisplay = displayPrev ;
            iMandiMIDlet.display.setCurrent((Canvas)displayPrev);
            ((Canvas)displayPrev).repaint();
            Profile.getInstance().setUserId("1020");

        }
    }

    private void addToCart(){
         product.setProductCartQuentity(1);

                    if(product.getProductCartQuentity()>0){
                        CartItems.getInstance().addItem(product,null);
                    }
                    else{
                         CartItems.getInstance().removeItem(product,null);
                    }
    }
}
