package com.iMandi;

import com.ui.components.TextBox;
import com.iMandi.bean.CitrusErrors;
import com.iMandi.bean.ClientParam;
import com.iMandi.bean.Profile;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.MyConnection;
import com.iMandi.connection.MyConnectionInf;
import com.iMandi.connection.RMS;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.TextField;
import javax.obex.ClientSession;
import org.json.me.JSONException;
import org.json.me.JSONObject;

/**
 *
 * @author nagendrakumar
 */
public class MobileVerificationScreen 
 extends ImandiCanvas  implements MyConnectionInf {

    byte type = 1;
    byte totalItem = 2;
    byte currentSelected = 1;
    byte buttonMargin = 2;
    TextBox box;
    FormHelper formHelper = new FormHelper() ;
    int otpWatingTime = 0 ;
    public MobileVerificationScreen(String title,byte type) {
        this.title = title;
        if(type==2)
            LEFT_MENU = "Change Number" ;
//        CENTER_MENU = "Send";
        this.type = type;
    }

    void init(){
        if(formHelper == null)
        formHelper = new FormHelper() ;
    }
    protected void paint(Graphics g) {
        super.paint(g);
        init();
        if(type==1)
        drawEnterMobileNumberScreen(g);
        else if(type==2)
        drawOTPScreen(g);
        drawBottomMenu();
        paintLoading();


    }

    protected void keyPressed(int keyCode) {
        
//        super.keyPressed(keyCode);
        
         if (!Constant.isKnownKey(this, keyCode)) {
            if (keyCode == Constant.KEY_SOFT_LEFT) {
                totalItem = 2;
                        type =1 ;
                        box = null;
                        LEFT_MENU = null ;
                        if(timer != null)
                        timer.cancel();
                        RMS.getInstance().setRecord("I", RMS.STATE);
            } else //if (keyCode == Constant.KEY_SOFT_LEF)
            {
            }
        }
          keyCode = getGameAction(keyCode);
         if(box != null)
          box.keyPressed(keyCode, keyCode);

        if (keyCode == UP) {
                    currentSelected--;
                    if(currentSelected<=0)
                        currentSelected = totalItem;
                } //going down
                else if (keyCode == DOWN) {
                    currentSelected++;
                    if(currentSelected>totalItem)
                        currentSelected = 1 ;

                } else if (keyCode == RIGHT) {

                } else if (keyCode == LEFT) {

                } else if (keyCode == FIRE) {
                    if(showAlert){
                        showAlert = false;
                    }
                    if(currentSelected==2 && type == 1){
                         final JSONObject inner = new JSONObject();
                    try {
                        Profile.getInstance().setMobileNumber(box.getText());
                        Constant.printLog("Profile", box.getText()) ;
                        //{&quot;mobileNumber&quot;:&quot;91-9717098493&quot;}
//                        request:{"clientVersion":"imandi_android_1_0_0","countryCode":"91","mobileNumber":"91-9454026969","imei":"353202068450225","token":"353202068450225","userId":0,"pendingProfile":false}
                        inner.put("mobileNumber", Profile.getInstance().getCountryCode()+"-"+Profile.getInstance().getMobileNumber());//Profile.getInstance().getMobileNumber());
                        inner.put("clientVersion", "iMandi_J2me_1_0_0");
                        inner.put("countryCode", Profile.getInstance().getCountryCode());
                        inner.put("imei", ClientParam.IMEI);
                        inner.put("token", ClientParam.IMEI);
                        inner.put("userId", "0");
                        inner.put("pendingProfile", false);

                    } catch (JSONException ex) {
                        ex.printStackTrace();
//                        Constant.showError(ex.getMessage(),HelloMIDlet.display);
                    }
               
                    showLoading();
                    refresh();
                    MyConnection.getInstance().makeConnection(Constant.URL_REGISTRATION, MobileVerificationScreen.this, inner.toString(),"POST",(byte)1);
                    
//                    MyConnection.getInstance().postData(Constant.URL_REGISTRATION, inner.toString());
                

                        
//                        Constant.showError(RMS.getInstance().getRecord(RMS.STATE));
                    }
                    else if(currentSelected==2 /*&& otpWatingTime==0 */&& type == 2){
                         if(timer != null)
                            timer.cancel();
                         //http://13.126.89.206/tiger/rest/user/mobileverification/matchcode?userId=#userId#&code=#OTP#&token=devicetoken&imei=deviceimei&clientVersion=iMandi_Android_1_0_0
                         String url = Constant.replace(Constant.URL_VERIFY_OTP, "#userId#", Profile.getInstance().getUserId()) ;
                         url = Constant.replace(url, "#OTP#", box.getText()) ;
                         MyConnection.getInstance().makeConnection(url, MobileVerificationScreen.this, null,"GET",(byte)3);
//                         Utility.openProfileScreen(this);
                    }
                    else if(currentSelected==3 && otpWatingTime==0 && type == 2){
                        otpWatingTime = 30 ;
                        timer = new Timer();
                        tt = new TestTimerTask();
                        timer.scheduleAtFixedRate(tt, 1, 1000);
                        //"http://13.126.89.206/tiger/rest/user/mobileverification/generatecode?userId=";
                        MyConnection.getInstance().makeConnection(Constant.URL_GENERATE_OTP+Profile.getInstance().getUserId(), MobileVerificationScreen.this, null,"GET",(byte)2);
                    }
                }
        refresh();
        
    }
    TestTimerTask tt;
    Timer timer ;

    public void recycleImage() {
        super.recycleImage();
        System.gc();
        CENTER_MENU = null ;
        formHelper = null ;
    }

    private void drawEnterMobileNumberScreen(Graphics g) {
        int bX = 10;
        int bY = formHelper.drawLabel(g,bX,Constant.TITLE_HEIGHT+buttonMargin,"Enter your 10 digit mobile number",getWidth(),getHeight(),buttonMargin, 0x00000,(byte)-1);
        int bW = getWidth() - 20;
        int bH = 20;
        String buttonTitle = "";
        if(box == null){
            box = new TextBox(buttonTitle,buttonTitle, bX, bY, bW,TextField.NUMERIC,10) ;
            box.setDisplay(iMandiMIDlet.display);
            box.setContainer(this);
        }
        bY= formHelper.drawEditField(g,bX,bY,bW,bH,currentSelected,buttonTitle,getWidth(),getHeight(),buttonMargin, 0x000FF,(byte)1,box);
        bY = formHelper.drawLabel(g,bX,bY,"You will receive an OTP to verify your number.",getWidth(),getHeight(),buttonMargin, 0x00000,(byte)-1);

        buttonTitle = "Next";
        bY = getHeight() - 75;
        formHelper.drawButton(g,bX,bY,bW,bH,currentSelected,buttonTitle,getWidth(),getHeight(),buttonMargin, 0x000FF,(byte)2);
      
    }
    private void drawOTPScreen(Graphics g) {
        int bX = 10;
        int bY = formHelper.drawLabel(g,bX,Constant.TITLE_HEIGHT+buttonMargin,"Mobile Number: "+Profile.getInstance().getMobileNumber(),getWidth(),getHeight(),buttonMargin, 0x00000,(byte)-1);
        bY = formHelper.drawLabel(g,bX,bY,"One Time Password",getWidth(),getHeight(),buttonMargin, 0x00000,(byte)-1);
        int bW = getWidth() - 20;
        int bH = 20;
        String buttonTitle = "";
        if(box == null){
            box = new TextBox("","", bX, bY, bW,TextField.NUMERIC) ;
            box.setDisplay(iMandiMIDlet.display);
            box.setContainer(this);
        }
        bY= formHelper.drawEditField(g,bX,bY,bW,bH,currentSelected,buttonTitle,getWidth(),getHeight(),buttonMargin, 0x000FF,(byte)1,box);

//        bY = formHelper.drawLabel(g,bX,bY,"You will receive an OTP to verify your number.",getWidth(),getHeight(),buttonMargin, 0x00000,(byte)1);

        buttonTitle = "Next";
//        bY = getHeight() - 100;
        bY = formHelper.drawButton(g,bX,bY,bW,bH,currentSelected,buttonTitle,getWidth(),getHeight(),buttonMargin, 0x000FF,(byte)2);

        
//        bY = getHeight() - 50;
        if(otpWatingTime>0){
            buttonTitle = "00:"+otpWatingTime;
            formHelper.drawButton(g,bX,bY,bW,bH,currentSelected,buttonTitle,getWidth(),getHeight(),buttonMargin, 0xFF0000,(byte)3);
        }
        else{
            buttonTitle = "Resend OTP";
            formHelper.drawButton(g,bX,bY,bW,bH,currentSelected,buttonTitle,getWidth(),getHeight(),buttonMargin, 0x000FF,(byte)3);
        }
    }


private class TestTimerTask extends TimerTask
  {
    public final void run()
    {
        otpWatingTime--;
        if(otpWatingTime<0){
            timer.cancel();
            otpWatingTime = 0 ;
        }
//      fmMain.append("run count: " + ++count + "\n");
        refresh();
    }
  }
public void sendData(String data,byte requestId) {
    super.sendData(data,requestId);
    if(requestId==1){
//    data = "{\"userId\":2,\"status\":\"success\",\"message\":\"User already registered.\"}" ;
        Profile profile = JsonStringToOjbect.getProfile(data);
        if(profile.getUserId() != null ){
            profile.setMessage("User registration successful");
            if(profile.getMessage().equalsIgnoreCase("User already registered.")){
                RMS.getInstance().setRecord(data, RMS.PROFILE);
                Utility.openLoginAsGuest(this);
                return;
            }else if(profile.getMessage().equalsIgnoreCase("User registration successful")){
                RMS.getInstance().setRecord(data, RMS.PROFILE);
                RMS.getInstance().setRecord("OTP", RMS.STATE);
                RMS.getInstance().setRecord("{}", RMS.PROFILE);
                        otpWatingTime = 30 ;
                        totalItem = 3;
                        type =2 ;
                        box = null;
                        LEFT_MENU = "Change Number" ;
                        timer = new Timer();
                        tt = new TestTimerTask();
                        timer.scheduleAtFixedRate(tt, 1, 1000);
            }
            MyConnection.getInstance().makeConnection(Constant.URL_GENERATE_OTP+profile.getUserId(), MobileVerificationScreen.this, null,"GET",(byte)2);
        }else{
            Constant.showError("Something went wrong!");
        }
    }else if(requestId==2){
        CitrusErrors citrusErrors = JsonStringToOjbect.getMessage(data);
        if(citrusErrors.messageSub!=null){
            Constant.showError(citrusErrors.messageSub);
            if(citrusErrors.code.equalsIgnoreCase("20035")){
                otpWatingTime = 0 ;
                if(timer!=null)
                timer.cancel();
            }
        }
        else
            Constant.showError(citrusErrors.message);
    }else if(requestId==3){
//        {"username":"91983tx74e4d621","password":"bhvsxdxQWt","status":"success","message":"Congratulations!! Your mobile number (91-9818662101) is successfully verified."}
//sendData::{"username":"91983tx74e4d621","password":"bhvsxdxQWt","status":"success","message":"Congratulations!! Your mobile number (91-9818662101) is successfully verified."}requestID 3
//getMessage jsonText -->{"username":"91983tx74e4d621","password":"bhvsxdxQWt","status":"success","message":"Congratulations!! Your mobile number (91-9818662101) is successfully verified."}
//o
//        Utility.openProfileScreen(this);
//        CitrusErrors citrusErrors = JsonStringToOjbect.getMessage(data);
        Profile profile = JsonStringToOjbect.getProfile(data);
        Constant.printLog("profile.getStatus()", profile.getStatus());
        Constant.printLog("profile.messageSub", profile.messageSub);
        if(profile.messageSub!=null){
            Constant.showError(profile.messageSub);
            if(profile.code.equalsIgnoreCase("20035")){
                otpWatingTime = 0 ;
                if(timer!=null)
                timer.cancel();
            }
        }
        else{
            if(profile.getStatus().equalsIgnoreCase("success")){
            profile.saveMe();
                if(Profile.getInstance().getPasswordEncrepted() == null)
                    Profile.getInstance().setPasswordEncrepted(Utility.encriptPass(Profile.getInstance().getPassword())) ;
//                String url = Constant.replace(Constant.URL_SET_VERIFY, "#userId#", Profile.getInstance().getUserId()) ;
//                MyConnection.getInstance().makeConnection(url, MobileVerificationScreen.this, null,"GET",(byte)4);
                Constant.showError(profile.message,new ProfileCanvas("Profile"));
                
            }else{
            Constant.showError(profile.message);
            }
        }
    }
        refresh();
    }

    public void sendStatus(String Status,byte requestId) {
        super.sendStatus(Status,requestId);
    }

    public void myConnectionBusy(String Status,byte requestId) {
        super.myConnectionBusy(Status);       
    }
}
