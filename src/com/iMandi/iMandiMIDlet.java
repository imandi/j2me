package com.iMandi;

import com.iMandi.bean.CartItems;
import com.iMandi.bean.Profile;
import com.iMandi.bean.ServerMessage;
import com.iMandi.connection.*;
import com.iMandi.connection.MyConnectionInf;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Vector;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.io.file.FileSystemRegistry;
import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;
import org.json.me.JSONException;
import org.json.me.JSONObject;
import org.json.me.util.ImageList;

public class iMandiMIDlet extends MIDlet {

    public static Display display;     // The display for this MIDlet

    public iMandiMIDlet() {
        Constant.printLog("Initial memory:", "");
        Constant.printMemory();
        display = Display.getDisplay(this);
    }

    public void startApp() {
        SplashScreenSwitcher screenSwitcher = new SplashScreenSwitcher(display, new Splash());
        screenSwitcher.start();
    }

    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
    }

    protected void pauseApp() {
    }

    class SplashScreenSwitcher extends Thread {

        private Display display;
        private Displayable splashScreen;
        private Displayable nextScreen;

        public SplashScreenSwitcher(Display display, Displayable splashScreen) {
            this.display = display;
            this.splashScreen = splashScreen;
//         this.nextScreen = nextScreen;
        }

        public void run() {
            display.setCurrent(splashScreen);
            if (true) {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
                Profile.getInstance().restoreMe();
                Profile profile = Profile.getInstance();
                if (profile.getUserId() != null /*&& 1 == 2*/) {
                    Utility.openLoginAsGuest(null);
//                    display.setCurrent(new ProfileCanvas("Profile"));
                } else {
                    if(RMS.getInstance().getRecord(RMS.STATE).equalsIgnoreCase("OTP"))
                         iMandiMIDlet.display.setCurrent(new MobileVerificationScreen("Login",(byte)2));
                    else
//                    iMandiMIDlet.display.setCurrent(new MobileVerificationScreen("Login",(byte)1));
                    display.setCurrent(new LoginRegistration("Login",1));
//                    Utility.openLoginAsGuest(null);
//                    display.setCurrent(new ProfileCanvas("Profile"));
                }
            }
        }
    }
}
