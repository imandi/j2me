package com.iMandi;

import com.iMandi.bean.Product;
import com.iMandi.bean.CartItems;
import java.util.Vector;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import org.json.me.util.ImageList;

public class CanvasProductCartList //extends Canvas
{

    protected int linePadding = 2;
    protected int topSpace = 0;
    protected int margin = 2;
    protected int padding = 2;
//    protected Font font = Font.getDefaultFont();
    protected int borderWidth = 1;
    // will contain item splitted lines
    String[][] itemLines = null;
    // will hold items image parts
    Image[] images = null;
    // will hold selected item index
    public int selectedItem = 0;
    // these will hold item graphical properties
    int[] itemsTop = null;
    int[] itemsHeight = null;
    // these will hold List vertical scrolling
    int scrollTop = 0;
    final int SCROLL_STEP = 70;
    Image voiceRecord = null;
    Image videoRecord = null;
    Image product = null;
    private CartCanvas canvas;
    boolean continueShoping = false;
    int rowMenuIndex = 0;
    int maxCommandInRow = 1;
//	public CanvasProductList(String title, String[] items, Image[] imageElements,TabMenuCanvas canvas)
//	{
//            this.canvas = canvas;
//
//            try{
//                    voiceRecord = Image.createImage("voice_record.png");
//                    videoRecord = Image.createImage("video_record.jpeg");
//                            }
//catch(Exception e)
//{
//	e.printStackTrace();
//}
//
////		canvas.setTitle(title);
//
////		this.images = imageElements;
//
//		itemLines = new String[items.length][];
//
//		itemsTop = new int[itemLines.length];
//		itemsHeight = new int[itemLines.length];
//
//		for(int i = 0; i < itemLines.length; i++)
//		{
//			// get image part of this item, if available
//			Image imagePart =product;// getImage(i);
//
//			// get avaiable width for text
//			int w = getItemWidth() - (imagePart != null ? imagePart.getWidth() + padding : 0);
//
//			// and split item text into text rows, to fit available width
//			itemLines[i] = getTextRows((String) items[i], font, w);
//		}
//	}
    Vector products;

    public CanvasProductCartList(String title, Vector products, CartCanvas canvas) {
        this.products = products;
        this.canvas = canvas;
        CartItems.getInstance().cartItemTable.removeAllElements();
        ;
        try {
            voiceRecord = Image.createImage("/voice_record.png");
            videoRecord = Image.createImage("/video_record.jpeg");
            product = Image.createImage("/default.png");
        } catch (Exception e) {
            e.printStackTrace();
        }

//		canvas.setTitle(title);

//		this.images = imageElements;

        itemLines = new String[products.size()][];

        itemsTop = new int[products.size()];
        itemsHeight = new int[products.size()];

        for (int i = 0; i < itemLines.length; i++) {
            // get image part of this item, if available
            Image imagePart = product;//getImage(i);

            // get avaiable width for text
            int w = getItemWidth() - (imagePart != null ? imagePart.getWidth() + padding : 0);

            // and split item text into text rows, to fit available width
            itemLines[i] = getTextRows((String) ((Product) products.elementAt(i)).getProductName(), Constant.font_small, w);
        }
    }

    public int getItemWidth() {
        return canvas.getWidth() - 2 * borderWidth - 2 * padding - 2 * margin;
    }

    public void keyPressed(int key) {
        int keyCode = canvas.getGameAction(key);

        if (!Constant.isKnownKey(canvas, key)) {
            if (key == Constant.KEY_SOFT_LEFT) {
//            fireLeftSoftCommand();
                iMandiMIDlet.display.setCurrent(new CategoryScreen());
            } else //if (key == canvas.KEY_SOFT_LEFT)
            {
                if (CartItems.getInstance().cartItemTable != null && CartItems.getInstance().cartItemTable.size() > 0) {
                    //(new ProfileScreen(canvas)).display();
                    iMandiMIDlet.display.setCurrent(new BankScreen());
                }
            }
        }
        // is there 1 item at least?
        if (itemLines.length > 0) {
            // going up
            if (keyCode == Canvas.UP) {
                // current item is clipped on top, so can scroll up
                if (itemsTop[selectedItem] < scrollTop) {
                    scrollTop -= SCROLL_STEP;

                    canvas.repaint();
                } // is there a previous item?
                else if (selectedItem > 0) {
                    selectedItem--;

                    canvas.repaint();
                }
                rowMenuIndex = 0;
                continueShoping = false;
            } //going down
            else if (keyCode == Canvas.DOWN) {
                // current item is clipped on bottom, so can scroll down
                if (itemsTop[selectedItem] + itemsHeight[selectedItem] >= scrollTop + getHeight()) {
                    scrollTop += SCROLL_STEP;

                    canvas.repaint();
                } // is there a following item?
                else if (selectedItem < itemLines.length - 1) {
                    selectedItem++;

                    canvas.repaint();
                }
                continueShoping = false;
                rowMenuIndex = 0;
            } else if (keyCode == Canvas.RIGHT) {
                continueShoping = true;
                rowMenuIndex++;
                if (rowMenuIndex > maxCommandInRow) {
                    rowMenuIndex = 1;
                }
            } else if (keyCode == Canvas.LEFT) {
                if (continueShoping) {
                    rowMenuIndex--;
                    if (rowMenuIndex == 0);
                    rowMenuIndex = 1;
                }
            } else if (keyCode == Canvas.FIRE) {

              
                Alert warningAlert = new Alert("Are you sure want to delete cart?");
                warningAlert.setTitle("Are you sure want to delete cart?");
                warningAlert.setString("Are you sure want to delete cart?");
                warningAlert.setType(AlertType.CONFIRMATION);
                warningAlert.setTimeout(Alert.FOREVER);

                final Command okCmd = new Command("OK", Command.OK, 4);
                warningAlert.addCommand(okCmd);
                final Command cancelCmd = new Command("Cancel", Command.CANCEL, 3);
                warningAlert.addCommand(cancelCmd);

                warningAlert.setCommandListener(new CommandListener() {

                    public void commandAction(Command c, Displayable d) {
                        if (c == okCmd) {
                            try {
                                  Product p = (Product) products.elementAt(selectedItem);
                                CartItems.getInstance().removeItem(p, canvas);
                                 CartItems.getInstance().cartItemTable.removeAllElements();
                                 itemLines = new String[0][];
                                 iMandiMIDlet.display.setCurrent(canvas);

                            } catch (IndexOutOfBoundsException ioobe) {
                                iMandiMIDlet.display.setCurrent(canvas);
                            }
                        } else if (c == cancelCmd) {
                            iMandiMIDlet.display.setCurrent(canvas);
                        }
                    }
                });

                iMandiMIDlet.display.setCurrent(warningAlert);
            }
        }
    }

//	Image getImage(int index)
//	{
//		return images != null && images.length > index ? images[index] : null;
//	}
//    float totalPrice = 0;
    protected void paint(Graphics g, int startX) {
        // paint List background
//        totalPrice = 0;
        g.setColor(Constant.COLOR_bgColor);
        g.fillRect(0, startX, canvas.getWidth(), canvas.getHeight());

        // translate accordingly to current List vertical scroll
        g.translate(0, -scrollTop);

        int top = startX;

        g.setFont(Constant.font_small);

        // loop List items
        for (int i = 0; i < itemLines.length; i++) {
            Product p = (Product) products.elementAt(i);
//            totalPrice = totalPrice + (p.getProductBasePrice()*p.getProductCartQuentity()) ;
            int itemRows = 2;//itemLines[i].length;

            Image imagePart = null;//product;//imgetImage(i);

            int itemHeight = itemRows * Constant.font_small.getHeight() + linePadding * (itemRows - 1);

            itemsTop[i] = top;
            itemsHeight[i] = itemHeight;

            // is image part higher than the text part?
            if (imagePart != null && imagePart.getHeight() > itemHeight) {
                itemHeight = imagePart.getHeight();
            }
//            itemHeight += 2 * padding + 2 * borderWidth;
            itemHeight = (g.getFont().getHeight()*2)+10;

            g.translate(0, top);

            if (borderWidth > 0) {
                g.setColor(i == selectedItem ? Constant.COLOR_borderColor : Constant.COLOR_borderColor);
                g.fillRect(margin, margin + topSpace, canvas.getWidth() - 2 * margin, itemHeight);
            }

            // paint item background
            g.setColor(i == selectedItem ? Constant.COLOR_backSelectedColor : Constant.COLOR_backColor);
            g.drawRect(margin + borderWidth, margin + borderWidth + topSpace, canvas.getWidth() - 2 * margin - 2 * borderWidth, itemHeight - 2 * borderWidth);

            // has this item an image part?
            if (imagePart != null) {
                g.drawImage(imagePart, margin + borderWidth + padding, margin + borderWidth + padding + topSpace, Graphics.TOP | Graphics.LEFT);
            }

            if(i == selectedItem)
                {
                    g.setColor(Constant.COLOR_backSelectedColor);
                    g.fillRect(margin + borderWidth, margin + borderWidth + topSpace,3, itemHeight - 2 * borderWidth);
                }
            
            // paint item text rows
            g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

//            g.setColor(Constant.COLOR_ITEM_TEXT);
            int textLeft = margin + borderWidth + padding + (imagePart != null ? imagePart.getWidth() + padding : 0);

            int nextRow = 0;

            ((ImandiCanvas)canvas).getPrevFont();
                g.setFont(Constant.font_medium_bold);
                g.drawString(itemLines[i][0], textLeft+6, topSpace + margin + borderWidth, Graphics.TOP | Graphics.LEFT);
                g.setFont(Constant.font_medium);
                String suk = p.getProductSku();
                g.drawString(suk, textLeft+6, topSpace + margin + borderWidth + padding  + g.getFont().getHeight(), Graphics.TOP | Graphics.LEFT);
                g.setFont(Constant.font_medium_bold);
                g.drawString(" | "+p.getProductPrice(), textLeft+6+(g.getFont().stringWidth(suk)), topSpace + margin + borderWidth + padding  + g.getFont().getHeight(), Graphics.TOP | Graphics.LEFT);
                
//            g.drawString(itemLines[i][0], textLeft, topSpace + margin + borderWidth + padding + 0 * (linePadding + Constant.font_small.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
//            g.drawString("Quantity: " + p.getProductCartQuentity(), textLeft, (g.getFont().getHeight()) + topSpace + margin + borderWidth + padding + 0 * (linePadding + Constant.font_small.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);

//            g.drawString("" + p.getProductPrice(), (canvas.getWidth() - g.getFont().stringWidth(p.getProductPrice())), topSpace + margin + borderWidth + padding + 0 * (linePadding + Constant.font_small.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);


            ((ImandiCanvas)canvas).setPrevFont();

            g.translate(0, -top);

            top += itemHeight + 2 * margin;
        }
        // finally, translate back
        g.translate(0, scrollTop);
        drawBottom(g);
        if (CartItems.getInstance().cartItemTable != null && CartItems.getInstance().cartItemTable.size() > 0) {
            drawBottomSumTotal(g);
        }
    }

    protected void paintXXXX(Graphics g, int startX) {
        // paint List background
//        totalPrice = 0;
        g.setColor(Constant.COLOR_bgColor);
        g.fillRect(0, startX, canvas.getWidth(), canvas.getHeight());

        // translate accordingly to current List vertical scroll
        g.translate(0, -scrollTop);

        int top = startX;

        g.setFont(Constant.font_small);

        // loop List items
        for (int i = 0; i < itemLines.length; i++) {
            Product p = (Product) products.elementAt(i);
//            totalPrice = totalPrice + (p.getProductBasePrice()*p.getProductCartQuentity()) ;
            int itemRows = itemLines[i].length;

            Image imagePart = null;//product;//imgetImage(i);

            int itemHeight = itemRows * Constant.font_small.getHeight() + linePadding * (itemRows - 1);

            itemsTop[i] = top;
            itemsHeight[i] = itemHeight;

            // is image part higher than the text part?
            if (imagePart != null && imagePart.getHeight() > itemHeight) {
                itemHeight = imagePart.getHeight();
            }
//            itemHeight += 2 * padding + 2 * borderWidth;
itemHeight = (g.getFont().getHeight()*2)+10;
            g.translate(0, top);

            if (borderWidth > 0) {
                // paint item border
//				g.setColor(i == selectedItem ? Constant.COLOR_borderSelectedColor : Constant.COLOR_borderColor);
//				g.fillRect(margin, margin+topSpace, canvas.getWidth() - 2 * margin, itemHeight);
            }

            // paint item background
            g.setColor(i == selectedItem ? Constant.COLOR_backSelectedColor : Constant.COLOR_backColor);
            g.fillRect(margin + borderWidth, margin + borderWidth + topSpace, canvas.getWidth() - 2 * margin - 2 * borderWidth, itemHeight - 2 * borderWidth);

            // has this item an image part?
            if (imagePart != null) {
                g.drawImage(imagePart, margin + borderWidth + padding, margin + borderWidth + padding + topSpace, Graphics.TOP | Graphics.LEFT);
            }

            // paint item text rows
            g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

            int textLeft = margin + borderWidth + padding + (imagePart != null ? imagePart.getWidth() + padding : 0);

            int nextRow = 0;

            g.drawString(itemLines[i][0], textLeft, topSpace + margin + borderWidth + padding + 0 * (linePadding + Constant.font_small.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
//            g.drawString("" + p.getProductSku(), textLeft + (canvas.getWidth() / 3), topSpace + margin + borderWidth + padding + 0 * (linePadding + font.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
            g.drawString("" + p.getProductSku(), textLeft, topSpace + margin + borderWidth + padding + 0 * (linePadding + Constant.font_small.getHeight()) + nextRow + Constant.font_small.getHeight(), Graphics.TOP | Graphics.LEFT);
            g.drawString("" + p.getProductCartQuentity() + "", textLeft + (canvas.getWidth() / 2), topSpace + margin + borderWidth + padding + 0 * (linePadding + Constant.font_small.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);
            g.drawString("" + p.getProductPrice(), textLeft + (canvas.getWidth() - canvas.getWidth() / 3), topSpace + margin + borderWidth + padding + 0 * (linePadding + Constant.font_small.getHeight()) + nextRow, Graphics.TOP | Graphics.LEFT);

//             if(rowMenu && i == selectedItem){
//                 g.setColor( Constant.COLOR_borderSelectedColor);
//                 g.drawRect(textLeft + (canvas.getWidth() / 2), topSpace + margin + borderWidth + padding + 0 * (linePadding + font.getHeight()) + nextRow, 30, 20);
//             }
            g.setColor(i == selectedItem ? Constant.COLOR_borderSelectedColor : Constant.COLOR_backSelectedColor);
//            g.setColor( Constant.COLOR_backSelectedColor);
            g.drawRect(textLeft + (canvas.getWidth() / 2), topSpace + margin + borderWidth + padding + 0 * (linePadding + Constant.font_small.getHeight()) + nextRow, 30, 20);

//                          g.drawString(Constant.LIST_TITLE_PRODUCT_NAME, 0, titelStartY+((titelWidth-f.getHeight())/2), Graphics.LEFT | Graphics.TOP);
//        g.drawString(Constant.LIST_TITLE_PRODUCT_SIZE, getWidth()/3, titelStartY+((titelWidth-f.getHeight())/2), Graphics.LEFT | Graphics.TOP);
//        g.drawString(Constant.LIST_TITLE_PRODUCT_QUENTITY, getWidth()/2, titelStartY+((titelWidth-f.getHeight())/2), Graphics.LEFT | Graphics.TOP);
//        g.drawString(Constant.LIST_TITLE_PRODUCT_PRICE, getWidth()-getWidth()/3, titelStartY+((titelWidth-f.getHeight())/2), Graphics.LEFT | Graphics.TOP);


//			for(int j = 0; j < 1; j++)
            {
//                                nextRow = 0;
//				g.drawString(itemLines[i][0]+rowMenuIndex, textLeft,topSpace+ margin + borderWidth + padding + 0 * (linePadding + font.getHeight())+nextRow, Graphics.TOP | Graphics.LEFT);
//                                nextRow = nextRow +font.getHeight();
//                                g.drawString(itemLines[i][j], textLeft,topSpace+ margin + borderWidth + padding + j * (linePadding + font.getHeight())+nextRow, Graphics.TOP | Graphics.LEFT);
//                                nextRow = nextRow +font.getHeight();
//                                g.drawString(itemLines[i][j], textLeft,topSpace+ margin + borderWidth + padding + j * (linePadding + font.getHeight())+nextRow, Graphics.TOP | Graphics.LEFT);
            }

            for (int j = 0; j < itemRows; j++) {
//                                nextRow = 0;
//				g.drawImage(voiceRecord,(canvas.getWidth()-voiceRecord.getWidth())-20,topSpace+ margin + borderWidth + padding + j * (linePadding + font.getHeight())+nextRow, Graphics.TOP | Graphics.LEFT);
//                                nextRow = nextRow +voiceRecord.getHeight()+voiceRecord.getHeight();
//                                g.drawImage(voiceRecord,(canvas.getWidth()-voiceRecord.getWidth())-20,topSpace+ margin + borderWidth + padding + j * (linePadding + font.getHeight())+nextRow, Graphics.TOP | Graphics.LEFT);
//                                if(rowMenu && i == selectedItem){
//                                    if(rowMenuIndex==1)
//                                    nextRow = 0;
//                                    if(rowMenuIndex==2)
//                                    nextRow = voiceRecord.getHeight()+voiceRecord.getHeight();
//
//                                    g.setColor( Constant.COLOR_borderSelectedColor);
//                                    g.drawRect((canvas.getWidth()-voiceRecord.getWidth())-20, topSpace+ margin + borderWidth + padding + j * (linePadding + font.getHeight())+nextRow, voiceRecord.getWidth(), voiceRecord.getHeight());
//                                }
            }

            g.translate(0, -top);

            top += itemHeight + 2 * margin;
        }
        // finally, translate back
        g.translate(0, scrollTop);
        drawBottom(g);
        drawBottomSumTotal(g);
    }

    static String[] getTextRows(String text, Font font, int width) {
        char SPACE_CHAR = ' ';
        String VOID_STRING = "";

        int prevIndex = 0;
        int currIndex = text.indexOf(SPACE_CHAR);

        Vector rowsVector = new Vector();

        StringBuffer stringBuffer = new StringBuffer();

        String currentToken;

        String currentRowText = VOID_STRING;

        while (prevIndex != -1) {
            int startCharIndex = prevIndex == 0 ? prevIndex : prevIndex + 1;

            if (currIndex != -1) {
                currentToken = text.substring(startCharIndex, currIndex);
            } else {
                currentToken = text.substring(startCharIndex);
            }

            prevIndex = currIndex;

            currIndex = text.indexOf(SPACE_CHAR, prevIndex + 1);

            if (currentToken.length() == 0) {
                continue;
            }

            if (stringBuffer.length() > 0) {
                stringBuffer.append(SPACE_CHAR);
            }

            stringBuffer.append(currentToken);

            if (font.stringWidth(stringBuffer.toString()) > width) {
                if (currentRowText.length() > 0) {
                    rowsVector.addElement(currentRowText);
                }
                stringBuffer.setLength(0);

                currentRowText = VOID_STRING;

                stringBuffer.append(currentToken);

                currentRowText = stringBuffer.toString();
            } else {
                currentRowText = stringBuffer.toString();
            }
        }
        if (currentRowText.length() > 0) {
            rowsVector.addElement(currentRowText);
        }
        String[] rowsArray = new String[rowsVector.size()];

        rowsVector.copyInto(rowsArray);

        return rowsArray;
    }

    void drawBottomSumTotal(Graphics g) {
        g.setColor(Constant.COLOR_PAYMENT_SCREEN_BG);
        g.fillRect(0, canvas.getHeight() - ((Constant.HEIGHT_BOTTOM * 2)), canvas.getWidth(), Constant.HEIGHT_BOTTOM);
        g.setColor(0);
        Font f = g.getFont();
        g.setColor(Constant.COLOR_ITEM_BG);
        if (continueShoping) {
            g.setColor(Constant.COLOR_borderSelectedColor);
        }
//        g.drawString(Constant.LIST_TITLE_CONTINUE_SHOPING, 10, canvas.getHeight()-((Constant.HEIGHT_BOTTOM*2)+10), 20);

        g.setFont(Constant.font_medium_bold);
        g.setColor(Constant.COLOR_BLACK);
        g.drawString(Constant.LIST_TITLE_SUB_TOTAL + "   " + CartItems.getInstance().total_amount, canvas.getWidth() - g.getFont().stringWidth(Constant.LIST_TITLE_SUB_TOTAL + "     " + CartItems.getInstance().total_amount), (canvas.getHeight() - ((Constant.HEIGHT_BOTTOM * 2))) + ((Constant.HEIGHT_BOTTOM - g.getFont().getHeight()) / 2), 20);
//        g.drawString(CartItems.getInstance().total_amount, canvas.getWidth() - f.stringWidth(CartItems.getInstance().total_amount), (canvas.getHeight() - ((Constant.HEIGHT_BOTTOM * 2))) + ((Constant.HEIGHT_BOTTOM - g.getFont().getHeight()) / 2), 20);
    }

    void drawBottom(Graphics g) {
//        g.setColor(Constant.COLOR_BOTTOM);
//        g.fillRect(0, canvas.getHeight()-(Constant.HEIGHT_BOTTOM+10), canvas.getWidth(), Constant.HEIGHT_BOTTOM+10);

        g.setColor(Constant.COLOR_BOTTOM);

        g.fillRect(0, canvas.getHeight() - (Constant.HEIGHT_BOTTOM), canvas.getWidth(), Constant.HEIGHT_BOTTOM);


        g.setColor(Constant.COLOR_BLACK);
        g.drawString(Constant.LIST_TITLE_CONTINUE_SHOPING, 2, canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
        //g.drawString("Add to cart", (canvas.getWidth()/2)-(g.getFont().stringWidth("Add to cart")/2), canvas.getHeight() - 2,Graphics.BOTTOM|Graphics.LEFT);
        if (CartItems.getInstance().cartItemTable != null && CartItems.getInstance().cartItemTable.size() > 0) {
            g.drawString(Constant.PLACE_ORDER, canvas.getWidth() - g.getFont().stringWidth(Constant.PLACE_ORDER), canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
        }

        if (CartItems.getInstance().cartItemTable.size() > 0) {
            g.drawString(Constant.DELETE_ALL, (canvas.getWidth() / 2) - (g.getFont().stringWidth(Constant.DELETE_ALL) / 2), canvas.getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);
        }

    }

    int getHeight() {
//        return canvas.getHeight() - (Constant.HEIGHT_BOTTOM * 2);
        return canvas.getHeight() - (ImageList.cartImage.getHeight() * 2);
    }
}
