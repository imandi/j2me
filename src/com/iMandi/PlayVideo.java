package com.iMandi;

import com.iMandi.bean.Product;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.GameCanvas;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;
import javax.microedition.media.control.VideoControl;
import javax.microedition.media.control.VolumeControl;
import javax.microedition.midlet.MIDlet;

public class PlayVideo  implements CommandListener {
  private Player player = null;

  private VideoControl videoControl = null;

  private VolumeControl volControl = null;


  private MovableVideoCanvas canvas = new MovableVideoCanvas(videoControl);

  private Command exitCommand = new Command("Back", Command.EXIT, 1);

  private Command stopAudioCommand = null;

//  private Display display = Display.getDisplay(this);

  private Alert alert = new Alert("Error");

  Canvas prevDisplay ;
  Product product ;
  public PlayVideo(Canvas prevDisplay,String url,Product product) {
      this.prevDisplay = prevDisplay ;
      this.product = product ;
    alert.addCommand(exitCommand);
    alert.setCommandListener(this);

    try {
              player = Manager.createPlayer(getClass().getResourceAsStream(url), "video/3gpp");//"/test.3gp"
              player.realize();
       videoControl = (VideoControl) player
      .getControl("javax.microedition.media.control.VideoControl");

  volControl = (VolumeControl) player
      .getControl("javax.microedition.media.control.VolumeControl");

//      player.realize();

      stopAudioCommand = new Command("Stop Audio", Command.SCREEN, 1);

      canvas.addCommand(exitCommand);
      canvas.addCommand(stopAudioCommand);
      canvas.setCommandListener(this);

      videoControl.initDisplayMode(VideoControl.USE_DIRECT_VIDEO, canvas);

      int halfCanvasWidth = canvas.getWidth() / 2;
      int halfCanvasHeight = canvas.getHeight() / 2;

      videoControl.setDisplayFullScreen(false);
      videoControl.setDisplaySize(halfCanvasWidth, halfCanvasHeight);
      videoControl.setDisplayLocation(halfCanvasWidth / 2,
          halfCanvasHeight / 2);
      videoControl.setVisible(true);

       player.start();
       iMandiMIDlet.display.setCurrent(canvas);

    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  public void startApp() {
//    try {
//      player.start();
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//    HelloMIDlet.display.setCurrent(canvas);
  }

  public void pauseApp() {
  }

  public void destroyApp(boolean unconditional) {
//    try {
//      if (player != null)
//        player.close();
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
  }

  public void commandAction(Command cmd, Displayable disp) {
    if (cmd == exitCommand) {
//      destroyApp(true);
//      notifyDestroyed();
        try {
      if (player != null)
        player.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
        iMandiMIDlet.display.setCurrent(this.prevDisplay);
    } else if (cmd == stopAudioCommand) {
      if (volControl != null)
        volControl.setMute(true);
    }
  }
}

class MovableVideoCanvas extends GameCanvas {
  private VideoControl videoControl = null;

    public MovableVideoCanvas(VideoControl vControl) {
    super(false);
    this.videoControl = vControl;
  }
  public void paint(Graphics g) {
    flushGraphics();
  }

  public void keyPressed(int keyCode) {
    int y = videoControl.getDisplayY();
      y -= 2;
    videoControl.setDisplayLocation(videoControl.getDisplayX(), y);
    repaint();
  }

}