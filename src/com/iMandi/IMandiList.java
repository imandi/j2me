package com.iMandi;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Image;

/**
 *
 * @author nagendrakumar
 */
public class IMandiList {
protected int linePadding = 2;
    protected int topSpace = 0;
    protected int margin = 2;
    protected int padding = 2;
    public Font font = Font.getDefaultFont();
    boolean showDetail = false;
    protected int borderWidth = 2;
    // will contain item splitted lines
//    String[][] itemLines = null;
    // will hold items image parts
    Image[] images = null;
    // will hold selected item index
    public int selectedItem = 0;
    // these will hold item graphical properties
    int[] itemsTop = null;
    int itemHeight = 50;
//    int[] itemsHeight = null;
    // these will hold List vertical scrolling
    int scrollTop = 0;
    final int SCROLL_STEP = itemHeight;//40;
    int maxCache = 15;
    
}
