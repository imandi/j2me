package com.iMandi;

import com.iMandi.bean.CartItems;
import com.iMandi.bean.ChatMessage;
import com.iMandi.bean.Product;
import com.iMandi.bean.Profile;
import com.iMandi.connection.MyConnection;
import java.util.Date;
import java.util.Vector;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class FourmChat extends ImandiCanvas{


    ChatMessage chatMessage;
    ChatDetail detailView = new ChatDetail();
//    int detailY = 0;

    public FourmChat() {
        title = "Soil management";
        LEFT_MENU = "Back";
        RIGHT_MENU = "Post";
        CENTER_MENU = "View";

        ChatMessage chatMessage = new ChatMessage();
        chatMessage.message = "Can anyone explain me what is relay cropping" ;
        chatMessage.senderName="Mohd Tariq";
        String s[] = CanvasProductList.getTextRows(chatMessage.message, Constant.font_small, getWidth()-50);
        chatMessage.messageSort = s[0]+"..." ;
        chatMessage.date = (new Date()).toString();
        chatMessage.date = dateFormate(chatMessage.date);
        chatMessage.contentType="t";
        Vector content = new Vector();
//        content.addElement("http://vignette.wikia.nocookie.net/farmville/images/e/e1/Corn-icon.png/revision/latest?cb=20091218043346");
//        chatMessage.contentVec = content ;
        Vector v = new Vector();
        v.addElement(chatMessage);

        chatMessage = new ChatMessage();
        chatMessage.message = "Its one of the indigenous practice used in soil conversation and management. its benefits with reduction in runoff and better utilisation of soil moisture" ;
        s = CanvasProductList.getTextRows(chatMessage.message, Constant.font_small, getWidth()-60);
        chatMessage.messageSort = s[0]+"..." ;
        chatMessage.senderName="Amanpreet kaur chadha";
        
        chatMessage.date = (new Date()).toString();
        chatMessage.date = dateFormate(chatMessage.date);
        chatMessage.contentType="t";
        content = new Vector();
//        content.addElement("http://vignette.wikia.nocookie.net/onthefarm/images/1/1f/Blueberry-icon.png/revision/latest?cb=20120618202503");
//        chatMessage.contentVec = content ;

        v.addElement(chatMessage);

        chatMessage = new ChatMessage();
        chatMessage.message = "thanks for sharing such valuable information. What crops can I grow in this?" ;
        s = CanvasProductList.getTextRows(chatMessage.message, Constant.font_small, getWidth()-50);
        chatMessage.messageSort = s[0]+"..." ;
        chatMessage.senderName="Mohd Tariq";
        chatMessage.date = (new Date()).toString();
        chatMessage.date = dateFormate(chatMessage.date);
        chatMessage.contentType="t";
        v.addElement(chatMessage);
        

        chatMessage = new ChatMessage();
        chatMessage.message = "" ;
        chatMessage.senderName="Amanpreet Kaur Chadha";
        chatMessage.date = (new Date()).toString();
        chatMessage.date = dateFormate(chatMessage.date);
        chatMessage.contentType="ti";
        content = new Vector();
        content.addElement("http://i.imgur.com/KTVDvSj.png");
        chatMessage.contentVec = content ;        
        v.addElement(chatMessage);

        

        chatMessage = new ChatMessage();
        chatMessage.message = "" ;
        chatMessage.senderName="Mohd Tariq";
        chatMessage.date = (new Date()).toString();
        chatMessage.date = dateFormate(chatMessage.date);
        chatMessage.contentType="ta";
        v.addElement(chatMessage);

        list = new List("", v, this);

//        isLoading= true;
//        MyConnection.getInstance().makeConnection(Constant.URL_PRODUCT_LIST + ((Category) Constant.currentCategory.elementAt(0)).getCategoryid(), this, null, "GET", (byte) 1);
    }
    List list;

    protected void paint(Graphics g) {
        super.paint(g);

        if (list != null && chatMessage == null) {
            getPrevFont();
            list.paint(g);
            setPrevFont();
        }

        if (isLoading) {
            Constant.loading(g, this, Constant.LOADING);
        }
        showMenu();

        if (chatMessage != null) {
            detailView.diaplayDetail(g, this, chatMessage, screenMoveY);
             drawTitle(title);
             if (isLoading) {
                Constant.loading(g, this, Constant.LOADING);
            }
            return;
        }

        showAlert();
        drawBottomMenu();
        drawTitle(title);
    }

    protected void keyPressed(final int keyCode) {
        if (isLoading) {
            return;
        }

        if (!Constant.isKnownKey(this, keyCode)) {
            if (keyCode == Constant.KEY_SOFT_LEFT) {


                if (chatMessage != null) {
                    ImageList.images.removeAllElements();
                    detailView.recycle();
                    chatMessage = null;
                    refresh();
                    System.gc();
                    title = "Soil management";
                    return;
                }

            } else {
                 if(Profile.getInstance().getUserId()!= null){
//                    CartCanvas canvas = new CartCanvas();
//                canvas.prevDisplay = canvas ;
//                iMandiMIDlet.display.setCurrent(canvas);
                    }else{
//                        CartCanvas canvas = new CartCanvas();
//                        LoginScreen loginScreen = new LoginScreen(canvas,null);
//                        loginScreen.display();
//                        HelloMIDlet.display.setCurrent();
                    }
            }
        }
        super.keyPressed(keyCode);
        if (!showMenu && !showAlert) {
            if (list != null) {
                new Thread(new Runnable() {

                    public void run() {
                        list.keyPressed(keyCode);
                    }
                }).start();
            }
        }
        refresh();
    }

    protected void keyRepeated(int keyCode) {
        super.keyRepeated(keyCode);
        if (list != null) {
            list.keyPressed(keyCode);
        }
        refresh();
    }

    class List extends IMandiList //extends Canvas
    {

        Vector chats;
        ImandiCanvas canvas;
        int rowMenuIndex;
        boolean rowMenu;
        int maxCommandInRow = 0;
//        private ImandiCanvas productCanvas;

        public List(String title, Vector chats, ImandiCanvas canvas) {
            this.chats = chats;
            this.canvas = canvas;
            itemsTop = new int[chats.size()];
//            this.itemHeight = (canvas.g.getFont().getHeight()*3)+5;
        }

        public int getItemWidth() {
            return canvas.getWidth() - 2 * borderWidth - 2 * padding - 2 * margin;
        }

        public void keyPressed(int key) {

            int keyCode = canvas.getGameAction(key);

            // is there 1 item at least?
            if (chats.size() > 0) {
                // going up
                if (keyCode == Canvas.UP) {

                     if(chatMessage!= null){
                    screenMoveY = screenMoveY +100;
                    if(screenMoveY>0)
                        screenMoveY =0;
                    return;
                }

                    // current item is clipped on top, so can scroll up
                    if (itemsTop[selectedItem] - Constant.TITLE_HEIGHT * 2 < scrollTop) {
                        scrollTop -= SCROLL_STEP;
                        if (scrollTop < 0) {
                            scrollTop = 0;
                        }
                        canvas.repaint();
                    } // is there a previous item?
                    else if (selectedItem > 0) {
                        selectedItem--;
                        canvas.repaint();
                    }
                    if (selectedItem == 0) {
                        
                    }
                    rowMenuIndex = 0;
                    rowMenu = false;
                } //going down
                else if (keyCode == Canvas.DOWN) {
                    if(chatMessage!=null){
                    if(screenDown)
                    screenMoveY = screenMoveY -100;

                    return;
                }
                    // current item is clipped on bottom, so can scroll down
                    if (itemsTop[selectedItem] + itemHeight >= scrollTop + (canvas.getHeight() - itemHeight)) {
                        scrollTop += SCROLL_STEP;
                        //selectedItem++;
                        canvas.repaint();
                    } // is there a following item?
                    else if (selectedItem < chats.size() - 1) {
                        selectedItem++;

                        canvas.repaint();
                    }
                    if (selectedItem == chats.size() - 1) {
                        
                    }
                    rowMenu = false;
                    rowMenuIndex = 0;
                } else if (keyCode == Canvas.RIGHT) {
                } else if (keyCode == Canvas.LEFT) {
                } else if (keyCode == Canvas.FIRE) {

                    if (chatMessage == null) {
                       startImageLoading();
                       chatMessage = (ChatMessage) chats.elementAt(selectedItem);
                       title = chatMessage.senderName;
//                    Utility.openProductList(CategoryScreen.this, (Category) products.elementAt(selectedItem));
                    }else {
                    if (chatMessage != null) {
                    }
                }
                }
            }
            //Constant.printLog("scrollTop:", ""+scrollTop+" :itemTop:"+itemsTop[selectedItem]);
            refresh();
        }

        protected void paint(Graphics g) {

            g.setFont(Constant.font_medium);
            g.setColor(Constant.COLOR_bgColor);
            g.fillRect(0, 20, canvas.getWidth(), canvas.getHeight());

            g.translate(0, -scrollTop);

            int top = 20;

            g.setFont(font);

            // loop List items
            for (int i = 0; i < chats.size(); i++) {
                ChatMessage chatMessage = ((ChatMessage) chats.elementAt(i));
                int itemRows = 1;//itemLines[i].length;

                Image imagePart = null;//product;//imgetImage(i);

                int itemHeight = this.itemHeight;//itemRows * font.getHeight() + linePadding * (itemRows - 1);

                itemsTop[i] = top;

                // is image part higher than the text part?
                if (imagePart != null && imagePart.getHeight() > itemHeight) {
                    itemHeight = imagePart.getHeight();
                }

                itemHeight = (g.getFont().getHeight()*4)+10;
                g.translate(0, top);

                if (borderWidth > 0) {
                    // paint item border
                    g.setColor(i == selectedItem ? Constant.COLOR_borderColor : Constant.COLOR_borderColor);
                    g.fillRect(margin, margin + topSpace, canvas.getWidth() - 2 * margin, itemHeight);
                }

                // paint item background
                g.setColor(i == selectedItem ? Constant.COLOR_backSelectedColor : Constant.COLOR_backColor);
                g.drawRect(margin + borderWidth, margin + borderWidth + topSpace, canvas.getWidth() - 2 * margin - 2 * borderWidth, itemHeight - 2 * borderWidth);

                if(i == selectedItem)
                {
                    g.setColor(Constant.COLOR_backSelectedColor);
//                    g.fillRect(margin + borderWidth, margin + borderWidth + topSpace,3, itemHeight - 2 * borderWidth);
                }
                // has this item an image part?
                if (imagePart != null) {
                    g.drawImage(imagePart, margin + borderWidth + padding, margin + borderWidth + padding + topSpace, Graphics.TOP | Graphics.LEFT);
                }

                // paint item text rows
                g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

                int textLeft = margin + borderWidth + padding + (imagePart != null ? imagePart.getWidth() + padding : 0);

                getPrevFont();
                g.setFont(Constant.font_medium_bold);
                g.drawString(chatMessage.senderName, textLeft+6, topSpace + margin + borderWidth, Graphics.TOP | Graphics.LEFT);
                g.setFont(Constant.font_medium);
                if(chatMessage.messageSort!=null)
                g.drawString(chatMessage.messageSort, textLeft+6, topSpace + margin + borderWidth + padding + (1 * font.getHeight()), Graphics.TOP | Graphics.LEFT);
                if(chatMessage.contentType.indexOf("i") != -1){
                    //image
                    g.drawImage(ImageList.videoRecord, textLeft+6, topSpace + margin + borderWidth + padding + (2 * font.getHeight()), 20);
                }
                if(chatMessage.contentType.indexOf("a") != -1){
                    //image
                    g.drawImage(ImageList.voiceRecord, textLeft+6, topSpace + margin + borderWidth + padding + (2 * font.getHeight()), 20);
                }
                //g.drawString(chatMessage.contentType, textLeft+6, topSpace + margin + borderWidth + padding + (2 * font.getHeight()), Graphics.TOP | Graphics.LEFT);
                g.drawString(chatMessage.date, textLeft+6, topSpace + margin + borderWidth + padding + (3 * font.getHeight()), Graphics.TOP | Graphics.LEFT);

                setPrevFont();
                g.translate(0, -top);

                top += itemHeight + 2 * margin;
            }
            // finally, translate back
            g.translate(0, scrollTop);
        }

        public void recycleImage() {
        }
        boolean adding = false;
        int index = 10;

       
    }

    public void menuItemSelected(String menuItemLabel) {
       
    }
 Thread imageLoading;

    private void startImageLoading() {
        try{

        if(chatMessage !=null && chatMessage.contentVec !=null && chatMessage.contentType.indexOf("i") != -1 && chatMessage.contentVec.size()>0)
        Constant.printLog("startImageLoading", chatMessage.toString());
        imageLoading = new Thread(new Runnable() {

            public void run() {

                ImageList.images.removeAllElements();
                if(chatMessage !=null && chatMessage.contentVec !=null && chatMessage.contentVec.elementAt(0) != null && chatMessage.contentVec.size()>0)
                for (int i = 0; i < chatMessage.contentVec.size(); i++) {
                    try {

//                        Thread.sleep(5000);
                        if(chatMessage == null)
                            return;
//                        showLoading();
                            showLoading();
                        if(chatMessage !=null && chatMessage.contentVec !=null && chatMessage.contentType.indexOf("i") != -1 && chatMessage.contentVec.size()>0){
                            MyConnection.getInstance().downloadAndSaveImage((String) chatMessage.contentVec.elementAt(0), i, FourmChat.this,(byte)1);
                        }
                        else
                        {
                            break;
                        }
                    } catch (Exception ex) {

                        ex.printStackTrace();
                    }
                }
                refresh();

            }
        });
        imageLoading.start();
        } catch (Exception ex) {

                        ex.printStackTrace();
                    }
    }

     public void imageLoaded(String Status,byte requestID) {
         super.imageLoaded(Status, requestID);
        repaint();
        System.out.println("IMAGE LOADED!!!!!");
    }

     public String dateFormate(String date){
         date = date.substring(0, 19);
         return "TUE, 7 NOV";
     }

}
