package com.iMandi;

import com.iMandi.bean.CartItems;
import com.iMandi.bean.CartProduct;
import com.iMandi.bean.Category;
import com.iMandi.bean.Product;
import com.iMandi.bean.Profile;
import com.iMandi.connection.JsonStringToOjbect;
import com.iMandi.connection.MyConnection;
import java.util.TimerTask;
import java.util.Vector;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import org.json.me.util.ImageList;

/**
 *
 * @author nagendrakumar
 */
public class CategoryScreen extends GridCanvas {

//    private Font mFont = Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_PLAIN, Font.SIZE_LARGE);;

    CategoryScreen canvas ;
    CategoryList categoryList ;
//    public CategoryScreen(){
//        canvas = this;
//        setFullScreenMode(true);
//        isLoading = true;
////        categoryList = new  CategoryList("", Constant.category, canvas);
//        MyConnection.getInstance().makeConnection(Constant.URL_PRODUCT_CATEGORY, this, null,"GET",(byte)1);
//    }







    List friendList;
//    Timer timer;

    public CategoryScreen() {
        super(new Vector(),
        10,
        3,2);
        title = "Category";
        isLoading= true;
        LEFT_MENU ="Back";
        CENTER_MENU ="Select";

        if (CartItems.getInstance().cartItemTable.size() > 0 /*&& 1==2*/) {
                RIGHT_MENU ="Cart[" + CartItems.getInstance().cartItemTable.size() + "]";
            }
            else
            {
                RIGHT_MENU = "Cart";
            }
        
//        isLoading= true;
// MyConnection.getInstance().makeConnection(Constant.URL_PRODUCT_CATEGORY, this, null,"GET",(byte)1);
        String menuItems[] = {"Ass as freind", "Add cart", "Ass as freind", "Ass as freind", "Ass as freind", "Ass as freind", "Ass as freind", "Ass as freind", "Ass as freind"};
       
        type = 2;
        new Thread(new Runnable() {

            public void run() {
               try{Thread.sleep(100);init();refresh();}catch(Exception e){}
            }
        }).start();
//        hasMenu = true;
    }
     protected void paint(Graphics g) {
         isLoading= false;
        super.paint(g);
     }
    protected void paintXXX(Graphics g) {
        super.paint(g);
        if(friendList!=null)
        friendList.paint(g);
        drawTitle(title);
        if (isLoading) {
            Constant.loading(g, this, Constant.LOADING);
        }
        showMenu();

//        g.setColor(Constant.COLOR_BOTTOM);
//        g.fillRect(0, getHeight() - (Constant.HEIGHT_BOTTOM), getWidth(), Constant.HEIGHT_BOTTOM);


//        g.setColor(Constant.COLOR_MENU_TEXT);
//        g.drawString("Menu", 2, getHeight() - 2, Graphics.BOTTOM | Graphics.LEFT);

        showAlert();
        drawBottomMenu();
    }

    protected void keyPressed(final int keyCode) {
        if (isLoading) {
            return;
        }
        super.keyPressed(keyCode);

        if (!Constant.isKnownKey(this, keyCode)) {
            if (keyCode == Constant.KEY_SOFT_LEFT) {


            } else {
                 if(Profile.getInstance().getUserId()!= null){
                    CartCanvas canvas = new CartCanvas();
                canvas.prevDisplay = canvas ;
                iMandiMIDlet.display.setCurrent(canvas);
                    }else{
//                        CartCanvas canvas = new CartCanvas();
//                        LoginScreen loginScreen = new LoginScreen(canvas,null);
//                        loginScreen.display();
//                        HelloMIDlet.display.setCurrent();
                    }
            }
        }
        if (!showMenu && !showAlert) {
            if(friendList!=null)
            new Thread(new Runnable() {

                public void run() {
//                    friendList.keyPressed(keyCode);
                }
            }).start();
        }
        
        refresh();
    }

    protected void keyRepeated(int keyCode) {
//        super.keyRepeated(keyCode);
//         if(friendList!=null)
//        friendList.keyPressed(keyCode);
        refresh();
    }

    class List extends IMandiList //extends Canvas
    {

        Vector products;
        CategoryScreen canvas;
        int rowMenuIndex;
        boolean rowMenu;
        int maxCommandInRow = 0;
        private ProductCanvas productCanvas;

        public List(String title, Vector products, CategoryScreen canvas) {
            this.products = products;
            this.canvas = canvas;
            itemsTop = new int[products.size()];
        }

        public int getItemWidth() {
            return canvas.getWidth() - 2 * borderWidth - 2 * padding - 2 * margin;
        }

        public void keyPressed(int key) {
if (isLoading) {
            return;
        }
            int keyCode = canvas.getGameAction(key);

            // is there 1 item at least?
            if (products.size() > 0) {
                // going up
                if (keyCode == Canvas.UP) {
                    // current item is clipped on top, so can scroll up
                    if (itemsTop[selectedItem] - Constant.TITLE_HEIGHT * 2 < scrollTop) {
                        scrollTop -= SCROLL_STEP;
                        if (scrollTop < 0) {
                            scrollTop = 0;
                        }
                        canvas.repaint();
                    } // is there a previous item?
                    else if (selectedItem > 0) {
                        selectedItem--;
                        canvas.repaint();
                    }
                    if (selectedItem == 0) {
                        
                    }
                    rowMenuIndex = 0;
                    rowMenu = false;
                } //going down
                else if (keyCode == Canvas.DOWN) {
                    // current item is clipped on bottom, so can scroll down
                    if (itemsTop[selectedItem] + itemHeight >= scrollTop + (canvas.getHeight() - itemHeight)) {
                        scrollTop += SCROLL_STEP;
                        //selectedItem++;
                        canvas.repaint();
                    } // is there a following item?
                    else if (selectedItem < products.size() - 1) {
                        selectedItem++;

                        canvas.repaint();
                    }
                    if (selectedItem == products.size() - 1) {
                        
                    }
                    rowMenu = false;
                    rowMenuIndex = 0;
                } else if (keyCode == Canvas.RIGHT) {

                } else if (keyCode == Canvas.LEFT) {

                } else if (keyCode == Canvas.FIRE) {
                    Utility.openProductList(CategoryScreen.this, (Category) products.elementAt(selectedItem));
                }
            }
            //Constant.printLog("scrollTop:", ""+scrollTop+" :itemTop:"+itemsTop[selectedItem]);
        }

        protected void paint(Graphics g) {

            // paint List background
            g.setColor(Constant.COLOR_bgColor);
            g.fillRect(0, 20, canvas.getWidth(), canvas.getHeight());

            if (showDetail) {
//            canvas.detailView.diaplayDetail(g, canvas);
                return;
            }

            // translate accordingly to current List vertical scroll
            g.translate(0, -scrollTop);

            int top = 20;

            g.setFont(font);

            // loop List items
            for (int i = 0; i < products.size(); i++) {
                int itemRows = 1;//itemLines[i].length;

                Image imagePart = null;//product;//imgetImage(i);

                int itemHeight = this.itemHeight;//itemRows * font.getHeight() + linePadding * (itemRows - 1);

                itemsTop[i] = top;
//            itemsHeight[i] = itemHeight;

                // is image part higher than the text part?
                if (imagePart != null && imagePart.getHeight() > itemHeight) {
                    itemHeight = imagePart.getHeight();
                }
//                itemHeight += 2 * padding + 2 * borderWidth;
                itemHeight = g.getFont().getHeight()+6;

                g.translate(0, top);

                if (borderWidth > 0) {
                    // paint item border
                    g.setColor(i == selectedItem ? Constant.COLOR_borderSelectedColor : Constant.COLOR_borderColor);
                    g.fillRect(margin, margin + topSpace, canvas.getWidth() - 2 * margin, itemHeight);
                }

                // paint item background
                g.setColor(i == selectedItem ? Constant.COLOR_backSelectedColor : Constant.COLOR_backColor);
                g.fillRect(margin + borderWidth, margin + borderWidth + topSpace, canvas.getWidth() - 2 * margin - 2 * borderWidth, itemHeight - 2 * borderWidth);

                // has this item an image part?
                if (imagePart != null) {
                    g.drawImage(imagePart, margin + borderWidth + padding, margin + borderWidth + padding + topSpace, Graphics.TOP | Graphics.LEFT);
                }

                // paint item text rows
                g.setColor(i == selectedItem ? Constant.COLOR_foreSelectedColor : Constant.COLOR_foreColor);

                int textLeft = margin + borderWidth + padding + (imagePart != null ? imagePart.getWidth() + padding : 0);

            g.drawString(((Category) products.elementAt(i)).getCategoryname(), textLeft, topSpace + margin + borderWidth + padding, Graphics.TOP | Graphics.LEFT);

                g.translate(0, -top);

                top += itemHeight + 2 * margin;
            }
            // finally, translate back
            g.translate(0, scrollTop);
        }



        public void recycleImage() {
        }
        boolean adding = false;
        int index = 10;

    }

    public void menuItemSelected(String menuItemLabel) {
        super.menuItemSelected(menuItemLabel);
        alertText = menuItemLabel;
        showAlert = true ;
        isKeyEventConsume = true;
    }
    ////////////////////////////////////////
    protected void paintXXXX(Graphics g) {
        if(isLoading)
            return;
        g.setFont(Constant.font_small);
        g.setColor(Constant.COLOR_TAB_SELECTED);
        g.fillRect(0, 0, getWidth(), getHeight());

        g.setColor(255,255,255);

        if(categoryList !=  null)
        categoryList.paint(g, 20);


        drawTitleXXX(g);
        drawBottomXXX(g);
    }
    private void drawTitleXXX(Graphics g) {
        Font o = g.getFont();
        g.setFont(Constant.font_medium);
        g.setColor(Constant.COLOR_HEADER);
        g.fillRect(0, 0, getWidth(), Constant.TITLE_HEIGHT);

        g.setColor(255, 255, 255);


        String s = Constant.CATEGORY_TITLE;
        int stringWidth = Constant.font_medium.stringWidth(s);
        int stringHeight = Constant.font_medium.getHeight();
        int x = (getWidth() - stringWidth) / 2;
        int y = getHeight() / 2;

//        g.setFont(mFont);
        g.drawString(s, x, 0, Graphics.TOP | Graphics.LEFT);
        g.drawImage(ImageList.logo_20x20, 0, 0, Graphics.TOP | Graphics.LEFT);
        g.setFont(o);

    }
void drawBottomXXX(Graphics g){
    Font o = g.getFont();
    g.setFont(Constant.font_small);
     g.setColor(Constant.COLOR_BOTTOM);

        g.fillRect(0, canvas.getHeight() - (Constant.HEIGHT_BOTTOM), canvas.getWidth(), Constant.HEIGHT_BOTTOM);

        g.setColor(Constant.COLOR_MENU_TEXT);
        if(CartItems.getInstance().cartItemTable.size()>0)
        g.drawString("Cart["+CartItems.getInstance().cartItemTable.size()+"]", 2, canvas.getHeight() - 2,Graphics.BOTTOM|Graphics.LEFT);
        else
         g.drawString("Cart["+CartItems.getInstance().cartItemTable.size()+"]", 2, canvas.getHeight() - 2,Graphics.BOTTOM|Graphics.LEFT);
//                       g.drawString("Cart["+CartItems.getInstance().cartItemTable.size()+"]", canvas.getWidth()-g.getFont().stringWidth("Cart["+CartItems.getInstance().cartItemTable.size()+"]"), canvas.getHeight() - 2,Graphics.BOTTOM|Graphics.LEFT);
//

g.setFont(o);
    }

    protected void keyPressedXXX(int keyCode) {
//        super.keyPressed(keyCode);
        if(categoryList!=null)
        categoryList.keyPressed(keyCode);


    }

    public void sendData(String data,byte requestId) {
        super.sendData(data,requestId);
        if(requestId==1){
           Vector products = JsonStringToOjbect.getCategoryList(data);
//                categoryList = new  CategoryList("", Constant.category, canvas);
//            Vector products = new Vector();

//        for (int i = 0; i < 10; i++) {
//            Product p = new CartProduct();
//            p.setProductName("" + i);
//            products.addElement(p);
//        }

        friendList = new List("", products, this);
        }
        refresh();
    }


}
