package com.ui.components;
/**
 *
 * @author nagendrakumar
 */
public interface SoftKeyListener {
	public void leftSoftKeyPressed();
	public void rightSoftKeyPressed();
}
