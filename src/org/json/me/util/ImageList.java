package org.json.me.util;

import java.util.Vector;
import javax.microedition.lcdui.Image;

/**
 *
 * @author nagendrakumar
 */
public class ImageList {
    public static Vector images = new Vector();

    public static Image voiceRecord,videoRecord,product,cartImage,more,home,logo,logo_20x20,thumbs_up,loading ;
    static{
    try {
            voiceRecord = Image.createImage("/voice_record.png");
            videoRecord = Image.createImage("/image-msg.png");
            product = Image.createImage("/default.png");
            cartImage = Image.createImage("/cart-icon.png") ;
            home = Image.createImage("/home.png") ;
            logo = Image.createImage("/logo.png") ;
            more = Image.createImage("/more.png") ;
            logo_20x20 = Image.createImage("/logo_20x20.png") ;//logo_20x20.png
            thumbs_up = Image.createImage("/thumbs_up.png") ;//logo_20x20.png
            loading = Image.createImage("/loading.png") ;//logo_20x20.png
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void recycleImage(){
        for (int i = 0; i < images.size(); i++) {
             Image img = ((Image)images.elementAt(i)) ;
             img= null;
        }
        images.removeAllElements();
        System.gc();
    }
}
